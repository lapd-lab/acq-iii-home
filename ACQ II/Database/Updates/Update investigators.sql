/*** Update investigators.sql ***/

UPDATE configuration.investigators
SET database_name = "@database",
    data_run_id = @data_run_id
WHERE investigator_id = @investigator_id