/*** Select investigators.sql ***/

SELECT
  a.investigator_id,
  a.investigator_name,
  a.affiliation,
  a.status,
  a.permission_id,
  a.database_name,
  a.data_run_id,
  a.created_datetime
FROM configuration.investigators a