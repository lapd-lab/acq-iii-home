/*** Select data file.sql ***/

SELECT
  a.data_file_id,
  c.data_file_path
FROM @database.data_file_assignments a,
     configuration.devices b,
     @database.data_files c
WHERE a.data_run_id = @data_run_id
  AND b.device_name = "@device_name"
  AND b.device_id = a.device_id
  AND a.data_file_id = c.data_file_id