/*** Select ancestors.sql ***/

SELECT
  a.experiment_id,
  b.experiment_name,
  a.experiment_set_id,
  c.experiment_set_name
FROM @database.data_runs a,
     @database.experiments b,
     configuration.experiment_sets c
WHERE a.data_run_id = @data_run_id
  AND a.experiment_id = b.experiment_id
  AND a.experiment_set_id = c.experiment_set_id
