SELECT dr.data_run_id,
       dr.data_run_name,
       dr.data_run_description,
       dr.experiment_set_id,
        i.investigator_name,
       dr.current_status,
       dr.status_datetime

FROM @database.data_runs dr,
     configuration.investigators i

WHERE dr.investigator_id = i.investigator_id

