CREATE TABLE IF NOT EXISTS @database.experiments
(
  experiment_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  experiment_name VARCHAR(120) NOT NULL, 
  experiment_description TEXT NOT NULL, 
  experiment_set_id SMALLINT UNSIGNED NOT NULL,
  investigator_id SMALLINT UNSIGNED NOT NULL,
  current_status VARCHAR(120) NOT NULL DEFAULT "Created",
  status_datetime DOUBLE NOT NULL, 

  INDEX experiments_pk (experiment_id),
  PRIMARY KEY (experiment_id),

  UNIQUE INDEX experiments_ui (experiment_name),

  INDEX experiments_fk1 (experiment_set_id),
  FOREIGN KEY (experiment_set_id) REFERENCES configuration.experiment_sets(experiment_set_id),

  INDEX data_runs_fk2 (investigator_id),
  FOREIGN KEY (investigator_id) REFERENCES configuration.investigators(investigator_id)

);


