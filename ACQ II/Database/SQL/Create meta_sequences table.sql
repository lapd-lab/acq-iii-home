CREATE TABLE IF NOT EXISTS @database.meta_sequences
(
  meta_sequence_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  meta_sequence_name VARCHAR(120) NOT NULL, 
  meta_sequence_description TEXT NOT NULL, 

  INDEX meta_sequences_pk (meta_sequence_id),
  PRIMARY KEY (meta_sequence_id),

  UNIQUE INDEX meta_sequences_UI (meta_sequence_name)

);


