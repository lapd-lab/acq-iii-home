SELECT a.device_id,
       a.device_name,
       b.device_type_name,
       a.device_description,
       a.device_bridge_vi_path,
       a.device_module_ip_address,
       a.device_module_vi_path,
       a.run_time_dock,
       a.created_datetime
FROM configuration.devices a,
     configuration.device_types b
WHERE a.device_type_id = b.device_type_id

