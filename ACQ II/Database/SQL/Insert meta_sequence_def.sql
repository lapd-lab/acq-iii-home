INSERT INTO @database.meta_sequence_defs
  (
  meta_sequence_id,
  meta_line_number,
  meta_line
  )
SELECT
  a.meta_sequence_id,
  @meta_line_number,
  "@meta_line_text"
FROM @database.meta_sequences a
WHERE a.meta_sequence_name = "@meta_sequence_name"
