CREATE TABLE IF NOT EXISTS @database.device_assignments
(
  data_run_id SMALLINT UNSIGNED NOT NULL,
  device_id SMALLINT UNSIGNED NOT NULL,

  INDEX device_assignments_pk (data_run_id, device_id),
  PRIMARY KEY (data_run_id, device_id),

  INDEX device_assignments_fk1 (data_run_id),
  FOREIGN KEY (data_run_id) REFERENCES @database.data_runs(data_run_id),

  INDEX device_assignments_fk2 (device_id),
  FOREIGN KEY (device_id) REFERENCES configuration.devices(device_id)
);


