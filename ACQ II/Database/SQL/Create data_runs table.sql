CREATE TABLE IF NOT EXISTS @database.data_runs
(
  data_run_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  experiment_id SMALLINT UNSIGNED NOT NULL,
  data_run_name VARCHAR(120) NOT NULL, 
  data_run_description TEXT NOT NULL, 
  experiment_set_id SMALLINT UNSIGNED NOT NULL,
  investigator_id SMALLINT UNSIGNED NOT NULL,
  current_status VARCHAR(120) NOT NULL DEFAULT "Created",
  status_datetime DOUBLE NOT NULL, 

  INDEX data_runs_pk (data_run_id),
  PRIMARY KEY (data_run_id),

  UNIQUE INDEX data_runs_ui (experiment_id, data_run_name),

  INDEX data_runs_fk1 (experiment_set_id),
  FOREIGN KEY (experiment_set_id) REFERENCES configuration.experiment_sets(experiment_set_id),

  INDEX data_runs_fk2 (experiment_id),
  FOREIGN KEY (experiment_id) REFERENCES @database.experiments(experiment_id),

  INDEX data_runs_fk3 (investigator_id),
  FOREIGN KEY (investigator_id) REFERENCES configuration.investigators(investigator_id)

);


