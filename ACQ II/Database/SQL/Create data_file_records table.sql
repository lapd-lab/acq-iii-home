CREATE TABLE IF NOT EXISTS @database.data_file_records
(
  data_file_id SMALLINT UNSIGNED NOT NULL,

  shot_number INT UNSIGNED NOT NULL,
  channel_designator_id SMALLINT UNSIGNED NOT NULL,
  byte_offset DOUBLE NOT NULL,
  byte_count DOUBLE NOT NULL,
  scale DOUBLE NOT NULL,
  offset DOUBLE NOT NULL,
  min SMALLINT NOT NULL,
  max SMALLINT NOT NULL,
  clipped TINYINT UNSIGNED NOT NULL,

  INDEX data_file_records_pk (data_file_id, shot_number, channel_designator_id),
  PRIMARY KEY (data_file_id, shot_number, channel_designator_id),

  INDEX data_file_records_fk1 (data_file_id),
  FOREIGN KEY (data_file_id) REFERENCES @database.data_files(data_file_id)

);


