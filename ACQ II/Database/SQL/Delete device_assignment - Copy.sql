DELETE @database.device_assignments
FROM @database.device_assignments a,
     configuration.devices b
WHERE a.device_id = b.device_id
  AND b.device_name = "@device_name"
  AND a.data_run_id = @data_run_id