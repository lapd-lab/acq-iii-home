CREATE TABLE IF NOT EXISTS @database.sequence_defs
(
  data_run_id SMALLINT UNSIGNED NOT NULL,
  meta_sequence_id SMALLINT UNSIGNED NOT NULL,
  line_number INT UNSIGNED NOT NULL,

  line TEXT NOT NULL, 
  shot_number INT UNSIGNED NOT NULL,
  expanded_line_number INT UNSIGNED NOT NULL,
  line_status VARCHAR(120) NOT NULL DEFAULT "Unknown",
  time_stamp DOUBLE NOT NULL,

  INDEX sequence_defs_pk (data_run_id, meta_sequence_id, line_number),
  PRIMARY KEY (data_run_id, meta_sequence_id, line_number),

  INDEX sequence_defs_fk1 (data_run_id),
  FOREIGN KEY (data_run_id) REFERENCES @database.data_runs(data_run_id),

  INDEX sequence_defs_fk2 (meta_sequence_id),
  FOREIGN KEY (meta_sequence_id) REFERENCES @database.meta_sequences(meta_sequence_id),

  INDEX sequence_defs_shot (data_run_id, shot_number)

);


