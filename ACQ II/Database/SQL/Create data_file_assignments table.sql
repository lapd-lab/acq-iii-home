CREATE TABLE IF NOT EXISTS @database.data_file_assignments
(
  data_run_id SMALLINT UNSIGNED NOT NULL,
  device_id SMALLINT UNSIGNED NOT NULL,

  data_file_id SMALLINT UNSIGNED NOT NULL,

  INDEX data_file_assignments_pk (data_run_id, device_id),
  PRIMARY KEY (data_run_id, device_id),

  UNIQUE INDEX data_file_assignments_ui1 (data_file_id),

  INDEX data_file_assignments_fk1 (data_run_id),
  FOREIGN KEY (data_run_id) REFERENCES @database.data_runs(data_run_id),

  INDEX data_file_assignments_fk2 (device_id),
  FOREIGN KEY (device_id) REFERENCES configuration.devices(device_id),

  INDEX data_file_assignments_fk3 (data_file_id),
  FOREIGN KEY (data_file_id) REFERENCES @database.data_files(data_file_id)

);


