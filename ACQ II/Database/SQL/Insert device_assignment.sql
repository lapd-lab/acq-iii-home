INSERT INTO @database.device_assignments
  (data_run_id, device_id)
SELECT @data_run_id,
       a.device_id
FROM configuration.devices a
WHERE device_name = "@device_name"
