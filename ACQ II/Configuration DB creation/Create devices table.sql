CREATE TABLE IF NOT EXISTS configuration.devices
(
  device_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  device_name VARCHAR(120) NOT NULL,
  device_type_id TINYINT UNSIGNED NOT NULL,
  device_description TEXT NOT NULL,
  device_bridge_vi_path TEXT NOT NULL,
  device_module_ip_address VARCHAR(120) NOT NULL,
  device_module_vi_path TEXT NOT NULL,
  run_time_dock TEXT NOT NULL,
  created_datetime DOUBLE NOT NULL,

  INDEX devices_pk (device_id),
  PRIMARY KEY (device_id),

  INDEX devices_fk1 (device_type_id),
  FOREIGN KEY (device_type_id) REFERENCES configuration.device_types(device_type_id),

  UNIQUE INDEX devices_ui1 (device_name)

);


