CREATE TABLE IF NOT EXISTS configuration.investigators
(
  investigator_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  investigator_name VARCHAR(120) NOT NULL, /* i.e. "Gekelman", "Guest" */
  affiliation VARCHAR(255) NOT NULL,
  status VARCHAR(120) NOT NULL DEFAULT "Active",
  permission_id SMALLINT UNSIGNED NOT NULL,
  database_name VARCHAR(120) NOT NULL,
  data_run_id SMALLINT UNSIGNED NOT NULL,
  created_datetime DOUBLE NOT NULL,

  INDEX investigators_pk (investigator_id),
  PRIMARY KEY (investigator_id),

  UNIQUE INDEX investigators_ui1 (investigator_name)
);


