INSERT INTO @database.meta_sequence_defs
  (
  meta_sequence_id,
  meta_line_number,
  meta_line
  )
VALUES
  (
  @meta_sequence_id,
  @meta_line_number,
  "@meta_line_text"
  )
