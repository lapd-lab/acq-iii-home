INSERT INTO @database.data_file_assignments
  (
  data_run_id,
  device_id,
  data_file_id
  )
SELECT
  @data_run_id,
  a.device_id,
  b.data_file_id
FROM configuration.devices a,
     @database.data_files b
WHERE a.device_name = "@device_name"
  AND b.unique_name = "@unique_name"
