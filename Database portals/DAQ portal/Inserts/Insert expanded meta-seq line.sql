INSERT INTO @database.expanded_meta_seq_defs
  (
  data_run_id,
  meta_sequence_id,
  expanded_line_number,
  expanded_line,
  meta_line_number
  )
VALUES
  (
  @data_run_id,
  @meta_sequence_id,
  @expanded_line_number,
  "@expanded_line_text",
  @meta_line_number
  )
