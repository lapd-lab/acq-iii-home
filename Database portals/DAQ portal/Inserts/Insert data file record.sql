INSERT INTO @database.data_file_records
  (
  data_file_id,
  shot_number,
  channel_designator_id,
  byte_offset,
  byte_count,
  scale,
  offset,
  min,
  max,
  clipped
  )
SELECT
  a.data_file_id,
  @shot_number,
  @channel_designator_id,
  @byte_offset,
  @byte_count,
  @scale,
  @offset,
  @min,
  @max,
  @clipped
FROM @database.data_file_assignments a,
     configuration.devices b
WHERE a.data_run_id = @data_run_id
  AND a.device_id = b.device_id
  AND b.device_name = "@device_name"
