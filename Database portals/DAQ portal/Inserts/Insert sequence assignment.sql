INSERT INTO @database.sequence_assignments
  (data_run_id, meta_sequence_id)
SELECT
  @data_run_id,
  a.meta_sequence_id
FROM @database.meta_sequences a
WHERE a.meta_sequence_name = "@meta_sequence_name"
