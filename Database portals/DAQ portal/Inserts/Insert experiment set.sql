INSERT INTO configuration.experiment_sets
  (
  experiment_set_name,
  experiment_set_description,
  created_datetime
  )
VALUES
  (
  "@experiment_set_name",
  @experiment_set_description,
  @created_datetime
  )
