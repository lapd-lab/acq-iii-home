SELECT a.experiment_id,
       a.experiment_name,
       a.experiment_description,
       a.experiment_set_id,
       b.investigator_name,
       a.current_status,
       a.status_datetime

FROM @database.experiments a,
     configuration.investigators b

WHERE a.investigator_id = b.investigator_id

