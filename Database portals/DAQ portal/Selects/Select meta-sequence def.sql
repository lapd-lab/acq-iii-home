SELECT a.meta_sequence_id,
       a.meta_line_number,
       a.meta_line

FROM @database.meta_sequence_defs a

WHERE a.meta_sequence_id = @meta_sequence_id
ORDER BY a.meta_line_number

