SELECT a.meta_sequence_id,
       a.meta_sequence_name,
       a.meta_sequence_description

FROM @database.meta_sequences a

WHERE 1=1 /* Allows additional clause to start with "AND" */

