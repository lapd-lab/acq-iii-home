SELECT dr.data_run_id,
        e.experiment_name,
       dr.data_run_name,
       dr.data_run_description,
       dr.experiment_set_id,
        i.investigator_name,
       dr.current_status,
       dr.status_datetime

FROM @database.data_runs dr,
     @database.experiments e,
     configuration.investigators i

WHERE dr.experiment_id = e.experiment_id
  AND dr.investigator_id = i.investigator_id

