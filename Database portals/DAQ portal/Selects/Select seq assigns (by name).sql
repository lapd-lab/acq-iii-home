SELECT a.data_run_id,
       a.meta_sequence_id,
       b.meta_sequence_name

FROM @database.sequence_assignments a,
     @database.meta_sequences b

WHERE a.meta_sequence_id = b.meta_sequence_id
  AND b.meta_sequence_name = "@meta_sequence_name"

