SELECT a.device_interface_id,
       a.device_interface_name,
       b.device_type_name,
       a.device_interface_description,
       a.device_interface_vi_path,
       a.created_datetime
FROM configuration.device_interfaces a,
     configuration.device_types b

WHERE a.device_type_id = b.device_type_id

