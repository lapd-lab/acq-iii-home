CREATE TABLE IF NOT EXISTS @database.xy_motions
(
  motion_list_id SMALLINT UNSIGNED NOT NULL,
  motion_number INT UNSIGNED NOT NULL,

  x DOUBLE NOT NULL,
  y DOUBLE NOT NULL,
  take_data TINYINT UNSIGNED NOT NULL,
  data_motion_number INT UNSIGNED NOT NULL,

  INDEX xy_motions_pk (motion_list_id, motion_number),
  PRIMARY KEY (motion_list_id, motion_number),

  INDEX xy_motions_fk1 (motion_list_id),
  FOREIGN KEY (motion_list_id) REFERENCES @database.motion_lists(motion_list_id)
);


