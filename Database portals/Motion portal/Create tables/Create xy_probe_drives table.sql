CREATE TABLE IF NOT EXISTS @database.xy_probe_drives
(
  data_run_id SMALLINT UNSIGNED NOT NULL,
  receptacle_id TINYINT UNSIGNED NOT NULL,

  port_number TINYINT UNSIGNED NOT NULL,
  port_location_id TINYINT UNSIGNED NOT NULL,
  probe_id SMALLINT UNSIGNED NOT NULL,
  retracted_sx_cm DOUBLE NOT NULL,
  level_sy_cm DOUBLE NOT NULL,

  INDEX xy_configurations_pk (data_run_id, receptacle_id),
  PRIMARY KEY (data_run_id, receptacle_id),

  INDEX xy_probe_drives_fk1 (data_run_id),
  FOREIGN KEY (data_run_id) REFERENCES @database.data_runs(data_run_id),

  INDEX xy_probe_drives_fk2 (receptacle_id),
  FOREIGN KEY (receptacle_id) REFERENCES motion.receptacles(receptacle_id),

  INDEX xy_probe_drives_fk3 (port_location_id),
  FOREIGN KEY (port_location_id) REFERENCES motion.port_locations(port_location_id),

  INDEX xy_probe_drives_fk4 (probe_id),
  FOREIGN KEY (probe_id) REFERENCES motion.probes(probe_id)
);


