CREATE TABLE IF NOT EXISTS @database.xy_motion_list_defs
(
  motion_list_id SMALLINT UNSIGNED NOT NULL,

  grid_center_x DOUBLE NOT NULL, /* cm */
  grid_center_y DOUBLE NOT NULL, /* cm */
  delta_x DOUBLE NOT NULL, /* cm */
  delta_y DOUBLE NOT NULL, /* cm */
  nx INT NOT NULL,
  ny INT NOT NULL,

  INDEX xy_motion_list_defs_pk (motion_list_id),
  PRIMARY KEY (motion_list_id),

  INDEX xy_locations_fk1 (motion_list_id),
  FOREIGN KEY (motion_list_id) REFERENCES @database.motion_lists(motion_list_id)
);


