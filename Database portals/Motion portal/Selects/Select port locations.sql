SELECT
  a.port_location_id,
  a.port_location_name
FROM motion.port_locations a
WHERE 1 = 1  /* this allows optional clauses to always start with "AND" */
