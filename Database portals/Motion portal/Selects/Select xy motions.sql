SELECT
  a.motion_list_id,
  a.motion_number, /* actually should be motion_index */
  a.x,
  a.y,
  a.take_data,
  a.data_motion_number
FROM @database.xy_motions a
WHERE a.motion_list_id = @motion_list_id
ORDER BY a.motion_number
