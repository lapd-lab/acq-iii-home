SELECT
  a.xy_calibration_id,
  a.xy_calibration_name,
  a.theta_coeff_count,
  a.r_coeff_count,
  a.created_datetime
FROM motion.xy_calibrations a
WHERE 1 = 1  /* this allows optional clauses to always start with "AND" */
