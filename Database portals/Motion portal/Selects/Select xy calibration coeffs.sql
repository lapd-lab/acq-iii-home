SELECT
  a.xy_calibration_id,
  a.theta_coeff_order,
  a.r_coeff_order,
  a.coeff
FROM motion.xy_calibration_coeffs a
WHERE 1 = 1  /* this allows optional clauses to always start with "AND" */
