INSERT INTO @database.motion_run_time
  (
  data_run_id,
  receptacle_id,
  shot_number,
  motion_list_id,
  motion_index
  )
SELECT
  @data_run_id,
  a.receptacle_id,
  @shot_number,
  b.motion_list_id,
  @motion_index
FROM motion.receptacles a,
     @database.motion_lists b
WHERE a.receptacle_number = @receptacle_number
  AND b.motion_list_name = "@motion_list_name"

