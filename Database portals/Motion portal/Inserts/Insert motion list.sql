INSERT INTO @database.motion_lists
  (motion_list_name, motion_count, data_motion_count, motion_device_type_id, created_datetime)
SELECT
  "@motion_list_name",
  @motion_count,
  @data_motion_count,
  a.motion_device_type_id,
  @created_datetime
FROM motion.motion_device_types a
WHERE a.motion_device_type_name = "@motion_device_type_name"