SELECT
  a.configuration_id,
  a.unit_number,
  a.channel_number,
  a.LP_filtering,
  a.coupling,
  a.impedance,
  a.range_pp,
  a.offset,
  a.auto_gain
FROM @database.tvs645a_verticals a
WHERE 1 = 1 /* Kludge to allow "and clauses" */
  AND a.configuration_id = @configuration_id
