SELECT
  a.data_run_id,
  a.configuration_id,
  b.configuration_name,
  b.created_datetime
FROM @database.tvs645a_assignments a, @database.tvs645a_config_names b
WHERE a.configuration_id = b.configuration_id
