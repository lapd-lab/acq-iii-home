SELECT
  a.data_run_id,
  a.shot_number,
  a.configuration_id,
  b.configuration_name
FROM @database.tvs645a_run_time a,
     @database.tvs645a_config_names b
WHERE 1 = 1 /* Kludge to allow "and clauses" */
  AND a.data_run_id = @data_run_id
  AND a.configuration_id = b.configuration_id
