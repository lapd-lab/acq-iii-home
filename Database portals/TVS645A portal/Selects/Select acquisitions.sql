SELECT
  a.configuration_id,
  a.unit_number,
  a.acquisition_mode,
  a.shots_to_average,
  a.format,
  a.byte_order
FROM @database.tvs645a_acquisitions a
WHERE 1 = 1 /* Kludge to allow "and clauses" */
  AND a.configuration_id = @configuration_id
