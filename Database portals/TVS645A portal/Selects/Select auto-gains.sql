SELECT
  a.configuration_id,
  a.unit_number,
  a.channel_number,
  a.auto_gain_mode,
  a.max_range_pp,
  a.min_range_pp,
  a.safety_shots,
  a.safety_factor
FROM @database.tvs645a_auto_gains a
WHERE 1 = 1 /* Kludge to allow "and clauses" */
  AND a.configuration_id = @configuration_id
