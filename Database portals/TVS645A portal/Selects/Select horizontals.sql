SELECT
  a.configuration_id,
  a.unit_number,
  a.sample_rate,
  a.record_length,
  a.trigger_position,
  a.trigger_pos_units
FROM @database.tvs645a_horizontals a
WHERE 1 = 1 /* Kludge to allow "and clauses" */
  AND a.configuration_id = @configuration_id


