SELECT
  a.configuration_id,
  a.unit_number,
  a.channel_number
FROM @database.tvs645a_unit_config a
WHERE 1 = 1 /* Kludge to allow "and clauses" */
  AND a.configuration_id = @configuration_id

