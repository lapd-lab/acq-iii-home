CREATE TABLE IF NOT EXISTS @database.tvs645a_horizontals
(
  configuration_id SMALLINT UNSIGNED NOT NULL,
  unit_number TINYINT UNSIGNED NOT NULL,

  sample_rate TINYINT UNSIGNED NOT NULL,
  record_length SMALLINT UNSIGNED NOT NULL,
  trigger_position DOUBLE NOT NULL,
  trigger_pos_units TINYINT UNSIGNED NOT NULL,

  INDEX tvs645a_horizontals_pk (configuration_id, unit_number),
  PRIMARY KEY (configuration_id, unit_number),

  INDEX tvs645a_horizontals_fk1 (configuration_id),
  FOREIGN KEY (configuration_id) REFERENCES @database.tvs645a_config_names(configuration_id)
);


