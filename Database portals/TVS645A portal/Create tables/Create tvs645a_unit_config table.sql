CREATE TABLE IF NOT EXISTS @database.tvs645a_unit_config
(
  configuration_id SMALLINT UNSIGNED NOT NULL,
  unit_number TINYINT UNSIGNED NOT NULL,
  channel_number TINYINT UNSIGNED NOT NULL,

  INDEX tvs645a_unit_config_pk (configuration_id, unit_number, channel_number),
  PRIMARY KEY (configuration_id, unit_number, channel_number),

  INDEX tvs645a_unit_config_fk1 (configuration_id),
  FOREIGN KEY (configuration_id) REFERENCES @database.tvs645a_config_names(configuration_id)
);


