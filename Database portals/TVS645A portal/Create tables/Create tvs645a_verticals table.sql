CREATE TABLE IF NOT EXISTS @database.tvs645a_verticals
(
  configuration_id SMALLINT UNSIGNED NOT NULL,
  unit_number TINYINT UNSIGNED NOT NULL,
  channel_number TINYINT UNSIGNED NOT NULL,

  LP_filtering TINYINT UNSIGNED NOT NULL,
  coupling TINYINT UNSIGNED NOT NULL,
  impedance TINYINT UNSIGNED NOT NULL,
  range_pp DOUBLE NOT NULL,
  offset DOUBLE NOT NULL,
  auto_gain TINYINT NOT NULL,

  INDEX tvs645a_verticals_pk (configuration_id, unit_number, channel_number),
  PRIMARY KEY (configuration_id, unit_number, channel_number),

  INDEX tvs645a_verticals_fk1 (configuration_id),
  FOREIGN KEY (configuration_id) REFERENCES @database.tvs645a_config_names(configuration_id)
);


