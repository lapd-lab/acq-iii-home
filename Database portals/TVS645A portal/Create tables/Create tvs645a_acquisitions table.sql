CREATE TABLE IF NOT EXISTS @database.tvs645a_acquisitions
(
  configuration_id SMALLINT UNSIGNED NOT NULL,
  unit_number TINYINT UNSIGNED NOT NULL,

  acquisition_mode TINYINT UNSIGNED NOT NULL,
  shots_to_average TINYINT UNSIGNED NOT NULL,
  format TINYINT NOT NULL,
  byte_order TINYINT NOT NULL,

  INDEX tvs645a_acquisitions_pk (configuration_id, unit_number),
  PRIMARY KEY (configuration_id, unit_number),

  INDEX tvs645a_acquisitions_fk1 (configuration_id),
  FOREIGN KEY (configuration_id) REFERENCES @database.tvs645a_config_names(configuration_id)
);


