CREATE TABLE IF NOT EXISTS @database.tvs645a_auto_gains
(
  configuration_id SMALLINT UNSIGNED NOT NULL,
  unit_number TINYINT UNSIGNED NOT NULL,
  channel_number TINYINT UNSIGNED NOT NULL,

  auto_gain_mode TINYINT UNSIGNED NOT NULL,
  max_range_pp DOUBLE NOT NULL,
  min_range_pp DOUBLE NOT NULL,
  safety_shots TINYINT UNSIGNED NOT NULL,
  safety_factor DOUBLE NOT NULL,

  INDEX tvs645a_auto_gains_pk (configuration_id, unit_number, channel_number),
  PRIMARY KEY (configuration_id, unit_number, channel_number),

  INDEX tvs645a_auto_gains_fk1 (configuration_id),
  FOREIGN KEY (configuration_id) REFERENCES @database.tvs645a_config_names(configuration_id)
);


