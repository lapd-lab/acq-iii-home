INSERT INTO @database.tvs645a_acquisitions
  (
  configuration_id,
  unit_number,
  acquisition_mode,
  shots_to_average,
  format,
  byte_order
  )
VALUES
  (
  @configuration_id,
  @unit_number,
  @acquisition_mode,
  @shots_to_average,
  @format,
  @byte_order
  )
