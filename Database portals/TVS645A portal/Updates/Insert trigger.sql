INSERT INTO @database.tvs645a_triggers
  (
  configuration_id,
  unit_number,
  trigger_mode,
  trigger_source,
  trigger_coupling,
  trigger_slope,
  trigger_level
  )
VALUES
  (
  @configuration_id,
  @unit_number,
  @trigger_mode,
  @trigger_source,
  @trigger_coupling,
  @trigger_slope,
  @trigger_level
  )
