CREATE TABLE IF NOT EXISTS @database.sis_3301_run_time
(
  data_run_id SMALLINT(5) UNSIGNED NOT NULL,
  sequence_number INT(10) UNSIGNED NOT NULL,

  configuration_id INT(10) UNSIGNED NOT NULL,
  time_stamp DOUBLE(16,3) NOT NULL,

  INDEX sis_3301_run_time_pk (data_run_id, sequence_number),
  PRIMARY KEY (data_run_id, sequence_number),

  INDEX sis_3301_run_time_fk1 (data_run_id),
  FOREIGN KEY (data_run_id) REFERENCES @database.data_runs,

  INDEX sis_3301_run_time_fk2 (configuration_id),
  FOREIGN KEY (configuration_id) REFERENCES @database.sis_3301,
);


