/*** Delete xy motions.sql ***/

DELETE @database.xy_motions
FROM @database.xy_motions a,
     @database.motion_lists b
WHERE a.motion_list_id = b.motion_list_id
  AND b.motion_list_name = "@motion_list_name"