SELECT DISTINCT
  a.motion_list_id,
  b.motion_list_name,
  b.motion_count,
  b.data_motion_count,
  b.created_datetime
FROM @database.motion_list_assignments a,
     @database.motion_lists b,
     motion.motion_device_types c,
     @database.motion_run_time d
WHERE a.data_run_id = @data_run_id
  AND a.motion_list_id = b.motion_list_id
  AND b.motion_device_type_id = c.motion_device_type_id
  AND c.motion_device_type_name = "@motion_device_type_name"
  AND a.data_run_id = d.data_run_id
  AND a.motion_list_id = d.motion_list_id
