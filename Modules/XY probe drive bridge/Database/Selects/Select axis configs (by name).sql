/*** Select axis configs (by name).sql ***/

SELECT
  a.configuration_name,
  a.axis_count,
  a.created_datetime
FROM @database.axis_configurations a
WHERE a.configuration_name = "@configuration_name"
