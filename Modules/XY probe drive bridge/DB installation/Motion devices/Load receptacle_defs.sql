INSERT INTO motion.receptacle_defs
  (receptacle_id, six_k_controller, axis)
SELECT a.receptacle_id,
       @six_k_controller,
       @axis
FROM motion.receptacles a
WHERE a.receptacle_number = @receptacle_number;
