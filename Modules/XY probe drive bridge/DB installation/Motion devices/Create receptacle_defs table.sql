CREATE TABLE IF NOT EXISTS motion.receptacle_defs
(
  receptacle_id TINYINT UNSIGNED NOT NULL,
  six_k_controller TINYINT UNSIGNED NOT NULL,
  axis TINYINT UNSIGNED NOT NULL,

  INDEX receptacle_defs_pk (receptacle_id, six_k_controller, axis),
  PRIMARY KEY (receptacle_id, six_k_controller, axis),

  INDEX receptacle_defs_fk1 (receptacle_id),
  FOREIGN KEY (receptacle_id) REFERENCES motion.receptacles(receptacle_id)

);


