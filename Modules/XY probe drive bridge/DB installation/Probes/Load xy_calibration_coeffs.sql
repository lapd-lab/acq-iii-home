INSERT INTO motion.xy_calibration_coeffs
  (xy_calibration_id, theta_coeff_order, r_coeff_order, coeff)
SELECT xy_calibration_id, @theta_coeff_order, @r_coeff_order, @coeff
FROM motion.xy_calibrations
WHERE xy_calibration_name = "@xy_calibration_name"
