CREATE TABLE IF NOT EXISTS motion.probe_types
(
  probe_type_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  probe_type_name VARCHAR(120) NOT NULL,

  INDEX probe_types_pk (probe_type_id),
  PRIMARY KEY (probe_type_id),

  UNIQUE INDEX probe_types_ui1 (probe_type_name)
);


