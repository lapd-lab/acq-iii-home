CREATE TABLE IF NOT EXISTS motion.probes
(
  probe_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  probe_type_id SMALLINT UNSIGNED NOT NULL,
  probe_name VARCHAR(120) NOT NULL,
  xy_calibration_id SMALLINT UNSIGNED NOT NULL,
  channel_count SMALLINT UNSIGNED NOT NULL,
  created_datetime DOUBLE NOT NULL,
  status VARCHAR(120) NOT NULL DEFAULT "Active",

  INDEX probes_pk (probe_id),
  PRIMARY KEY (probe_id),

  INDEX probes_fk1 (probe_type_id),
  FOREIGN KEY (probe_type_id) REFERENCES motion.probe_types(probe_type_id),

  INDEX probes_fk2 (xy_calibration_id),
  FOREIGN KEY (xy_calibration_id) REFERENCES motion.xy_calibrations(xy_calibration_id),

  UNIQUE INDEX probes_ui1 (probe_name),
);


