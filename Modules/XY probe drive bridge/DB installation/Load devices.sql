INSERT INTO configuration.devices
  (
  device_name,
  device_type_id,
  device_description,
  device_bridge_vi_path,
  device_module_ip_address,
  device_module_vi_path,
  created_datetime
  )
SELECT "6K Compumotor",
       a.device_type_id,
       "Provides general motion control using the 6K Compumotor motor controller.  Use to access XY probe drives and raw 2-axis motion devices.",
       "@ACQ_II_home\\Modules\\Motion\\Motion bridge.vi",
       "@IP_address",
       "@ACQ_II_home\\Modules\\Motion\\Motion.vi",
       3168025745
FROM configuration.device_types a
WHERE a.device_type_name = "Motion"
