INSERT INTO configuration.devices
  (
  device_name,
  device_type_id,
  device_description,
  device_bridge_vi_path,
  device_module_ip_address,
  device_module_vi_path,
  created_datetime
  )
SELECT "GPIB interface",
       a.device_type_id,
       "Simple interface to create and send strings to GPIB devices.",
       "@ACQ_II_home\\Modules\\GPIB\\GPIB bridge.vi",
       "@IP_address",
       "@ACQ_II_home\\Modules\\GPIB\\GPIB interface.vi",
       3168025745
FROM configuration.device_types a
WHERE a.device_type_name = "Experiment control"

