/*** Delete gpib strings.sql ***/

DELETE @database.gpib_string_lists
FROM @database.gpib_configurations a,
     @database.gpib_string_lists b
WHERE a.configuration_name = "@configuration_name"
  AND a.configuration_id = b.configuration_id
