/*** Insert gpib assignment.sql ***/

INSERT INTO @database.gpib_assignments
  (
  data_run_id,
  configuration_id
  )
SELECT
  @data_run_id,
  a.configuration_id
FROM @database.gpib_configurations a
WHERE a.configuration_name = "@configuration_name"