/*** Insert gpib run-time state.sql ***/

INSERT INTO @database.gpib_run_time
  (
  data_run_id,
  shot_number,
  configuration_id,
  string_index
  )
VALUES
  (
  @data_run_id,
  @shot_number,
  @configuration_id,
  @string_index
  )
