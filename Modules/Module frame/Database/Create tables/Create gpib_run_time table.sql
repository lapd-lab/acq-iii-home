/*** Create gpib_run_time table.sql ***/

CREATE TABLE IF NOT EXISTS @database.gpib_run_time
(
  data_run_id SMALLINT UNSIGNED NOT NULL,
  shot_number INT UNSIGNED NOT NULL,

  configuration_id SMALLINT UNSIGNED NOT NULL,
  string_index INT UNSIGNED NOT NULL,

  INDEX gpib_run_time_pk (data_run_id, shot_number),
  PRIMARY KEY (data_run_id, shot_number),

  INDEX gpib_run_time_fk1 (data_run_id),
  FOREIGN KEY (data_run_id) REFERENCES @database.data_runs(data_run_id),

  INDEX gpib_run_time_fk2 (configuration_id),
  FOREIGN KEY (configuration_id) REFERENCES @database.gpib_configurations(configuration_id)
);


