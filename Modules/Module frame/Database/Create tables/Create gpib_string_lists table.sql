/*** Create gpib_string_lists table.sql ***/

CREATE TABLE IF NOT EXISTS @database.gpib_string_lists
(
  configuration_id SMALLINT UNSIGNED NOT NULL,
  string_index INT UNSIGNED NOT NULL,

  gpib_string TEXT NOT NULL,

  INDEX gpib_strings_pk (configuration_id, string_index),
  PRIMARY KEY (configuration_id, string_index)
);


