/***  Create gpib_configurations table.sql ***/

CREATE TABLE IF NOT EXISTS @database.gpib_configurations
(
  configuration_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  configuration_name VARCHAR(120) NOT NULL,
  gpib_channel SMALLINT UNSIGNED NOT NULL,
  string_count INT UNSIGNED NOT NULL,
  created_datetime DOUBLE NOT NULL,

  INDEX gpib_configurations_pk (configuration_id),
  PRIMARY KEY (configuration_id),

  UNIQUE INDEX gpib_configurations_ui1 (configuration_name)
);
