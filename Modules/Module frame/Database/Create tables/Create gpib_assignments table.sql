/*** Create gpib_assignments table.sql ***/

CREATE TABLE IF NOT EXISTS @database.gpib_assignments
(
  data_run_id SMALLINT UNSIGNED NOT NULL,
  configuration_id SMALLINT UNSIGNED NOT NULL,

  INDEX gpib_assignments_pk (data_run_id, configuration_id),
  PRIMARY KEY (data_run_id, configuration_id),

  INDEX gpib_assignments_fk1 (data_run_id),
  FOREIGN KEY (data_run_id) REFERENCES @database.data_runs(data_run_id),

  INDEX gpib_assignments_fk2 (configuration_id),
  FOREIGN KEY (configuration_id) REFERENCES @database.gpib_configurations(configuration_id)
);
