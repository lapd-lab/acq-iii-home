/*** Select run-time configurations.sql ***/

SELECT DISTINCT
  b.configuration_id,
  b.configuration_name
FROM @database.gpib_run_time a,
     @database.gpib_configurations b
WHERE a.data_run_id = @data_run_id
  AND a.configuration_id = b.configuration_id
