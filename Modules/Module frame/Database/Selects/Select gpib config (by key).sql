/*** Select gpib config (by key).sql ***/

SELECT
  a.configuration_id,
  a.configuration_name,
  a.gpib_channel,
  a.string_count,
  a.created_datetime
FROM @database.gpib_configurations a
WHERE a.configuration_id = @configuration_id
