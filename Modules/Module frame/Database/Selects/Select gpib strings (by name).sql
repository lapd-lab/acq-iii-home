/*** Select gpib strings (by name).sql ***/

SELECT
  b.string_index,
  b.gpib_string
FROM @database.gpib_configurations a, @database.gpib_string_lists b
WHERE a.configuration_name = "@configuration_name"
  AND b.configuration_id = a.configuration_id
ORDER BY b.string_index
