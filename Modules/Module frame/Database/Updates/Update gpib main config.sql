/*** Update gpib main config.sql ***/

UPDATE @database.gpib_configurations
SET gpib_channel       = @gpib_channel,
    string_count       = @string_count,
    created_datetime   = @created_datetime
WHERE
  configuration_name = "@configuration_name"
