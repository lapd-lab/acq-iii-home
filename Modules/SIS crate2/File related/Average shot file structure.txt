Average shot file structure
===========================

     4 bytes:  Configuration string length (nchars)
nchars bytes:  Configuration string
     4 bytes:  Channel count (nchan)

nchan blocks follow:

         4 bytes:  Channel string length (nchars)
    nchars bytes:  Channel string
         2 bytes:  Channel mode (see note 1)
         2 bytes:  Average count
         2 bytes:  Average shot number
         4 bytes:  Vertical scale (float)
         4 bytes:  Vertical offset (float)
         1 byte :  Enabled code (see note 2)
         4 bytes:  Data length (nsamples)
nsamples*4 bytes:  Raw U32 data


Only channels that are still building up average shots are included in the file.

Note 1: Channel mode
  0 -- SIS 3305 8 channels (clock X1)
  1 -- SIS 3305 4 channels (clock X2)
  2 -- SIS 3305 2 channels (clock X4)
  3 -- SIS 3302

Note 2: Enabled code
  For SIS 3302, always 1
  For SIS 3305, bit mask showing which channels of the FPGA are enabled.
  Remember that the data from a single FPGA are written as a single "channel".

