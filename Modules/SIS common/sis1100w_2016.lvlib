﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,(!!!*Q(C=\:;^CF."&amp;-@`[QKL:7I&lt;#=)7+=]LJ,Z&lt;B8W$]QIJ82;,,)D)QI*(!G+&lt;6AR##GPB6.PH&amp;@)+V^_&gt;/^H6R716&amp;#W=O?&gt;_`-`(`/\-Z"+JN%@31\8X.?VL)`T0&gt;L@2&gt;?&gt;@5G8:,MNF?8/53`&amp;X$]M3U=759^OX_8XU.OOO`T&lt;\DH`?FPJ^@HO&lt;PRW`^S^XNHF8PZHP&lt;N@\X\]J`K:JWK&lt;JT`V2HZPZVN_\WX,5@OPPR7XS.R&amp;N^4=VY=@_ENGW&gt;]@PVL:``W:H_Z\PP`_P_%@\^^`I?F`\G:`XW&lt;V"@\B*"_I_2&amp;BCA4FG[FRNIC&gt;[IC&gt;[IC&gt;[I!&gt;[I!&gt;[I!?[ITO[ITO[ITO[I2O[I2O[I2N[[?B#&amp;\L1G:6E]'3AJ'B3)!E'2=EJY5FY%J[%BU=F0!F0QJ0Q*$S%+/&amp;*?"+?B#@B)5U*4]+4]#1]#1_F#EG7DAZ0QE.Z"4Q"4]!4]!1]$+G!*Q!)"AM+"U8!5/!-&lt;A+?A#@AY69"4]!4]!1]!1^O"4Q"4]!4]!1]J*2:C5,4&gt;82Y+#/(R_&amp;R?"Q?BY@3=HA=(I@(Y8&amp;Y'%Y/D]0D1$A$/M6"E*0E"$A0$I`$QU5/D]0D]$A]$A_OME*?:K;D[4I[0!;0Q70Q'$Q'$S6E]"A]"I`"9`"16A;0Q70Q'$Q'$U0*Y$&amp;Y$"Y$R"C5Y755-R+.)%-Q?$D+&lt;L'S3F&amp;)L(3J&lt;F\64;G[W61XE?LG5&amp;VUV=658346S6?&gt;6.8*5JU%V:&gt;4B6;&amp;52V%.&lt;E,V)&lt;T'FNB#WS'4&lt;%*.M;'W+",`=W"G]V'[`6;K^6+C]6#M^F-U_F5E]F%Y`&amp;9Q_&amp;1A](AZO`%5XL@$L&lt;@J40OM=.8]]PDV_]`HLZ]=X6[`O,K&gt;0\J]BD^[&amp;U@=XD_?(BU^G2S^0&lt;,Z/4S9H,SY7,YP00`KH8V0N^]F`[&amp;&lt;[-?K0XG&lt;RBT^"7Q3KF2!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Close.vi" Type="VI" URL="../VIs/Close.vi"/>
	<Item Name="Find No Of sis1100.vi" Type="VI" URL="../VIs/Find No Of sis1100.vi"/>
	<Item Name="Get Handle And Open.vi" Type="VI" URL="../VIs/Get Handle And Open.vi"/>
	<Item Name="Get Ident Version.vi" Type="VI" URL="../VIs/Get Ident Version.vi"/>
	<Item Name="Get Last Plx Error.vi" Type="VI" URL="../VIs/Get Last Plx Error.vi"/>
	<Item Name="Init sis3100.vi" Type="VI" URL="../VIs/Init sis3100.vi"/>
	<Item Name="Init.vi" Type="VI" URL="../VIs/Init.vi"/>
	<Item Name="Sdram Sharc Dma Read.vi" Type="VI" URL="../VIs/Sdram Sharc Dma Read.vi"/>
	<Item Name="Sdram Sharc Dma Write.vi" Type="VI" URL="../VIs/Sdram Sharc Dma Write.vi"/>
	<Item Name="Sdram Sharc Single Read.vi" Type="VI" URL="../VIs/Sdram Sharc Single Read.vi"/>
	<Item Name="Sdram Sharc Single Write.vi" Type="VI" URL="../VIs/Sdram Sharc Single Write.vi"/>
	<Item Name="sis310x Register For Irq.vi" Type="VI" URL="../VIs/sis310x Register For Irq.vi"/>
	<Item Name="sis310x Unregister From Irq.vi" Type="VI" URL="../VIs/sis310x Unregister From Irq.vi"/>
	<Item Name="sis310x Wait For Irq.vi" Type="VI" URL="../VIs/sis310x Wait For Irq.vi"/>
	<Item Name="sis1100 Control Read.vi" Type="VI" URL="../VIs/sis1100 Control Read.vi"/>
	<Item Name="sis1100 Control Write.vi" Type="VI" URL="../VIs/sis1100 Control Write.vi"/>
	<Item Name="sis1100w_2016.dll" Type="Document" URL="../sis1100w_2016.dll"/>
	<Item Name="sis3100 Control Read.vi" Type="VI" URL="../VIs/sis3100 Control Read.vi"/>
	<Item Name="sis3100 Control Write.vi" Type="VI" URL="../VIs/sis3100 Control Write.vi"/>
	<Item Name="Vme Dma Read.vi" Type="VI" URL="../VIs/Vme Dma Read.vi"/>
	<Item Name="Vme Dma Write.vi" Type="VI" URL="../VIs/Vme Dma Write.vi"/>
	<Item Name="Vme Single Read.vi" Type="VI" URL="../VIs/Vme Single Read.vi"/>
	<Item Name="Vme Single Write.vi" Type="VI" URL="../VIs/Vme Single Write.vi"/>
	<Item Name="Vme Sysreset.vi" Type="VI" URL="../VIs/Vme Sysreset.vi"/>
</Library>
