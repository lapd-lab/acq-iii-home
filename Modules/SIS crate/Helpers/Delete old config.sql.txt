delete from configuration.devices
where device_name = "SIS crate";


drop table Ac.sis_crate_3302_calibration;
drop table Ac.sis_crate_3305_calibration;
drop table Ac.sis_crate_configuration_text;


drop table Alfven_waves_and_striations.sis_crate_3302_calibration;
drop table Alfven_waves_and_striations.sis_crate_3305_calibration;
drop table Alfven_waves_and_striations.sis_crate_configuration_text;


drop table Ar.sis_crate_3302_calibration;
drop table Ar.sis_crate_3305_calibration;
drop table Ar.sis_crate_configuration_text;


drop table Axial_Plasma_Jets.sis_crate_3302_calibration;
drop table Axial_Plasma_Jets.sis_crate_3305_calibration;
drop table Axial_Plasma_Jets.sis_crate_configuration_text;


drop table Bortnik.sis_crate_3302_calibration;
drop table Bortnik.sis_crate_3305_calibration;
drop table Bortnik.sis_crate_configuration_text;


drop table Electron_Beam.sis_crate_3302_calibration;
drop table Electron_Beam.sis_crate_3305_calibration;
drop table Electron_Beam.sis_crate_configuration_text;


drop table Electron_holes.sis_crate_3302_calibration;
drop table Electron_holes.sis_crate_3305_calibration;
drop table Electron_holes.sis_crate_configuration_text;


drop table Fast_Ions.sis_crate_3302_calibration;
drop table Fast_Ions.sis_crate_3305_calibration;
drop table Fast_Ions.sis_crate_configuration_text;


drop table FUSION_CAMPAIGN.sis_crate_3302_calibration;
drop table FUSION_CAMPAIGN.sis_crate_3305_calibration;
drop table FUSION_CAMPAIGN.sis_crate_configuration_text;


drop table High_Power_RF.sis_crate_3302_calibration;
drop table High_Power_RF.sis_crate_3305_calibration;
drop table High_Power_RF.sis_crate_configuration_text;


drop table IAR.sis_crate_3302_calibration;
drop table IAR.sis_crate_3305_calibration;
drop table IAR.sis_crate_configuration_text;


drop table ICRF_campaign.sis_crate_3302_calibration;
drop table ICRF_campaign.sis_crate_3305_calibration;
drop table ICRF_campaign.sis_crate_configuration_text;


drop table Intermittent_turbulence.sis_crate_3302_calibration;
drop table Intermittent_turbulence.sis_crate_3305_calibration;
drop table Intermittent_turbulence.sis_crate_configuration_text;


drop table LaB6_Cathode.sis_crate_3302_calibration;
drop table LaB6_Cathode.sis_crate_3305_calibration;
drop table LaB6_Cathode.sis_crate_configuration_text;


drop table Langmuir_Mode_Conversion.sis_crate_3302_calibration;
drop table Langmuir_Mode_Conversion.sis_crate_3305_calibration;
drop table Langmuir_Mode_Conversion.sis_crate_configuration_text;


drop table LANL.sis_crate_3302_calibration;
drop table LANL.sis_crate_3305_calibration;
drop table LANL.sis_crate_configuration_text;


drop table LAPD_General.sis_crate_3302_calibration;
drop table LAPD_General.sis_crate_3305_calibration;
drop table LAPD_General.sis_crate_configuration_text;


drop table Laser_Plasma.sis_crate_3302_calibration;
drop table Laser_Plasma.sis_crate_3305_calibration;
drop table Laser_Plasma.sis_crate_configuration_text;


drop table Laser_Shock.sis_crate_3302_calibration;
drop table Laser_Shock.sis_crate_3305_calibration;
drop table Laser_Shock.sis_crate_configuration_text;


drop table Lif_cw.sis_crate_3302_calibration;
drop table Lif_cw.sis_crate_3305_calibration;
drop table Lif_cw.sis_crate_configuration_text;


drop table Long_Range_Correlations.sis_crate_3302_calibration;
drop table Long_Range_Correlations.sis_crate_3305_calibration;
drop table Long_Range_Correlations.sis_crate_configuration_text;


drop table Maggs_Experimental.sis_crate_3302_calibration;
drop table Maggs_Experimental.sis_crate_3305_calibration;
drop table Maggs_Experimental.sis_crate_configuration_text;


drop table MURI.sis_crate_3302_calibration;
drop table MURI.sis_crate_3305_calibration;
drop table MURI.sis_crate_configuration_text;


drop table Nonlinear_Alfven_Interactions.sis_crate_3302_calibration;
drop table Nonlinear_Alfven_Interactions.sis_crate_3305_calibration;
drop table Nonlinear_Alfven_Interactions.sis_crate_configuration_text;


drop table Plasma_Flow.sis_crate_3302_calibration;
drop table Plasma_Flow.sis_crate_3305_calibration;
drop table Plasma_Flow.sis_crate_configuration_text;


drop table red_breasted_zonal_flow.sis_crate_3302_calibration;
drop table red_breasted_zonal_flow.sis_crate_3305_calibration;
drop table red_breasted_zonal_flow.sis_crate_configuration_text;


drop table Rotation.sis_crate_3302_calibration;
drop table Rotation.sis_crate_3305_calibration;
drop table Rotation.sis_crate_configuration_text;


drop table Shakedown.sis_crate_3302_calibration;
drop table Shakedown.sis_crate_3305_calibration;
drop table Shakedown.sis_crate_configuration_text;


delete from Shakedown_2.data_run_assignments
where device_name = "SIS crate";

delete from Shakedown_2.experiments
where experiment_name = "SIS_Crate_2";

delete from Shakedown_2.data_runs
where experiment_id = 17;

delete from Shakedown_2.device_assignments
where device_id = 25;

delete from Shakedown_2.expanded_meta_seq_defs
where data_run_id >= 285;

delete from Shakedown_2.sequence_assignments
where data_run_id >= 285;

delete from Shakedown_2.sequence_defs
where data_run_id >= 285;

drop table Shakedown_2.sis_crate_3302_calibration;
drop table Shakedown_2.sis_crate_3305_calibration;
drop table Shakedown_2.sis_crate_configuration_text;


drop table SIAW.sis_crate_3302_calibration;
drop table SIAW.sis_crate_3305_calibration;
drop table SIAW.sis_crate_configuration_text;


drop table Thin_Current_Sheets.sis_crate_3302_calibration;
drop table Thin_Current_Sheets.sis_crate_3305_calibration;
drop table Thin_Current_Sheets.sis_crate_configuration_text;


drop table Turbulence_imaging.sis_crate_3302_calibration;
drop table Turbulence_imaging.sis_crate_3305_calibration;
drop table Turbulence_imaging.sis_crate_configuration_text;


drop table UIowa_Alfven_waves.sis_crate_3302_calibration;
drop table UIowa_Alfven_waves.sis_crate_3305_calibration;
drop table UIowa_Alfven_waves.sis_crate_configuration_text;


drop table Vincena_Experimental.sis_crate_3302_calibration;
drop table Vincena_Experimental.sis_crate_3305_calibration;
drop table Vincena_Experimental.sis_crate_configuration_text;


