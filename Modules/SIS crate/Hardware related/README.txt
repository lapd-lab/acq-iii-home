2011-07-12
==========
Deleting PlxApi631.dll and sis1100w.dll from ../SIS crate/Hardware related.
Copying over PlxApi.dll and sis1100w_debug.dll from C:\sis11003100vis260411.
Renaming sis1100w_debug.dll to sis1100w.dll.

SIS crate -- 1100 3100 init.vi returning an error when trying to get a handle.
Switch back to PlxApi631.dll.
OK, that doesn't work with sis1100w_debug.dll, have to switch both.

With both switched back, I do get the SIS3100 Ident (x2210202).
However, I also get:

Error 1097 occurred at Call Library Function Node in 
vme_a32d32_writesub.vi

Possible reason(s):

LabVIEW:  An exception occurred within the external code called by a 
Call Library Function Node. The exception might have corrupted the 
LabVIEW memory. Save any work to a new location and restart LabVIEW.



2012-04-15
==========
For running under 32-bit LabVIEW, use "sis1100w 2011-04-15" renamed "sis1100w".  (I know, it's a strange coincidence that the date of
this note and the date of the DLL are the same.)

2017-03-08
==========
In the process of trying to update to Windows 10 (64bit) with 32bit LabVIEW. Got new driver from SIS for Windows 10. and will try to update Plx9054.sys and Plx8311.sys
files as well.
