/*** Insert sis 3301 board config.sql ***/

INSERT INTO @database.sis_3301_boards
  (
  configuration_id,
  board_number,
  samples
  )
SELECT
  a.configuration_id,
  @board_number,
  @samples
FROM @database.sis_3301 a
WHERE a.configuration_name = "@configuration_name"