/*** Insert sis 3301 assign.sql ***/

INSERT INTO @database.sis_3301_assignments
  (
  data_run_id,
  configuration_id
  )
SELECT
  @data_run_id,
  a.configuration_id
FROM @database.sis_3301 a
WHERE a.configuration_name = "@configuration_name"
