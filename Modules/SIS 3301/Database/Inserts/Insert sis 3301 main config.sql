/*** Insert sis 3301 main config.sql ***/

INSERT INTO @database.sis_3301
  (
  configuration_name,
  clock_rate,
  trigger_mode,
  stop_delay,
  software_start,
  shots_to_average,
  samples_to_average,
  created_datetime
  )
VALUES
  (
  "@configuration_name",
  @clock_rate,
  @trigger_mode,
  @stop_delay,
  @software_start,
  @shots_to_average,
  @samples_to_average,
  @created_datetime
  )
