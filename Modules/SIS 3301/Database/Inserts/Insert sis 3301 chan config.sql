/*** Insert sis 3301 chan config.sql ***/

INSERT INTO @database.sis_3301_channels
  (
  configuration_id,
  board_number,
  channel_number,
  data_type,
  dc_offset
  )
SELECT
  a.configuration_id,
  @board_number,
  @channel_number,
  "@data_type",
  @dc_offset
FROM @database.sis_3301 a
WHERE a.configuration_name = "@configuration_name"