/*** Select other sis 3301 assigns.sql ***/

SELECT
  b.configuration_name,
  c.data_run_name
FROM @database.sis_3301_assignments a,
     @database.sis_3301 b,
     @database.data_runs c
WHERE a.data_run_id = c.data_run_id
  AND a.configuration_id = b.configuration_id
  AND b.configuration_name = "@configuration_name"
  AND a.data_run_id <> @data_run_id
