/*** Select run-time list.sql ***/

SELECT
  a.shot_number,
  b.configuration_name
FROM @database.sis_3301_run_time a,
     @database.sis_3301 b
WHERE a.data_run_id = @data_run_id
  AND a.configuration_id = b.configuration_id
ORDER BY a.shot_number
