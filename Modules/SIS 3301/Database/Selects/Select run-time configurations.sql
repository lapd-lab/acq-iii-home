/*** Select run-time configurations.sql ***/

SELECT DISTINCT
  b.configuration_name
FROM @database.sis_3301_run_time a,
     @database.sis_3301 b
WHERE a.data_run_id = @data_run_id
  AND a.configuration_id = b.configuration_id
