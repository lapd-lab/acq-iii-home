CREATE TABLE IF NOT EXISTS @database.sis_3301_run_time
(
  data_run_id SMALLINT UNSIGNED NOT NULL,
  shot_number INT UNSIGNED NOT NULL,

  configuration_id SMALLINT UNSIGNED NOT NULL,

  INDEX sis_3301_run_time_pk (data_run_id, shot_number),
  PRIMARY KEY (data_run_id, shot_number),

  INDEX sis_3301_run_time_fk1 (data_run_id),
  FOREIGN KEY (data_run_id) REFERENCES @database.data_runs(data_run_id),

  INDEX sis_3301_run_time_fk2 (configuration_id),
  FOREIGN KEY (configuration_id) REFERENCES @database.sis_3301(configuration_id)
);


