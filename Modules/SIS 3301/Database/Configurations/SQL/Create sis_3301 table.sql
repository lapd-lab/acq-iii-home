CREATE TABLE IF NOT EXISTS @database.sis_3301
(
  configuration_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  configuration_name VARCHAR(120) NOT NULL,
  clock_rate SMALLINT UNSIGNED NOT NULL,
  trigger_mode SMALLINT UNSIGNED NOT NULL,
  stop_delay SMALLINT UNSIGNED NOT NULL,
  software_start TINYINT NOT NULL,
  shots_to_average SMALLINT UNSIGNED NOT NULL,
  samples_to_average SMALLINT UNSIGNED NOT NULL,
  created_datetime DOUBLE NOT NULL,

  INDEX sis_3301_pk (configuration_id),
  PRIMARY KEY (configuration_id),

  UNIQUE INDEX sis_3301_ui1 (configuration_name)
);


