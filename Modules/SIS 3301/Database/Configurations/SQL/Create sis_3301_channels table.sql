CREATE TABLE IF NOT EXISTS @database.sis_3301_channels
(
  configuration_id SMALLINT UNSIGNED NOT NULL,
  board_number TINYINT UNSIGNED NOT NULL,
  channel_number TINYINT UNSIGNED NOT NULL,

  data_type VARCHAR(120) NOT NULL,
  dc_offset DOUBLE NOT NULL,

  INDEX sis_3301_channels_pk (configuration_id, board_number, channel_number),
  PRIMARY KEY (configuration_id, board_number, channel_number),

  INDEX sis_3301_channels_fk1 (configuration_id, board_number),
  FOREIGN KEY (configuration_id, board_number) REFERENCES @database.sis_3301_boards(configuration_id, board_number)
);


