CREATE TABLE IF NOT EXISTS @database.sis_3301_boards
(
  configuration_id SMALLINT UNSIGNED NOT NULL,
  board_number TINYINT UNSIGNED NOT NULL,

  samples INT UNSIGNED NOT NULL,

  INDEX sis_3301_boards_pk (configuration_id, board_number),
  PRIMARY KEY (configuration_id, board_number),

  INDEX sis_3301_boards_fk1 (configuration_id),
  FOREIGN KEY (configuration_id) REFERENCES @database.sis_3301(configuration_id)
);


