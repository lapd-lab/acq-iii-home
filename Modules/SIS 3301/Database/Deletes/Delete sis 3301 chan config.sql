/*** Delete sis 3301 chan config.sql ***/

DELETE @database.sis_3301_channels
FROM @database.sis_3301 a,
     @database.sis_3301_channels b
WHERE a.configuration_name = "@configuration_name"
  AND a.configuration_id = b.configuration_id
