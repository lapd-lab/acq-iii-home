/*** Delete sis 3301 board config.sql ***/

DELETE @database.sis_3301_boards
FROM @database.sis_3301 a,
     @database.sis_3301_boards b
WHERE a.configuration_name = "@configuration_name"
  AND a.configuration_id = b.configuration_id
