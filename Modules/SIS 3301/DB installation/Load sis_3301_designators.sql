INSERT INTO data_acquisition.sis_3301_designators
  SET channel_designator = "@channel_designator",
      board_number       = @board_number,
      channel_number     = @channel_number;
