CREATE TABLE IF NOT EXISTS data_acquisition.sis_3301_designators
(
  channel_designator_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  board_number TINYINT UNSIGNED NOT NULL,
  channel_number TINYINT UNSIGNED NOT NULL,
  channel_designator VARCHAR(120) NOT NULL,

  INDEX sis_3301_designators_pk (channel_designator_id),
  PRIMARY KEY (channel_designator_id),

  UNIQUE INDEX sis_3301_designators_ui (channel_designator),

);


