  ;Compare stored motor/encoder counts with actual
  IF(VARI3@axis=@axisPC AND VARI4@axis=@axisPE)
    ;OK response
    VARI1@axis = 0 ;

  ELSE
    ;Out of sync response
    VARI1@axis = 5400 ;

  NIF