
;Scaling Setup, must be outside a program
;Distance Units - counts,counts,counts,counts,counts,counts,counts,counts
SCLD 1,1,1,1,1,1,1,1
;Velocity Units - counts/s,counts/s,counts/s,counts/s,counts/s,counts/s,counts/s,counts/s
SCLV 1,1,1,1,1,1,1,1
;Acceleration Units - counts/s/s,counts/s/s,counts/s/s,counts/s/s,counts/s/s,counts/s/s,counts/s/s,counts/s/s
SCLA 1,1,1,1,1,1,1,1
SCALE1


;-------------
;Setup Program
DEL SETUP
DEF SETUP
FOLMAS 0,0,0,0,0,0,0,0
FOLEN00000000

  ;Disable drives
  DRIVE00000000

  ;Drive Setup, all steppers
  AXSDEF 00000000
  DRFLVL 11111111
  DRFEN 00000000

  DRES 20000,20000,20000,20000,20000,20000,20000,20000
  PULSE  0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3
  DSTALL 00000000

  ;Feedback Setup
  ERES 400,2000,400,2000,400,2000,400,2000
  EFAIL 11111111
  ENCPOL 00000000
  ENCSND 00000000
  ESTALL 11111111
  ESDB 4,4,4,4,4,4,4,4
  ESK 11111111
  ENCCNT 11111111

  ;Hardware Limit Setup
  LH 3,3,3,3,3,3,3,3
  LHAD 1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000

  ;Software Limit Setup
  LS 0,0,0,0,0,0,0,0
  LSAD 1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000
  LSNEG 0,0,0,0,0,0,0,0
  LSPOS 0,0,0,0,0,0,0,0

  ;Home Limit Setup
  HOMA 10,10,10,10,10,10,10,10
  HOMAA 10,10,10,10,10,10,10,10
  HOMV 1,1,1,1,1,1,1,1
  HOMAD 10,10,10,10,10,10,10,10
  HOMADA 10,10,10,10,10,10,10,10
  HOMBAC 00000000
  HOMZ 00000000
  HOMDF 00000000
  HOMVF 0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1
  HOMEDG 00000000

  LIMLVL 000000000000000000000000

  ;Command Processing During Motion
  COMEXC 1
  ;Command Processing on Stop
  COMEXS 1
  ;Command Processing on Limit
  COMEXL 11111111
  ;Error Setup
  ;To modify the error bits,
  ;Please double-click on your Error Program
  
  ; Error program setup
  ERRORP CRASH ; Error program setup
  ERROR XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX ; Bits tested

  ; Assign axes to separate tasks
  1%TSKAX1,1
  2%TSKAX2,2
  3%TSKAX3,3
  4%TSKAX4,4
  5%TSKAX5,5
  6%TSKAX6,6
  7%TSKAX7,7
  8%TSKAX8,8
END

;-------------
;Main Program
DEL MAIN
DEF MAIN

  GOSUB SETUP
END
STARTP MAIN

;-------------
;Error Program
DEL CRASH
DEF CRASH

END

;Preset move for axis 1
DEL MOVE1
DEF MOVE1

  DRIVE 1XXXXXXX
  GO 10000000
  WAIT(MOV=b0)
  T.1
  VARI31 = 1PC ;
  VARI41 = 1PE ;
  DRIVE 0XXXXXXX

END

;Preset move for axis 2
DEL MOVE2
DEF MOVE2

  DRIVE X1XXXXXX
  GO 01000000
  WAIT(MOV=bX0)
  T.1
  VARI32 = 2PC ;
  VARI42 = 2PE ;
  DRIVE X0XXXXXX

END

;Preset move for axis 3
DEL MOVE3
DEF MOVE3

  DRIVE XX1XXXXX
  GO 00100000
  WAIT(MOV=bXX0)
  T.1
  VARI33 = 3PC ;
  VARI43 = 3PE ;
  DRIVE XX0XXXXX

END

;Preset move for axis 4
DEL MOVE4
DEF MOVE4

  DRIVE XXX1XXXX
  GO 00010000
  WAIT(MOV=bXXX0)
  T.1
  VARI34 = 4PC ;
  VARI44 = 4PE ;
  DRIVE XXX0XXXX

END

;Preset move for axis 5
DEL MOVE5
DEF MOVE5

  DRIVE XXXX1XXX
  GO 00001000
  WAIT(MOV=bXXXX0)
  T.1
  VARI35 = 5PC ;
  VARI45 = 5PE ;
  DRIVE XXXX0XXX

END

;Preset move for axis 6
DEL MOVE6
DEF MOVE6

  DRIVE XXXXX1XX
  GO 00000100
  WAIT(MOV=bXXXXX0)
  T.1
  VARI36 = 6PC ;
  VARI46 = 6PE ;
  DRIVE XXXXX0XX

END

;Preset move for axis 7
DEL MOVE7
DEF MOVE7

  DRIVE XXXXXX1X
  GO 00000010
  WAIT(MOV=bXXXXXX0)
  T.1
  VARI37 = 7PC ;
  VARI47 = 7PE ;
  DRIVE XXXXXX0X

END

;Preset move for axis 8
DEL MOVE8
DEF MOVE8

  DRIVE XXXXXXX1
  GO 00000001
  WAIT(MOV=bXXXXXXX0)
  T.1
  VARI38 = 8PC ;
  VARI48 = 8PE ;
  DRIVE XXXXXXX0

END

;Continuous move for axis 1
DEL CMOV1
DEF CMOV1

  DRIVE 1XXXXXXX
  GO 10000000

END

;Continuous move for axis 2
DEL CMOV2
DEF CMOV2

  DRIVE X1XXXXXX
  GO 01000000

END

;Continuous move for axis 3
DEL CMOV3
DEF CMOV3

  DRIVE XX1XXXXX
  GO 00100000

END

;Continuous move for axis 4
DEL CMOV4
DEF CMOV4

  DRIVE XXX1XXXX
  GO 00010000

END

;Continuous move for axis 5
DEL CMOV5
DEF CMOV5

  DRIVE XXXX1XXX
  GO 00001000

END

;Continuous move for axis 6
DEL CMOV6
DEF CMOV6

  DRIVE XXXXX1XX
  GO 00000100

END

;Continuous move for axis 7
DEL CMOV7
DEF CMOV7

  DRIVE XXXXXX1X
  GO 00000010

END

;Continuous move for axis 8
DEL CMOV8
DEF CMOV8

  DRIVE XXXXXXX1
  GO 00000001

END

