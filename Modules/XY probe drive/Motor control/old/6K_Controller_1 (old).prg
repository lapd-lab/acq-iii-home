; WIZID = 00000020
;Product Setup Code
;  Wizard developed for 8 axes with a 6K8 using RS232 COM2

; WIZID = 00000080
;Scaling Setup
;Distance Units - counts,counts,counts,counts,counts,counts,counts,counts
SCLD 1,1,1,1,1,1,1,1
;Velocity Units - counts/s,counts/s,counts/s,counts/s,counts/s,counts/s,counts/s,counts/s
SCLV 1,1,1,1,1,1,1,1
;Acceleration Units - counts/s/s,counts/s/s,counts/s/s,counts/s/s,counts/s/s,counts/s/s,counts/s/s,counts/s/s
SCLA 1,1,1,1,1,1,1,1
SCALE1


; WIZID = 00000040
;-------------
;Setup Program
DEL SETUP
DEF SETUP
FOLMAS 0,0,0,0,0,0,0,0
FOLEN00000000

  ; WIZID = 00000060
  ;Enable Mode Code
  DRIVE00000000
  ; WIZID = 00000080
;Scaling Setup
;Because scaling commands are not allowed in a program,
;the scaling commands will be placed at the beginning
;of the program file.  This insures that motion programs
;in subsequent programs will be scaled correctly.

  ; WIZID = 00000100
  ;Drive Setup
  ;Axis 1, Stepper Control, No Drive
  ;Axis 2, Stepper Control, No Drive
  ;Axis 3, Stepper Control, No Drive
  ;Axis 4, Stepper Control, No Drive
  ;Axis 5, Stepper Control, No Drive
  ;Axis 6, Stepper Control, No Drive
  ;Axis 7, Stepper Control, No Drive
  ;Axis 8, Stepper Control, No Drive
  AXSDEF 00000000
  DRFLVL 11111111
  DRFEN 00000000

  DRES 1600,1600,1600,1600,1600,1600,1600,1600
  PULSE  0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3
  DSTALL 00000000

  ; WIZID = 00000120
  ;Feedback Setup
  ERES 400,400,400,400,400,400,400,400
  EFAIL 10000000
  ENCPOL 00000000
  ENCSND 00000000
  ESTALL 11111111
  ESDB 4,4,4,4,4,4,4,4
  ESK 00000000
  ENCCNT 11111111
  ; WIZID = 00000140
  ;Hardware Limit Setup
  LH 0,0,0,0,0,0,0,0
  LHAD 100,100,100,100,100,100,100,100
  LHADA 100,100,100,100,100,100,100,100

  ;Software Limit Setup
  LS 0,0,0,0,0,0,0,0
  LSAD 100,100,100,100,100,100,100,100
  LSADA 100,100,100,100,100,100,100,100
  LSNEG 0,0,0,0,0,0,0,0
  LSPOS 0,0,0,0,0,0,0,0

  ;Home Limit Setup
  HOMA 10,10,10,10,10,10,10,10
  HOMAA 10,10,10,10,10,10,10,10
  HOMV 1,1,1,1,1,1,1,1
  HOMAD 10,10,10,10,10,10,10,10
  HOMADA 10,10,10,10,10,10,10,10
  HOMBAC 00000000
  HOMZ 00000000
  HOMDF 00000000
  HOMVF 0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1
  HOMEDG 00000000

  LIMLVL 000000000000000000000000
  ; WIZID = 00000160
  ;Enable Mode Code
  DRIVE11111111
  ; WIZID = 00000180
  ;Command Processing Code

  ;Command Processing During Motion
  COMEXC 1
  ;Command Processing on Stop
  COMEXS 1
  ;Error Setup
  ;To modify the error bits,
  ;Please double-click on your Error Program
  
  ; Error program setup
  ERRORP CRASH ; Error program setup
  ERROR XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX ; Bits tested
END

; WIZID = 00000200
;-------------
;Main Program
DEL MAIN
DEF MAIN

  ; WIZID = 00000220
  GOSUB SETUP
END
STARTP MAIN

; WIZID = 00000240
;-------------
;Error Program
DEL CRASH
DEF CRASH

END

