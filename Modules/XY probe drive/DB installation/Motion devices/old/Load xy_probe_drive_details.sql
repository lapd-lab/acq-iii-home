INSERT INTO motion.xy_probe_drive_details
(
  motion_device_id,
  L_vert_arm,
  D_pivot_to_mount,
  D_perpendicular,
  L_eff_radial_arm,
  D_end_to_mount,
  sx_min,
  sy_min,
  sy_max
)
SELECT
  motion_device_id,
  @L_vert_arm,
  @D_pivot_to_mount,
  @D_perpendicular,
  @L_eff_radial_arm,
  @D_end_to_mount,
  @sx_min,
  @sy_min,
  @sy_max
FROM motion.motion_devices
WHERE motion_device_name = "@motion_device_name";



