@probe_name=B-field 3-axis #1,
@channel_name=Bx;
@probe_name=B-field 3-axis #1,
@channel_name=By;
@probe_name=B-field 3-axis #1,
@channel_name=Bz;

@probe_name=B-field 3-axis #2,
@channel_name=Bx;
@probe_name=B-field 3-axis #2,
@channel_name=By;
@probe_name=B-field 3-axis #2,
@channel_name=Bz;

@probe_name=B-field 3-axis #3,
@channel_name=Bx;
@probe_name=B-field 3-axis #3,
@channel_name=By;
@probe_name=B-field 3-axis #3,
@channel_name=Bz;

@probe_name=B-field 3-axis #4,
@channel_name=Bx;
@probe_name=B-field 3-axis #4,
@channel_name=By;
@probe_name=B-field 3-axis #4,
@channel_name=Bz;

@probe_name=B-field loop #1,
@channel_name=Channel 1;

@probe_name=B-field loop #2,
@channel_name=Channel 1;

@probe_name=E-field 2-axis #1,
@channel_name=Channel 1;
@probe_name=E-field 2-axis #1,
@channel_name=Channel 2;

@probe_name=E-field dipole #1,
@channel_name=delta V;

@probe_name=E-field dipole #2,
@channel_name=delta V;

@probe_name=E-field dipole #3,
@channel_name=delta V;

@probe_name=E-field micro #1,
@channel_name=delta V;

@probe_name=Langmuir #1,
@channel_name=face 1;
@probe_name=Langmuir #1,
@channel_name=face 2;

@probe_name=Langmuir #2,
@channel_name=Channel 1;
@probe_name=Langmuir #2,
@channel_name=Channel 2;
@probe_name=Langmuir #2,
@channel_name=Channel 3;
@probe_name=Langmuir #2,
@channel_name=Channel 4;
@probe_name=Langmuir #2,
@channel_name=Channel 5;
@probe_name=Langmuir #2,
@channel_name=Channel 6;
@probe_name=Langmuir #2,
@channel_name=Channel 7;
@probe_name=Langmuir #2,
@channel_name=Channel 8;

@probe_name=Langmuir #3,
@channel_name=Channel 1;
@probe_name=Langmuir #3,
@channel_name=Channel 2;
@probe_name=Langmuir #3,
@channel_name=Channel 3;
@probe_name=Langmuir #3,
@channel_name=Channel 4;
@probe_name=Langmuir #3,
@channel_name=Channel 5;
@probe_name=Langmuir #3,
@channel_name=Channel 6;
@probe_name=Langmuir #3,
@channel_name=Channel 7;
@probe_name=Langmuir #3,
@channel_name=Channel 8;

@probe_name=Langmuir #4,
@channel_name=Channel 1;
@probe_name=Langmuir #4,
@channel_name=Channel 2;
@probe_name=Langmuir #4,
@channel_name=Channel 3;
@probe_name=Langmuir #4,
@channel_name=Channel 4;

@probe_name=Langmuir #5,
@channel_name=Isat;
@probe_name=Langmuir #5,
@channel_name=Te;
@probe_name=Langmuir #5,
@channel_name=Vf;

@probe_name=LIF #1,
@channel_name=PMT 1;
@probe_name=LIF #1,
@channel_name=PMT 2;

