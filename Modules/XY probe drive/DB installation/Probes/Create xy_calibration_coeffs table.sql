CREATE TABLE IF NOT EXISTS motion.xy_calibration_coeffs
(
  xy_calibration_id SMALLINT UNSIGNED NOT NULL,
  theta_coeff_order SMALLINT UNSIGNED NOT NULL,
  r_coeff_order SMALLINT UNSIGNED NOT NULL,

  coeff DOUBLE NOT NULL,

  INDEX xy_calibration_coeffs_pk (xy_calibration_id, theta_coeff_order, r_coeff_order),
  PRIMARY KEY (xy_calibration_id, theta_coeff_order, r_coeff_order),

  INDEX xy_calibration_coeffs_fk1 (xy_calibration_id),
  FOREIGN KEY (xy_calibration_id) REFERENCES motion.xy_calibrations(xy_calibration_id)
);


