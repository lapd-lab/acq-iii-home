CREATE TABLE IF NOT EXISTS motion.xy_calibrations
(
  xy_calibration_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  xy_calibration_name VARCHAR(120) NOT NULL,
  theta_coeff_count SMALLINT UNSIGNED NOT NULL,
  r_coeff_count SMALLINT UNSIGNED NOT NULL,
  created_datetime DOUBLE NOT NULL,

  INDEX xy_calibrations_pk (xy_calibration_id),
  PRIMARY KEY (xy_calibration_id),

  UNIQUE INDEX probe_xy_calibrations_ui1 (xy_calibration_name)
);


