INSERT INTO motion.probes
  (probe_type_id, probe_name, xy_calibration_id, channel_count, created_datetime, status)
SELECT a.probe_type_id,
       "@probe_name",
       b.xy_calibration_id,
       @channel_count,
       3142504800,
       "Active"
FROM motion.probe_types a,
     motion.xy_calibrations b
WHERE a.probe_type_name = "@probe_type_name"
  AND b.xy_calibration_name = "@xy_calibration_name";
