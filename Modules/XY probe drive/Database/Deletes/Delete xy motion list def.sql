/*** Delete xy motion list def.sql ***/

DELETE @database.xy_motion_list_defs
FROM @database.xy_motion_list_defs a,
     @database.motion_lists b
WHERE a.motion_list_id = b.motion_list_id
  AND b.motion_list_name = "@motion_list_name"