/*** Insert axis details.sql ***/

INSERT INTO @database.axis_details
  (
  configuration_id,
  axis_index,
  motor_resolution,
  encoder_resolution,
  motor_velocity,
  motor_acceleration
  )
SELECT
  a.configuration_id,
  @axis_index,
  @motor_resolution,
  @encoder_resolution,
  @motor_velocity,
  @motor_acceleration
FROM @database.axis_configurations a
WHERE a.configuration_name = "@configuration_name"