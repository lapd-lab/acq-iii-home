INSERT INTO @database.xy_configurations
  (data_run_id, receptacle_id, port_number, port_location_id, probe_id, retracted_sx_cm, level_sy_cm)
SELECT
  @data_run_id,
  a.receptacle_id,
  @port_number,
  b.port_location_id,
  c.probe_id,
  @retracted_sx_cm,
  @level_sy_cm
FROM motion.receptacles a,
     motion.port_locations b,
     motion.probes c
WHERE a.receptacle_name = "@receptacle_name"
  AND b.port_location_name = "@port_location_name"
  AND c.probe_name = "@probe_name"

