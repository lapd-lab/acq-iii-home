SELECT
  a.physical_setup_id,
  a.saved_datetime
FROM motion.xy_physical_setup_ids a
WHERE a.saved_datetime = @max_saved_datetime
