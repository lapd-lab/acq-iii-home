/*** Select motion run-time.sql **/

SELECT
  a.motion_list_id,
  a.motion_index
FROM @database.motion_run_time a,
     motion.receptacles b
WHERE a.data_run_id = @data_run_id
  AND a.receptacle_id = b.receptacle_id
  AND b.receptacle_number = @receptacle_number
  AND a.shot_number = @shot_number
