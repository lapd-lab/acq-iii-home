/*** Select raw 2-axis assigns.sql ***/

SELECT
  a.receptacle_id,
  b.receptacle_number,
  a.configuration_id,
  c.configuration_name
FROM @database.raw_2_axis_assigns a,
     motion.receptacles b,
     @database.axis_configurations c
WHERE a.data_run_id = @data_run_id
  AND a.receptacle_id = b.receptacle_id
  AND a.configuration_id = c.configuration_id
