/*** Select other raw 2-axis assigns.sql ***/

SELECT
  b.data_run_name
FROM @database.raw_2_axis_assigns a,
     @database.data_runs b,
     @database.axis_configurations c
WHERE a.data_run_id = b.data_run_id
  AND a.configuration_id = c.configuration_id
  AND c.configuration_name = "@configuration_name"
  AND b.data_run_id <> @data_run_id
