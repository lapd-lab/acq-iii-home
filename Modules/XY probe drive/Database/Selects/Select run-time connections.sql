SELECT DISTINCT
  a.receptacle_id,
  b.receptacle_number
FROM @database.motion_assignments a,
     motion.receptacles b,
     motion.motion_device_types c,
     @database.motion_run_time d
WHERE a.data_run_id = @data_run_id
  AND a.receptacle_id = b.receptacle_id
  AND a.motion_device_type_id = c.motion_device_type_id
  AND c.motion_device_type_name = "XY probe drive"
  AND a.data_run_id = d.data_run_id
  AND a.receptacle_id = d.receptacle_id
ORDER BY b.receptacle_number
