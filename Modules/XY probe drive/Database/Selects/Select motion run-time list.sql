/*** Select motion run-time list.sql **/

SELECT
  a.shot_number,
  b.x,
  b.y,
  c.port_number,
  d.motion_list_name,
  e.probe_name
FROM @database.motion_run_time a,
     @database.xy_motions b,
     @database.xy_probe_drives c,
     @database.motion_lists d,
     motion.probes e
WHERE a.data_run_id = @data_run_id
  AND a.motion_list_id = b.motion_list_id
  AND a.motion_index + 1 = b.motion_number /* Don't try to understand this; b.motion_number should be
	  /* called b.motion_index and a.motion_index is stored incorrectly, it should be increased by 1 */
  AND a.data_run_id = c.data_run_id
  AND a.receptacle_id = c.receptacle_id
  AND a.motion_list_id = d.motion_list_id
  AND c.probe_id = e.probe_id
ORDER BY a.shot_number
