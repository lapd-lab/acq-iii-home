CREATE TABLE IF NOT EXISTS motion.xy_probe_drive_details /* physical details of probe construction and safety limits */
(
  motion_device_id SMALLINT(5) UNSIGNED NOT NULL,

  L_vert_arm DOUBLE(7,2) NOT NULL, /* length of vertical arm, which extends from the upper pivot to the radial
                                   /* arm mount point, in cm */
  D_pivot_to_mount DOUBLE(7,2) NOT NULL, /* distance from the ball pivot point to the point along the probe center line
                                         /* perpendicular to the radial arm mount point, in cm */
  D_perpendicular DOUBLE(7,2) NOT NULL, /* perpendicular distance from the probe center line to the radial arm mount point,
                                        /* in cm */
  L_eff_radial_arm DOUBLE(7,2) NOT NULL, /* effective length of the radial arm, i.e. the length along which the probe can
                                         /* travel, in cm */
  D_end_to_mount DOUBLE(7,2) NOT NULL, /* distance from the effective end of the radial arm (probe at maximum extension
                                       /* into the chamber) to the point perpendicular to the radial mount point, in cm */
  sx_min DOUBLE(7,2) NOT NULL, /* cm */
                               /* sx_min + L_eff_radial_arm --> sx_max */
  sy_min DOUBLE(7,2) NOT NULL, /* cm */
  sy_max DOUBLE(7,2) NOT NULL, /* cm */
                               /* sy increases as the arm is raised (must be confirmed) */

  INDEX xy_probe_drive_details_pk (motion_device_id),
  PRIMARY KEY (motion_device_id),

  INDEX xy_probe_drive_details_fk1 (motion_device_id),
  FOREIGN KEY (motion_device_id) REFERENCES motion.motion_devices(motion_device_id),
);


