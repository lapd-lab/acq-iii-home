CREATE TABLE IF NOT EXISTS motion.motion_devices
(
  motion_device_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  motion_device_type_id SMALLINT UNSIGNED NOT NULL,
  motion_device_name VARCHAR(120) NOT NULL,
  created_datetime DOUBLE NOT NULL,
  status VARCHAR(120) NOT NULL DEFAULT "Active",

  INDEX motion_devices_pk (motion_device_id),
  PRIMARY KEY (motion_device_id),

  UNIQUE INDEX motion_devices_ui1 (motion_device_name),
);


