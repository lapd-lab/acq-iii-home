CREATE TABLE IF NOT EXISTS motion.motion_device_types
(
  motion_device_type_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,

  motion_device_type_name VARCHAR(120) NOT NULL,

  INDEX motion_devices_pk (motion_device_type_id),
  PRIMARY KEY (motion_device_type_id),

  UNIQUE INDEX motion_device_types_ui1 (motion_device_type_name),
);


