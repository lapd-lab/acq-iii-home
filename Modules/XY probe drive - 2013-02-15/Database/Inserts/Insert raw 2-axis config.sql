/*** Insert raw 2-axis config.sql ***/

INSERT INTO @database.axis_configurations
  (
  configuration_name,
  axis_count,
  created_datetime
  )
VALUES
  (
  "@configuration_name",
  @axis_count,
  @created_datetime
  )
