SELECT
  a.data_run_id,
  a.receptacle_id,
  a.port_number,
  a.port_location_id,
  b.port_location_name,
  a.probe_id,
  c.probe_name,
  a.retracted_sx_cm,
  a.level_sy_cm
FROM @database.xy_configurations a,
     motion.port_locations b,
     motion.probes c
WHERE a.port_location_id = b.port_location_id
  AND a.probe_id = c.probe_id
