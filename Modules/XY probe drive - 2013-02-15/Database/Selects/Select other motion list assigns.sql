/*** Select other motion list assigns.sql ***/

SELECT
  b.motion_list_name,
  c.data_run_name
FROM @database.motion_list_assignments a,
     @database.motion_lists b,
     @database.data_runs c
WHERE a.data_run_id = c.data_run_id
  AND a.motion_list_id = b.motion_list_id
  AND b.motion_list_name = "@motion_list_name"
  AND a.data_run_id <> @data_run_id
