SELECT
  a.data_run_id,
  a.receptacle_id,
  b.receptacle_number,
  c.motion_device_type_name
FROM @database.motion_assignments a,
     motion.receptacles b,
     motion.motion_device_types c
WHERE a.data_run_id = @data_run_id
  AND a.receptacle_id = b.receptacle_id
  AND a.motion_device_type_id = c.motion_device_type_id
ORDER BY b.receptacle_number
