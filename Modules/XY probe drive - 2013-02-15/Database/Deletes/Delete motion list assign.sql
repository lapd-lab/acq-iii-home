/*** Delete motion list assign.sql ***/

DELETE @database.motion_list_assignments
FROM @database.motion_list_assignments a,
     @database.motion_lists b
WHERE a.data_run_id = @data_run_id
  AND a.motion_list_id = b.motion_list_id
  AND b.motion_list_name = "@motion_list_name"