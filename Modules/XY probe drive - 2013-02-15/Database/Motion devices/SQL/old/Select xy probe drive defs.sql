SELECT
  a.motion_device_id,
  a.motion_device_name,
  a.created_datetime,
  c.L_vert_arm,
  c.D_pivot_to_mount,
  c.D_perpendicular,
  c.L_eff_radial_arm,
  c.D_end_to_mount,
  c.sx_min,
  c.sy_min,
  c.sy_max
FROM motion.motion_devices a,
     motion.motion_device_types b,
     motion.xy_probe_drive_details c
WHERE a.motion_device_type_id = b.motion_device_type_id
  AND a.status = "Active"
  AND b.motion_device_type_name = "XY probe drive"
  AND a.motion_device_id = c.motion_device_id