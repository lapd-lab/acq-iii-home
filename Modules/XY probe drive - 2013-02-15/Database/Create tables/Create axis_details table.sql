/*** Create axis_details table.sql ***/

CREATE TABLE IF NOT EXISTS @database.axis_details
(
  configuration_id SMALLINT UNSIGNED NOT NULL,
  axis_index SMALLINT UNSIGNED NOT NULL,

  motor_resolution INT UNSIGNED NOT NULL, /* counts/rev */
  encoder_resolution INT UNSIGNED NOT NULL, /* counts/rev */
  motor_velocity INT UNSIGNED NOT NULL, /* counts/s */
  motor_acceleration INT UNSIGNED NOT NULL, /* counts/s/s */

  INDEX axis_details_pk (configuration_id, axis_index),
  PRIMARY KEY (configuration_id, axis_index),
);


