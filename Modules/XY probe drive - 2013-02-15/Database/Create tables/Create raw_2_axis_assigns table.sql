/*** Create raw_2_axis_assigns table.sql ***/

CREATE TABLE IF NOT EXISTS @database.raw_2_axis_assigns
(
  data_run_id SMALLINT UNSIGNED NOT NULL,
  receptacle_id TINYINT UNSIGNED NOT NULL,

  configuration_id TINYINT UNSIGNED NOT NULL,

  INDEX raw_2_axis_assigns_pk (data_run_id, receptacle_id),
  PRIMARY KEY (data_run_id, receptacle_id),

  INDEX raw_2_axis_assigns_fk1 (data_run_id),
  FOREIGN KEY (data_run_id) REFERENCES @database.data_runs(data_run_id),

  INDEX raw_2_axis_assigns_fk2 (receptacle_id),
  FOREIGN KEY (receptacle_id) REFERENCES motion.receptacles(receptacle_id),

  INDEX raw_2_axis_assigns_fk3 (configuration_id),
  FOREIGN KEY (configuration_id) REFERENCES @database.axis_configurations(configuration_id),
);


