/***  Create axis_configurations table.sql ***/

CREATE TABLE IF NOT EXISTS @database.axis_configurations
(
  configuration_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  configuration_name VARCHAR(120) NOT NULL,
  axis_count SMALLINT UNSIGNED NOT NULL,
  created_datetime DOUBLE NOT NULL,

  INDEX axis_configurations_pk (configuration_id),
  PRIMARY KEY (configuration_id),

  UNIQUE INDEX axis_configurations_ui1 (configuration_name)
);
