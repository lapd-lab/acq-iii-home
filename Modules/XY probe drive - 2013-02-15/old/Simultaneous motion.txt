Would it work to have one VI for each receptacle, running in sub-panels?

Or how about a VI for each motion device type, which would run in sub-panels?  These VI's would have 4 independent loops
(one for each receptacle) plus a control loop.


No.  Don't want independent loops because of the possibility of mixing up inputs on simultaneous calls to MC client.