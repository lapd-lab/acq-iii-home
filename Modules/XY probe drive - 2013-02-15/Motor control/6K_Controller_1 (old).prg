; WIZID = 00000020
;Product Setup Code
;  Wizard developed for 8 axes with a 6K8 using RS232 COM2

; WIZID = 00000080
;Scaling Setup
;Distance Units - counts,counts,counts,counts,counts,counts,counts,counts
SCLD 1,1,1,1,1,1,1,1
;Velocity Units - counts/s,counts/s,counts/s,counts/s,counts/s,counts/s,counts/s,counts/s
SCLV 1,1,1,1,1,1,1,1
;Acceleration Units - counts/s/s,counts/s/s,counts/s/s,counts/s/s,counts/s/s,counts/s/s,counts/s/s,counts/s/s
SCLA 1,1,1,1,1,1,1,1
SCALE1


; WIZID = 00000040
;-------------
;Setup Program
DEL SETUP
DEF SETUP
FOLMAS 0,0,0,0,0,0,0,0
FOLEN00000000

  ; WIZID = 00000060
  ;Enable Mode Code
  DRIVE00000000
  ; WIZID = 00000080
;Scaling Setup
;Because scaling commands are not allowed in a program,
;the scaling commands will be placed at the beginning
;of the program file.  This insures that motion programs
;in subsequent programs will be scaled correctly.

  ; WIZID = 00000100
  ;Drive Setup
  ;Axis 1, Stepper Control, No Drive
  ;Axis 2, Stepper Control, No Drive
  ;Axis 3, Stepper Control, No Drive
  ;Axis 4, Stepper Control, No Drive
  ;Axis 5, Stepper Control, No Drive
  ;Axis 6, Stepper Control, No Drive
  ;Axis 7, Stepper Control, No Drive
  ;Axis 8, Stepper Control, No Drive
  AXSDEF 00000000
  DRFLVL 11111111
  DRFEN 00000000

  DRES 20000,20000,20000,20000,20000,20000,20000,20000
  PULSE  0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3
  DSTALL 00000000

  ; WIZID = 00000120
  ;Feedback Setup
  ERES 400,2000,400,2000,400,2000,400,2000
  EFAIL 11111111
  ENCPOL 00000000
  ENCSND 00000000
  ESTALL 11111111
  ESDB 4,4,4,4,4,4,4,4
  ESK 11111111
  ENCCNT 11111111
  ; WIZID = 00000140
  ;Hardware Limit Setup
  LH 3,3,3,3,3,3,3,3
  LHAD 1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000

  ;Software Limit Setup
  LS 0,0,0,0,0,0,0,0
  LSAD 1000000,1000000,1000000,1000000,1000000,1000000,1000000,1000000
  LSNEG 0,0,0,0,0,0,0,0
  LSPOS 0,0,0,0,0,0,0,0

  ;Home Limit Setup
  HOMA 10,10,10,10,10,10,10,10
  HOMAA 10,10,10,10,10,10,10,10
  HOMV 1,1,1,1,1,1,1,1
  HOMAD 10,10,10,10,10,10,10,10
  HOMADA 10,10,10,10,10,10,10,10
  HOMBAC 00000000
  HOMZ 00000000
  HOMDF 00000000
  HOMVF 0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1
  HOMEDG 00000000

  LIMLVL 000000000000000000000000
  ; WIZID = 00000160
  ;Enable Mode Code
  DRIVE11111111
  ; WIZID = 00000180
  ;Command Processing Code

  ;Command Processing During Motion
  COMEXC 1
  ;Command Processing on Stop
  COMEXS 1
  ;Command Processing on Limit
  COMEXL 11111111
  ;Error Setup
  ;To modify the error bits,
  ;Please double-click on your Error Program
  
  ; Error program setup
  ERRORP CRASH ; Error program setup
  ERROR XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX ; Bits tested

  1%TSKAX1,1
  2%TSKAX2,2
  3%TSKAX3,3
  4%TSKAX4,4
  5%TSKAX5,5
  6%TSKAX6,6
  7%TSKAX7,7
  8%TSKAX8,8
END

; WIZID = 00000200
;-------------
;Main Program
DEL MAIN
DEF MAIN

  ; WIZID = 00000220
  GOSUB SETUP
END
STARTP MAIN

; WIZID = 00000240
;-------------
;Error Program
DEL CRASH
DEF CRASH

END

; WIZID = 00000260
;-------------
;User Program
;Preset move for axis 1
DEL MOVE1
DEF MOVE1

  GO 10000000
  WAIT(MOV=b0)
  T.1
  VARI31 = 1PC ;
  VARI41 = 1PE ;

END

; WIZID = 00000260
;-------------
;User Program
;Preset move for axis 2
DEL MOVE2
DEF MOVE2

  GO 01000000
  WAIT(MOV=bX0)
  T.1
  VARI32 = 2PC ;
  VARI42 = 2PE ;

END

; WIZID = 00000260
;-------------
;User Program
;Preset move for axis 3
DEL MOVE3
DEF MOVE3

  GO 00100000
  WAIT(MOV=bXX0)
  T.1
  VARI33 = 3PC ;
  VARI43 = 3PE ;

END

; WIZID = 00000260
;-------------
;User Program
;Preset move for axis 4
DEL MOVE4
DEF MOVE4

  GO 00010000
  WAIT(MOV=bXXX0)
  T.1
  VARI34 = 4PC ;
  VARI44 = 4PE ;

END

; WIZID = 00000260
;-------------
;User Program
;Preset move for axis 5
DEL MOVE5
DEF MOVE5

  GO 00001000
  WAIT(MOV=bXXXX0)
  T.1
  VARI35 = 5PC ;
  VARI45 = 5PE ;

END

; WIZID = 00000260
;-------------
;User Program
;Preset move for axis 6
DEL MOVE6
DEF MOVE6

  GO 00000100
  WAIT(MOV=bXXXXX0)
  T.1
  VARI36 = 6PC ;
  VARI46 = 6PE ;

END

; WIZID = 00000260
;-------------
;User Program
;Preset move for axis 7
DEL MOVE7
DEF MOVE7

  GO 00000010
  WAIT(MOV=bXXXXXX0)
  T.1
  VARI37 = 7PC ;
  VARI47 = 7PE ;

END

; WIZID = 00000260
;-------------
;User Program
;Preset move for axis 8
DEL MOVE8
DEF MOVE8

  GO 00000001
  WAIT(MOV=bXXXXXXX0)
  T.1
  VARI38 = 8PC ;
  VARI48 = 8PE ;

END

; WIZID = 00000520
;-------------
;User Program
;Continuous move for axis 1
DEL CMOV1
DEF CMOV1

  GO 10000000

END

; WIZID = 00000520
;-------------
;User Program
;Continuous move for axis 2
DEL CMOV2
DEF CMOV2

  GO 01000000

END

; WIZID = 00000520
;-------------
;User Program
;Continuous move for axis 3
DEL CMOV3
DEF CMOV3

  GO 00100000

END

; WIZID = 00000520
;-------------
;User Program
;Continuous move for axis 4
DEL CMOV4
DEF CMOV4

  GO 00010000

END

; WIZID = 00000520
;-------------
;User Program
;Continuous move for axis 5
DEL CMOV5
DEF CMOV5

  GO 00001000

END

; WIZID = 00000520
;-------------
;User Program
;Continuous move for axis 6
DEL CMOV6
DEF CMOV6

  GO 00000100

END

; WIZID = 00000520
;-------------
;User Program
;Continuous move for axis 7
DEL CMOV7
DEF CMOV7

  GO 00000010

END

; WIZID = 00000520
;-------------
;User Program
;Continuous move for axis 8
DEL CMOV8
DEF CMOV8

  GO 00000001

END

