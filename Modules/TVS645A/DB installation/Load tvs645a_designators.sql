INSERT INTO data_acquisition.tvs645a_designators
  SET channel_designator = "@channel_designator",
      unit_number        = @unit_number,
      channel_number     = @channel_number;
