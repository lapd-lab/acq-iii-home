INSERT INTO configuration.devices
  (
  device_name,
  device_type_id,
  device_description,
  device_bridge_vi_path,
  device_module_ip_address,
  device_module_vi_path,
  created_datetime
  )
SELECT "TVS645A",
       a.device_type_id,
       "Tektronix TVS645A fast waveform analyzers.",
       "@ACQ_II_home\\Modules\\TVS645A\\TVS645A bridge.vi",
       "@IP_address",
       "@ACQ_II_home\\Modules\\TVS645A\\TVS645A.vi",
       3168025745
FROM configuration.device_types a
WHERE a.device_type_name = "Data acquisition"

