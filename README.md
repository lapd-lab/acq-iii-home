# ACQ-II-home
Contains the LabVIEW code (and other code) supporting the ACQ II data acquisition system

_June, 2022_
- Most recent code base added

_July, 2022_
- Generic MSI handling added (beta version)
- ACQ II - v2.2
- HDF5 - v1.3


## Description
This is the data acquisition system for the Large Plasma Device (LAPD) at the UCLA Basic Plasma Science Facility.  It is custom software specific to this device.  It is written primarily in LabVIEW with certain
speed-critical components written in C++.  There is an initiative beginning to integrate some Python code into the ACQ II system also.  This may lead to a full migration into Python.

There are 2 applications within the system:
- ACQ II: the actual data acquisition system itself
- HDF5: a companion application which translates data run information into HDF5 files in real-time

More detailed description can be found in documents within this project.

## Deployment
Full system deployment from scratch is described in https://gitlab.com/lapd-lab/acq-ii-home/-/blob/development/Documentation/ACQ%20II%20installation%20guide.doc.  In the past, before the code was
stored in this repository, deployments were done incrementally by hand.  A zip file was typically sent with instructions as to which files to update, etc.  Starting with the change to generic Machine State Information (MSI)
handling in July, 2022, the deployment procedure will change as proposed below.  As we work with this, we may need to adjust the details.  In general, now each change will be versioned.  This might affect the ACQ II application,
the HDF5 application, or both.  Changed files will be put under the `ACQ II deployments` and `HDF5 deployments` folders.  This will always include the new top-level file for each application.  This way,
the changes will be easy to identify.  Note that the normal way of versioning under a source control system, such as GitLab, is not available with LabVIEW because all the files are binary.

Here is a step-by-step description of the process for deploying the latest version of the system:

### Download the code from GitLab
- Navigate to https://gitlab.com/lapd-lab/acq-ii-home/-/tree/main and confirm that you are viewing the "main" branch
- Click on the Download button and choose "zip" or whatever compression format that you prefer

### Install the system
- Make sure the previous version of this code is backed up or at least won't get overwritten when you uncompress this download
- Uncompress the download in the location of your preference, which we refer to as `<acq-ii>`

### Sanity test the system
- Select the latest version of the ACQ II application and run it, call this latest version `<acq-ii-version>`, i.e. `v2.2`
  - the latest version will be indicated in the notes at the top of this README, it will also be the latest version name under `ACQ II deployments`
  - navigate to `<acq-ii>/ACQ II deployments/ACQ II - <acq-ii-version>/`
  - run `DAQ controller - <acq-ii-version>.vi`
- Select the latest version of the HDF5 application and run it, call this latest version `<hdf5-version>`, i.e. `v1.3`
  - the latest version will be indicated in the notes at the top of this README, it will also be the latest version name under `HDF5 deployments`
  - navigate to `<acq-ii>/HDF5 deployments/HDF5 - <hdf5-version>/`
  - run `HDF5 real-time translator - <hdf5-version>.vi`
- Make sure that the expected new functionality is there and working correctly
- Make sure that the remaining functionality is unchanged

### Cut over and operate with the latest version
- Note that older versions of the top-level VIs have been renamed to include the suffix `- deprecated`
  - this will cause any links to these VIs to break (which is what we want to avoid confusion over versions)
- It is strongly recommended that you run the top-level VIs via links so that the versioning and deployment process is not visible the user
- Update the links for ACQ II and HDF5 to point to the latest versions

## Roadmap
- Upgrade MSI handling to be generic:
  - analysis phase: June, 2022  - Done
  - beta version: July, 2022  - Done
  - on-site integration and testing  - In Progress
  - complete documentation   - In Progress

- Analysis of migration to Python:
  - high-level analysis of order of migration  - In Progress

## Authors and acknowledgment
James Bamber, Steve Vincena
- Please feel free to add the names of others who have contributed to this project.

## Project status
This project has been stable since 2004.  We have the opportunity to improve it in the summer of 2022.
