//========================================================================================
// H5D Insert devices.cpp : Defines the entry point for the console application.
//
// Usage:
//   H5D_Insert_devices.exe <HDF5 file> <shot number> <experiment set> <experiment> <data run> <device count>
//     {<device name> list}
//
// <HDF5 file>: full path of the HDF5 file in which to insert a single shot of device data
// <shot number>: <index> = <shot number> - 1
// <experiment set> <experiment> <data run>: 3 strings which locate the shot files
// <device count>: the number of devices in the data run
// {<device name> list}: the device names surrounded by double quotes, i.e. "SIS 3301" "6K Compumotor"
//
// No output file.
//
//
// In this file:
//
//	herr_t find_dataset(
//		hid_t file_id,
//		char *group_name,
//		char *dataset_name,
//		hid_t *group_id,
//		bool *found)
//
//	herr_t extend_create_2D_dataset(
//		char* dataset,
//		hid_t type_id,
//		bool found,
//		hid_t group_id,
//		int index,
//		int data_length,
//		hid_t* dataset_id,
//		hid_t* file_space_id,
//		hid_t* mem_space_id,
//		hid_t* mem_type_id )
//
//	herr_t extend_create_header_dataset(
//		char* dataset,
//		char* header_type,
//		bool found,
//		hid_t group_id,
//		int index,
//		hid_t* dataset_id,
//		hid_t* file_space_id,
//		hid_t* mem_space_id,
//		hid_t* mem_type_id )
//	
//   int _tmain(int argc, _TCHAR* argv[])


#include "stdafx.h"
#include "hdf5.h"


// This array is based on the LabVIEW type codes.  See:
// LabVIEW Help >> Fundamentals >> How LabVIEW Stores Data in Memory >> Concepts >> Type Descriptors
//
// Note that strings (code '\30') are all set to be 120 characters long
//
int buffer_bytes[49] = 
{
	 0,  1,  2,  4,  8,  1,  2,  4,  // 00 - 07
	 8,  4,  8, 16,  8, 16, 32,  0,  // 08 - 0F
	 0,  0,  0,  0,  0,  1,  2,  4,  // 10 - 17
	 0,  4,  8, 16,  8, 16, 32,  0,  // 18 - 1F
	 0,  1,  0,  0,  0,  0,  0,  0,  // 20 - 27
	 0,  0,  0,  0,  0,  0,  0,  0,  // 28 - 2F
	 120                             // 30
};


//----------------------------------------------------------------------------------------
// Find dataset
//
herr_t find_dataset(
	hid_t file_id,
	char *group_name,
	char *dataset_name,
	hid_t *group_id,
	bool *found)
{
	*found = false;
	fprintf( stdout, "Looking for group: %s...\n", group_name );
	*group_id = H5Gopen( file_id, group_name );
	if ( group_id < 0 ) { H5Eprint( stderr ); return -1; }

	hsize_t num_obj;
	herr_t err = H5Gget_num_objs( *group_id, &num_obj );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }


	// Loop through objects, looking for a match with the dataset name
	//
	fprintf( stdout, "Looking for dataset: %s...\n", dataset_name );
	char obj_name[1024];
	for ( hsize_t idx=0; idx<num_obj; idx++ )
	{
		ssize_t size = H5Gget_objname_by_idx( *group_id, idx, obj_name, 1024 );
		if ( size < 0 ) { H5Eprint( stderr ); return -1; }
		if ( strcmp(obj_name, dataset_name) == 0 ) { *found = true; break; }
	}
	if ( *found ) fprintf( stdout, "%s found\n", dataset_name );
	else fprintf( stdout, "%s not found\n", dataset_name );

	return 0;
}

	
//----------------------------------------------------------------------------------------
// Extend/Create 2D dataset
//
herr_t extend_create_2D_dataset(
	char* dataset,
	hid_t type_id,
	bool found,
	hid_t group_id,
	int index,
	int data_length,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id )
{
	if ( found ) fprintf( stdout, "\nExtending dataset: " );
	else fprintf( stdout, "\nCreating dataset: " );
	fprintf( stdout, "%s...\n", dataset );
	fflush( stdout );
	herr_t err;


	// Open the dataset (if it was found)
	//
	if ( found )
	{
		*dataset_id = H5Dopen( group_id, dataset );
		if ( *dataset_id < 0 ) { H5Eprint( stderr ); return -1; }
	}


	// File dataspace
	//
	int file_rank = 2;
	hsize_t file_dims[2] = { index+1, data_length };
	hsize_t file_maxdims[2] = { 4294967295, data_length };

	if ( found )
	{
		*file_space_id = H5Dget_space( *dataset_id );
		if ( *file_space_id < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Sset_extent_simple( *file_space_id, file_rank, file_dims, file_maxdims );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}
	else
	{
		*file_space_id = H5Screate_simple( file_rank, file_dims, file_maxdims );
		if ( *file_space_id < 0 ) { H5Eprint( stderr ); return -1; }
	}

	hsize_t start[2] = { index, 0 };
	hsize_t stride[2] = { 1, 1 };
	hsize_t count[2] = { 1, 1 };
	hsize_t block[2] = { 1, data_length };
	err = H5Sselect_hyperslab( *file_space_id, H5S_SELECT_SET, start, stride, count, block );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }


	// Memory dataspace
	//
	int mem_rank = 1;
	hsize_t mem_dims[1] = { data_length };
	hsize_t mem_maxdims[1] = { data_length };
	*mem_space_id = H5Screate_simple( mem_rank, mem_dims, mem_maxdims );
	if ( *mem_space_id < 0 ) { H5Eprint( stderr ); return -1; }


	// Memory datatype
	// This is the hard part because there are a number of different datatypes, should have a
	// function call for it.
	//
	*mem_type_id = H5Tcopy( type_id );
	if ( *mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }


	if ( found )
	{
		// Extend the dataset
		//
		err = H5Dextend( *dataset_id, file_dims );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}
	else
	{
		// Dataset creation property list
		//
		hid_t plist_id = H5Pcreate( H5P_DATASET_CREATE );
		if ( plist_id < 0 ) { H5Eprint( stderr ); return -1; }

		hsize_t file_chunk[2] = { 1, data_length };
		int compression = 4;
		H5Pset_chunk( plist_id, file_rank, file_chunk );
		H5Pset_deflate( plist_id, compression );


		// Everything is set up so now create the 2D dataset
		//
		*dataset_id = H5Dcreate( group_id, dataset, *mem_type_id, *file_space_id, plist_id );
		if ( *dataset_id < 0 ) { H5Eprint( stderr ); return -1; }

		H5Pclose( plist_id );
	}

	return 0;
}


//----------------------------------------------------------------------------------------
// Extend/Create header dataset
//
herr_t extend_create_header_dataset(
	char* dataset,
	char* header_type,
	bool found,
	hid_t group_id,
	int index,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id )
{
	if ( found ) fprintf( stdout, "\nExtending dataset: " );
	else fprintf( stdout, "\nCreating dataset: " );
	fprintf( stdout, "%s...\n", dataset );
	fflush( stdout );
	herr_t err;


	// Open the dataset (if it was found)
	//
	if ( found )
	{
		*dataset_id = H5Dopen( group_id, dataset );
		if ( *dataset_id < 0 ) { H5Eprint( stderr ); return -1; }
	}


	// File dataspace
	//
	int file_rank = 1;
	hsize_t file_dims[1] = { index+1 };
	hsize_t file_maxdims[1] = { 4294967295 };

	if ( found )
	{
		*file_space_id = H5Dget_space( *dataset_id );
		if ( *file_space_id < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Sset_extent_simple( *file_space_id, file_rank, file_dims, file_maxdims );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}
	else
	{
		*file_space_id = H5Screate_simple( file_rank, file_dims, file_maxdims );
		if ( *file_space_id < 0 ) { H5Eprint( stderr ); return -1; }
	}

	hsize_t start[1] = { index };
	hsize_t stride[1] = { 1 };
	hsize_t count[1] = { 1 };
	hsize_t block[1] = { 1 };
	err = H5Sselect_hyperslab( *file_space_id, H5S_SELECT_SET, start, stride, count, block );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }


	// Memory dataspace
	//
	int mem_rank = 1;
	hsize_t mem_dims[1] = { 1 };
	hsize_t mem_maxdims[1] = { 1 };
	*mem_space_id = H5Screate_simple( mem_rank, mem_dims, mem_maxdims );
	if ( *mem_space_id < 0 ) { H5Eprint( stderr ); return -1; }


	// Memory datatype
	// This is the hard part because there are a number of different datatypes, should have a
	// function call for it.
	//
	hid_t char_field_id = H5Tcopy( H5T_NATIVE_CHAR );
	hid_t short_field_id = H5Tcopy( H5T_NATIVE_SHORT );
	hid_t int_field_id = H5Tcopy( H5T_NATIVE_INT );
	hid_t float_field_id = H5Tcopy( H5T_NATIVE_FLOAT );
	hid_t double_field_id = H5Tcopy( H5T_NATIVE_DOUBLE );
	hid_t string120_field_id = H5Tcopy( H5T_C_S1 );  H5Tset_size( string120_field_id, 120 );

	if ( strcmp(header_type, "SIS 3301") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 18 );
		if ( *mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Scale", 4, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Offset", 8, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Min", 12, short_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Max", 14, short_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Clipped", 16, short_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}

	if ( strcmp(header_type, "TVS645A") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 18 );
		if ( *mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Scale", 4, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Offset", 8, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Min", 12, short_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Max", 14, short_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Clipped", 16, short_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}

	if ( strcmp(header_type, "6K Compumotor") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 284 );
		if ( *mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "x", 4, double_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "y", 12, double_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "z", 20, double_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "theta", 28, double_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "phi", 36, double_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Motion list", 44, string120_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Probe name", 164, string120_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}

	if ( strcmp(header_type, "GPIB interface") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 246 );
		if ( *mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "GPIB channel", 4, short_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Configuration name", 6, string120_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "GPIB string", 126, string120_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}

	H5Tclose( char_field_id );  H5Tclose( short_field_id );  H5Tclose( int_field_id );  H5Tclose( float_field_id );
	H5Tclose( double_field_id );  H5Tclose( string120_field_id );


	if ( found )
	{
		// Extend the dataset
		//
		err = H5Dextend( *dataset_id, file_dims );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}
	else
	{
		// Dataset creation property list
		//
		hid_t plist_id = H5Pcreate( H5P_DATASET_CREATE );
		if ( plist_id < 0 ) { H5Eprint( stderr ); return -1; }

		hsize_t file_chunk[1] = { 1 };
		int compression = 4;
		H5Pset_chunk( plist_id, file_rank, file_chunk );
		H5Pset_deflate( plist_id, compression );


		// Everything is set up so now create the 2D dataset
		//
		*dataset_id = H5Dcreate( group_id, dataset, *mem_type_id, *file_space_id, plist_id );
		if ( *dataset_id < 0 ) { H5Eprint( stderr ); return -1; }

		H5Pclose( plist_id );
	}

	return 0;
}


//----------------------------------------------------------------------------------------
// Extend/Create remote header
//
herr_t extend_create_remote_header(
	char* dataset,
	char* device_type,
	int data_type,
	char* descriptor,
	int header_bytes,
	bool found,
	hid_t group_id,
	int index,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id )
{
	if ( found ) fprintf( stdout, "\nExtending dataset: " );
	else fprintf( stdout, "\nCreating dataset: " );
	fprintf( stdout, "%s...\n", dataset );
	fflush( stdout );
	herr_t err;


	// Open the dataset (if it was found)
	//
	if ( found )
	{
		*dataset_id = H5Dopen( group_id, dataset );
		if ( *dataset_id < 0 ) { H5Eprint( stderr ); return -1; }
	}


	// File dataspace
	//
	int file_rank = 1;
	hsize_t file_dims[1] = { index+1 };
	hsize_t file_maxdims[1] = { 4294967295 };

	if ( found )
	{
		*file_space_id = H5Dget_space( *dataset_id );
		if ( *file_space_id < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Sset_extent_simple( *file_space_id, file_rank, file_dims, file_maxdims );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}
	else
	{
		*file_space_id = H5Screate_simple( file_rank, file_dims, file_maxdims );
		if ( *file_space_id < 0 ) { H5Eprint( stderr ); return -1; }
	}

	hsize_t start[1] = { index };
	hsize_t stride[1] = { 1 };
	hsize_t count[1] = { 1 };
	hsize_t block[1] = { 1 };
	err = H5Sselect_hyperslab( *file_space_id, H5S_SELECT_SET, start, stride, count, block );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }


	// Memory dataspace
	//
	int mem_rank = 1;
	hsize_t mem_dims[1] = { 1 };
	hsize_t mem_maxdims[1] = { 1 };
	*mem_space_id = H5Screate_simple( mem_rank, mem_dims, mem_maxdims );
	if ( *mem_space_id < 0 ) { H5Eprint( stderr ); return -1; }


	// Memory datatype
	// This is the hard part because there are a number of different datatypes, should have a
	// function call for it.
	//
	hid_t char_field_id = H5Tcopy( H5T_NATIVE_CHAR );
	hid_t short_field_id = H5Tcopy( H5T_NATIVE_SHORT );
	hid_t int_field_id = H5Tcopy( H5T_NATIVE_INT );
	hid_t float_field_id = H5Tcopy( H5T_NATIVE_FLOAT );
	hid_t double_field_id = H5Tcopy( H5T_NATIVE_DOUBLE );
	hid_t string120_field_id = H5Tcopy( H5T_C_S1 );  H5Tset_size( string120_field_id, 120 );

	// Remote digitizers
	//
	if ( strcmp(device_type, "Data acquisition") == 0 )
	{
		if ( data_type == 2 )
		{
			*mem_type_id = H5Tcreate( H5T_COMPOUND, header_bytes );
			if ( *mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Scale", 4, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Offset", 8, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Min", 12, short_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Max", 14, short_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Clipped", 16, char_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		}
		if ( data_type == 3 )
		{
			*mem_type_id = H5Tcreate( H5T_COMPOUND, header_bytes );
			if ( *mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Scale", 4, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Offset", 8, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Min", 12, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Max", 16, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Clipped", 20, char_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		}
		if ( data_type == 9 )
		{
			*mem_type_id = H5Tcreate( H5T_COMPOUND, header_bytes );
			if ( *mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Scale", 4, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Offset", 8, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Min", 12, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Max", 16, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
			err = H5Tinsert( *mem_type_id, "Clipped", 20, char_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		}
	} // end remote digitizer case


	// Other remote devices (i.e. non-digitizers)
	//
	else
	{
		// Have to read through the descriptor string
		//
		fprintf( stdout, "Remote non-digitizer\n" );
		fflush( stdout );
		*mem_type_id = H5Tcreate( H5T_COMPOUND, header_bytes );
		if ( *mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }
		int descriptor_index = 4;
		char string_length;
		char element_name[1024];
		int nelements = sread_short( descriptor, &descriptor_index );
		fprintf( stdout, "nelements = %d\n", nelements );
		fflush( stdout );
		char LV_type[2];
		int buffer_index = 0;
		for ( int i=0; i<nelements; i++ )
		{
			fprintf( stdout, "element %d: ", i );
			fflush( stdout );
			short nbytes = sread_short( descriptor, &descriptor_index );
			sread_short_bytes( descriptor, LV_type, &descriptor_index );
			int next_element_index = descriptor_index + nbytes - 4;

			// 8-bit integer types + boolean
			if ( (LV_type[0] == 0x01) || (LV_type[0] == 0x05) || (LV_type[0] == 0x15) || (LV_type[0] == 0x21) )
			{
				sread_bool_byte( descriptor, &string_length, &descriptor_index );
				for ( int j=0; j<(int)string_length; j++ ) element_name[j] = descriptor[descriptor_index+j];
				element_name[string_length] = '\0';
				descriptor_index = descriptor_index + string_length;
				fprintf( stdout, "%s\n", element_name );
				fflush( stdout );
				err = H5Tinsert( *mem_type_id, element_name, buffer_index, char_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
				buffer_index = buffer_index + 1;
			}
			
			// 16-bit integer types
			else if ( (LV_type[0] == 0x02) || (LV_type[0] == 0x06) || (LV_type[0] ==  0x16) )
			{
				sread_bool_byte( descriptor, &string_length, &descriptor_index );
				for ( int j=0; j<(int)string_length; j++ ) element_name[j] = descriptor[descriptor_index+j];
				element_name[string_length] = '\0';
				descriptor_index = descriptor_index + string_length;
				fprintf( stdout, "%s\n", element_name );
				fflush( stdout );
				err = H5Tinsert( *mem_type_id, element_name, buffer_index, short_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
				buffer_index = buffer_index + 2;
			}
			
			// 32-bit integer types
			else if ( (LV_type[0] == 0x03) || (LV_type[0] == 0x07) || (LV_type[0] == 0x17) )
			{
				sread_bool_byte( descriptor, &string_length, &descriptor_index );
				for ( int j=0; j<(int)string_length; j++ ) element_name[j] = descriptor[descriptor_index+j];
				element_name[string_length] = '\0';
				descriptor_index = descriptor_index + string_length;
				fprintf( stdout, "%s\n", element_name );
				fflush( stdout );
				err = H5Tinsert( *mem_type_id, element_name, buffer_index, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
				buffer_index = buffer_index + 4;
			}
			
			// 32-bit float types
			else if ( (LV_type[0] == 0x09) || (LV_type[0] == 0x19) )
			{
				sread_bool_byte( descriptor, &string_length, &descriptor_index );
				for ( int j=0; j<(int)string_length; j++ ) element_name[j] = descriptor[descriptor_index+j];
				element_name[string_length] = '\0';
				descriptor_index = descriptor_index + string_length;
				fprintf( stdout, "%s\n", element_name );
				fflush( stdout );
				err = H5Tinsert( *mem_type_id, element_name, buffer_index, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
				buffer_index = buffer_index + 4;
			}
			
			// 64-bit double types
			else if ( (LV_type[0] == 0x0A) || (LV_type[0] == 0x1A) )
			{
				sread_bool_byte( descriptor, &string_length, &descriptor_index );
				for ( int j=0; j<(int)string_length; j++ ) element_name[j] = descriptor[descriptor_index+j];
				element_name[string_length] = '\0';
				descriptor_index = descriptor_index + string_length;
				fprintf( stdout, "%s\n", element_name );
				fflush( stdout );
				err = H5Tinsert( *mem_type_id, element_name, buffer_index, double_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
				buffer_index = buffer_index + 8;
			}
			
			// string
			else if ( (LV_type[0] == 0x30) )
			{
				int unused_string_length = sread_int( descriptor, &descriptor_index );
				sread_bool_byte( descriptor, &string_length, &descriptor_index );
				for ( int j=0; j<(int)string_length; j++ ) element_name[j] = descriptor[descriptor_index+j];
				element_name[string_length] = '\0';
				descriptor_index = descriptor_index + string_length;
				fprintf( stdout, "%s\n", element_name );
				fflush( stdout );
				err = H5Tinsert( *mem_type_id, element_name, buffer_index, string120_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
				buffer_index = buffer_index + 120;
			}

			descriptor_index = next_element_index;
		} // end looping through cluster elements
	} // end remote non-digitizer case

	H5Tclose( char_field_id );  H5Tclose( short_field_id );  H5Tclose( int_field_id );  H5Tclose( float_field_id );
	H5Tclose( double_field_id );  H5Tclose( string120_field_id );


	if ( found )
	{
		// Extend the dataset
		//
		err = H5Dextend( *dataset_id, file_dims );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}
	else
	{
		// Dataset creation property list
		//
		hid_t plist_id = H5Pcreate( H5P_DATASET_CREATE );
		if ( plist_id < 0 ) { H5Eprint( stderr ); return -1; }

		hsize_t file_chunk[1] = { 1 };
		int compression = 4;
		H5Pset_chunk( plist_id, file_rank, file_chunk );
		H5Pset_deflate( plist_id, compression );


		// Everything is set up so now create the 2D dataset
		//
		*dataset_id = H5Dcreate( group_id, dataset, *mem_type_id, *file_space_id, plist_id );
		if ( *dataset_id < 0 ) { H5Eprint( stderr ); return -1; }

		H5Pclose( plist_id );
	}

	fprintf( stdout, "Finished extend_create_remote_header()\n" );
	fflush( stdout );

	return 0;
}


//========================================================================================
//----------------------------------------------------------------------------------------
// Main
//
int _tmain(int argc, _TCHAR* argv[])
{
	// Expecting "H5D_Insert_devices.exe" plus 1 argument
	// ??? No. call is almost the same, just change shot_number to
	// shot_count
	// ??? No again. Don't know shot_count at the beginning, just remove that argument.
	//
	if ( argc != 2 )
	{
		fprintf( stderr, "argc = %d\n", argc );
		return -1;
	}
	char start_time_str[1024];
	strcpy( start_time_str, argv[1] );


	// Loop indefinitely, processing "shot number" files.  Keep track of time
	// since the last file appeared and stop when the time is too long, say
	// 100,000 seconds, i.e. a little more than 1 day.  Also, look within each
	// file for the "translation complete" flag to be true
	//
	int file_index = 0;
	char system_exec_filename[1024];
	char root_folder[1024] = "C:/Shadow data";
	int delay_count = 0;
	int max_delay = ???
	while ( delay_count < 100000 )
	{
		sprintf( experiment_folder, "%s/%s/%s", root_folder, experiment_set, experiment );
		Sleep( 20000 );


		/*
		// Expecting "H5D_Insert_devices.exe" plus 6+ndevices arguments
		//
		// Read in the first 6 arguments
		//
		char HDF5_filename[1024];
		int *shot_number = (int *)malloc( sizeof(int) );
		char *shot_number_bytes;
		int device_count;
		char experiment_set[1024];
		char experiment[1024];
		char data_run[1024];

		strcpy( HDF5_filename, argv[1] );
		*shot_number = atoi(argv[2]);
		shot_number_bytes = (char *) shot_number;
		int index = *shot_number - 1;
		strcpy( experiment_set, argv[3] );
		strcpy( experiment, argv[4] );
		strcpy( data_run, argv[5] );
		char experiment_folder[1024];
		sprintf( experiment_folder, "%s/%s/%s", root_folder, experiment_set, experiment );

		device_count = atoi(argv[6]);
		if ( argc != 7+device_count )
		{
			fprintf( stderr, "argc = %d\n", argc );
			return -1;
		}


		// Open the HDF5 file
		//
		hid_t file_id = H5Fopen( HDF5_filename, H5F_ACC_RDWR, H5P_DEFAULT );  // hid_t H5Fopen(const char *name, unsigned flags, hid_t access_id )
		if ( file_id < 0 ) { H5Eprint( stderr ); return -1; }


		// Declare the id's that we'll need
		//
		hid_t group_id;
		hid_t dataset_id;
		hid_t file_space_id;
		hid_t mem_space_id;
		hid_t mem_type_id;

		herr_t err;
		bool found;


		// Loop through devices
		//
		for ( int idevice = 0; idevice<device_count; idevice++ )
		{
			char device_name[1024];
			strcpy( device_name, argv[7+idevice] );
			fprintf( stdout, "device = %s\n", device_name );


			// *****************************
			// *** Switch on device name ***
			//
			// SIS 3301
			//
			if ( strcmp(device_name, "SIS 3301") == 0 )
			{
				// Figure out the config names
				//
				fprintf( stdout, "Processing SIS 3301...\n" );
				fflush( stdout );
				char configfile_name[2048];
				sprintf( configfile_name, "%s/%s.%s/SIS 3301 configs.dat",
					experiment_folder, data_run, device_name );

				FILE* configfile = fopen( configfile_name, "rb" );
				int nids = read_int( configfile );
				short* ids = (short *)malloc( nids*sizeof(short) );
				read_short_array( configfile, ids, nids );
				int nconfigs = read_int( configfile );
				char* config_names = (char *)malloc( nconfigs*1024 );
				for ( int i=0; i<nconfigs; i++ )
					read_string( configfile, &(config_names[i*1024]) );

				fclose( configfile );


				// Open the shotfile
				//
				char shotfile_name[2048];
				sprintf( shotfile_name, "%s/%s.%s/%s.%s.shot%06d.dat",
					experiment_folder, data_run, device_name, data_run, device_name, *shot_number );
				fprintf( stdout, "shotfile_name = %s\n", shotfile_name );
				fflush( stdout );

				FILE* shotfile = fopen( shotfile_name, "rb" );

				// SIS 3301 header looks like this:
				//
				// Shot number |	int (4) |	Cross reference to all data run information
				// Data packet length |	int (4) |	Number of bytes of data (= 2 * number of samples)
				// Clipped? |	int (2) |	0: FALSE, 1: TRUE
				// Channel number |	int (2) |	0-indexed
				// Minimum value |	int (2) |	Minimum value (unscaled) in data record
				// Maximum value |	int (2) |	Maximum value (unscaled) in data record
				// Configuration id |	int (4) |	Id into sis_3301 table
				// Board number |	int (4) |	0-indexed
				// unused |	flt (4) |	0.00
				// unused |	flt (4) |	0.00
				// unused |	flt (4) |	0.00
				// unused |	flt (4) |	0.00
				// unused |	flt (4) |	0.00
				// Packet multiplier |	flt (4) |	Usage: Scaled data = saved data * packet multiplier + packet offet
				// Packet offset |	flt (4) |	See "Packet multiplier"
				//
				char header[18];
				// Note: size_t fread( void *buffer, size_t size, size_t count, FILE *stream );
				fread( &(header[0]), 4, 1, shotfile );  // Shot number (start read here to facilitate eof check)
				while ( feof(shotfile) == 0 )
				{
					// Finish reading header and data for this channel
					//
					int data_bytes; fread( &data_bytes, 4, 1, shotfile );  // Data packet length (in bytes)
					fread( &(header[16]), 2, 1, shotfile );  // Clipped
					short channel_number; fread( &channel_number, 2, 1, shotfile );  // Channel number
					fread( &(header[12]), 2, 1, shotfile );  // Minimum value
					fread( &(header[14]), 2, 1, shotfile );  // Maximum value
					int configuration_id; fread( &configuration_id, 4, 1, shotfile );  // Configuration id
					int board_number; fread( &board_number, 4, 1, shotfile );  // Board number
					float unused[5];
					fread( unused, 4, 5, shotfile );  // Unused[5]
					fread( &(header[4]), 4, 1, shotfile );  // Packet multiplier (i.e. Scale)
					fread( &(header[8]), 4, 1, shotfile );  // Packet offset

					short *data_buffer = (short *)malloc( data_bytes );
					int data_points = data_bytes / 2;
					fread( data_buffer, 2, data_points, shotfile );


					// Create the data and header dataset names
					//
					int array_index = -1;
					for ( int i=0; i<nids; i++ )
						if ( ids[i] == configuration_id ) array_index = i;

					if ( array_index == -1 )
					{
						fprintf( stderr, "Could not find configuration name, id = %d", configuration_id );
						return -1;
					}

					char dataset_name[2048];
					char header_name[2048];
					sprintf( dataset_name, "%s [%d:%d]", &(config_names[array_index*1024]), board_number, channel_number );
					sprintf( header_name, "%s headers", dataset_name );


					// ********************************************
					// *** Process the data and header datasets ***
					//
					// You have to:
					//   1) look for the dataset
					//   2) if found, extend it
					//   3) if not found, create it
					//   4) write the shot into it
					//   5) close the id's that were opened along the way
					//
					err = find_dataset( file_id, "/Raw data + config/SIS 3301", dataset_name, &group_id, &found );  if ( err < 0 ) return -1;
					err = extend_create_2D_dataset( dataset_name, H5T_NATIVE_SHORT,
						found, group_id, index, data_points, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
					if ( err < 0 ) return -1;
					err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, data_buffer );
					if ( err < 0 ) { H5Eprint( stderr ); return -1; }
					H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );

					err = find_dataset( file_id, "/Raw data + config/SIS 3301", header_name, &group_id, &found );  if ( err < 0 ) return -1;
					err = extend_create_header_dataset( header_name, "SIS 3301",
						found, group_id, index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
					if ( err < 0 ) return -1;
					err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, header );
					if ( err < 0 ) { H5Eprint( stderr ); return -1; }
					H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );

					free( data_buffer );


					// Start reading header/data for next channel
					//
					fread( &(header[0]), 4, 1, shotfile );  // Shot number (read this down here to test for end of file at top)
				} // End looping through data channels
				free( ids );
				free( config_names );
				fclose( shotfile );
				fprintf( stdout, "Finished SIS 3301...\n" );
				fflush( stdout );
			} // End SIS 3301 case


			// TVS645A
			//
			else if ( strcmp(device_name, "TVS645A") == 0 )
			{
				fprintf( stdout, "Processing TVS645A...\n" );
				fflush( stdout );
				char shotfile_name[2048];
				sprintf( shotfile_name, "%s/%s.%s/%s.%s.shot%06d.dat",
					experiment_folder, data_run, device_name, data_run, device_name, *shot_number );
				fprintf( stdout, "shotfile_name = %s\n", shotfile_name );

				FILE* shotfile = fopen( shotfile_name, "rb" );

				// TVS645A header looks like this:
				//
				// Shot number |	int (4) |	Cross reference to all data run information
				// Data packet length |	int (4) |	Number of bytes of data (= 2 * number of samples)
				// Clipped? |	int (2) |	0: FALSE, 1: TRUE
				// Channel number |	int (2) |	0-indexed
				// Minimum value |	int (2) |	Minimum value (unscaled) in data record
				// Maximum value |	int (2) |	Maximum value (unscaled) in data record
				// Configuration id |	int (4) |	Id into sis_3301 table
				// Board number |	int (4) |	0-indexed
				// unused |	flt (4) |	0.00
				// unused |	flt (4) |	0.00
				// unused |	flt (4) |	0.00
				// unused |	flt (4) |	0.00
				// unused |	flt (4) |	0.00
				// Packet multiplier |	flt (4) |	Usage: Scaled data = saved data * packet multiplier + packet offet
				// Packet offset |	flt (4) |	See "Packet multiplier"
				//
				char header[18];
				// Note: size_t fread( void *buffer, size_t size, size_t count, FILE *stream );
				fread( &(header[0]), 4, 1, shotfile );  // Shot number (start read here to facilitate eof check)
				while ( feof(shotfile) == 0 )
				{
					// Finish reading header and data for this channel
					//
					int data_bytes; fread( &data_bytes, 4, 1, shotfile );  // Data packet length (in bytes)
					fread( &(header[16]), 2, 1, shotfile );  // Clipped
					short channel_number; fread( &channel_number, 2, 1, shotfile );  // Channel number
					fread( &(header[12]), 2, 1, shotfile );  // Minimum value
					fread( &(header[14]), 2, 1, shotfile );  // Maximum value
					int configuration_id; fread( &configuration_id, 4, 1, shotfile );  // Configuration id
					int board_number; fread( &board_number, 4, 1, shotfile );  // Board number
					float unused[5];
					fread( unused, 4, 5, shotfile );  // Unused[5]
					fread( &(header[4]), 4, 1, shotfile );  // Packet multiplier (i.e. Scale)
					fread( &(header[8]), 4, 1, shotfile );  // Packet offset

					short *data_buffer = (short *)malloc( data_bytes );
					int data_points = data_bytes / 2;
					fread( data_buffer, 2, data_points, shotfile );


					// Figure out the dataset names
					//
					char configfile_name[2048];
					sprintf( configfile_name, "%s/%s.%s/TVS645A configs.dat",
						experiment_folder, data_run, device_name );

					FILE* configfile = fopen( configfile_name, "rb" );
					int nids = read_int( configfile );
					short *ids = (short *)malloc( nids * sizeof(short) );
					read_short_array( configfile, ids, nids );
					int nconfigs = read_int( configfile );
					char config_name[1024];
					int array_index = -1;
					for ( int i=0; i<nconfigs; i++ )
					{
						read_string( configfile, config_name );
						if ( ids[i] == configuration_id ) { array_index = i; break; }
					}
					fclose( configfile );

					if ( array_index == -1 )
					{
						fprintf( stderr, "Could not find configuration name, id = %d", configuration_id );
						return -1;
					}

					char dataset_name[2048];
					char header_name[2048];
					sprintf( dataset_name, "%s [%d:%d]", config_name, board_number, channel_number );
					sprintf( header_name, "%s headers", dataset_name );
					fprintf( stdout, "dataset_name = %s\n", dataset_name );


					// ********************************************
					// *** Process the data and header datasets ***
					//
					// You have to:
					//   1) look for the dataset
					//   2) if found, extend it
					//   3) if not found, create it
					//   4) write the shot into it
					//   5) close the id's that were opened along the way
					//
					err = find_dataset( file_id, "/Raw data + config/TVS645A", dataset_name, &group_id, &found );  if ( err < 0 ) return -1;
					err = extend_create_2D_dataset( dataset_name, H5T_NATIVE_SHORT,
						found, group_id, index, data_points, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
					if ( err < 0 ) return -1;
					err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, data_buffer );
					if ( err < 0 ) { H5Eprint( stderr ); return -1; }
					H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );

					err = find_dataset( file_id, "/Raw data + config/TVS645A", header_name, &group_id, &found );  if ( err < 0 ) return -1;
					err = extend_create_header_dataset( header_name, "TVS645A",
						found, group_id, index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
					if ( err < 0 ) return -1;
					err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, header );
					if ( err < 0 ) { H5Eprint( stderr ); return -1; }
					H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );

					free( ids );
					free( data_buffer );


					// Start reading header/data for next channel
					//
					fread( &(header[0]), 4, 1, shotfile );  // Shot number (read this down here to test for end of file at top)
				} // End looping through data channels
				fclose( shotfile );
				fprintf( stdout, "Finished TVS645A...\n" );
				fflush( stdout );
			} // End TVS645A case


			// 6K Compumotor
			//
			else if ( strcmp(device_name, "6K Compumotor") == 0 )
			{
				// Open the shotfile
				//
				fprintf( stdout, "Processing 6K Compumotor...\n" );
				fflush( stdout );
				char shotfile_name[2048];
				sprintf( shotfile_name, "%s/%s.%s/%s.%s.shot%06d.dat",
					experiment_folder, data_run, device_name, data_run, device_name, *shot_number );
				fprintf( stdout, "shotfile_name = %s\n", shotfile_name );

				FILE* shotfile = fopen( shotfile_name, "rb" );

				// 6K Compumotor run-time looks like this:
				//
				// Shot number |	int (4) |	Cross reference to all data run information
				// x |	double |	x-location of probe tip
				// y |	double |	y-location of probe tip
				// z |	double |	z-location of probe tip
				// theta |	double |	Angle of probe shaft in x-y plane
				// phi |	double |	Always 0.0
				// Motion list |	string |	Motion list name
				// Probe name |	string |	Probe name
				//
				char runtime[284];
				// Note: size_t fread( void *buffer, size_t size, size_t count, FILE *stream );
				int nprobes = read_int( shotfile );
				//fread( &nprobes, 4, 1, shotfile );
				fprintf( stdout, "nprobes = %d\n", nprobes );
				fflush( stdout );
				for ( int i=0; i<nprobes; i++ )
				{
					fprintf( stdout, "Probe number: %d\n", i );
					fflush( stdout );
					// Read run-time entry for this probe
					//
					read_int_bytes( shotfile, &(runtime[0]) );  // Shot number
					read_double_bytes( shotfile, &(runtime[4]) );  // x
					read_double_bytes( shotfile, &(runtime[12]) );  // y
					read_double_bytes( shotfile, &(runtime[20]) );  // z
					read_double_bytes( shotfile, &(runtime[28]) );  // theta
					read_double_bytes( shotfile, &(runtime[36]) );  // phi
					read_string( shotfile, &(runtime[44]) );  // Motion list
					read_string( shotfile, &(runtime[164]) );  // Probe name

					char runtime_name[2048];
					sprintf( runtime_name, "%s", &(runtime[164]) );
					fprintf( stdout, "runtime_name = %s\n", runtime_name );
					fflush( stdout );


					// ********************************************
					// *** Process the data and header datasets ***
					//
					// You have to:
					//   1) look for the dataset
					//   2) if found, extend it
					//   3) if not found, create it
					//   4) write the shot into it
					//   5) close the id's that were opened along the way
					//
					fprintf( stdout, "index = %d\n", index );
					fflush( stdout );
					err = find_dataset( file_id, "/Raw data + config/6K Compumotor", runtime_name, &group_id, &found );  if ( err < 0 ) return -1;
					err = extend_create_header_dataset( runtime_name, "6K Compumotor",
						found, group_id, index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
					if ( err < 0 ) return -1;
					err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, runtime );
					if ( err < 0 ) { H5Eprint( stderr ); return -1; }
					H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );
					fprintf( stdout, "Finished closing ids\n" );
					fflush( stdout );

				} // End looping through probes
				fclose( shotfile );
				fprintf( stdout, "Finished 6K Compumotor...\n" );
				fflush( stdout );
			} // End 6K Compumotor case


			// GPIB interface
			//
			else if ( strcmp(device_name, "GPIB interface") == 0 )
			{
				fprintf( stdout, "Processing GPIB interface...\n" );
				fflush( stdout );
				char shotfile_name[2048];
				sprintf( shotfile_name, "%s/%s.%s/%s.%s.shot%06d.dat",
					experiment_folder, data_run, device_name, data_run, device_name, *shot_number );
				fprintf( stdout, "shotfile_name = %s\n", shotfile_name );
				fflush( stdout );

				FILE* shotfile = fopen( shotfile_name, "rb" );


				// GPIB interface shot file looks like this:
				//
				// Reported length |	int (4) | Number of bytes following
				// Shot number |	int (4) |	Cross reference to all data run information
				// GPIB channel |	int (2) |	Identifies the GPIB device being controlled
				// Configuration name |	string |	Configuration of the GPIB interface module
				// GPIB string |	string |	String sent to the GPIB device
				//
				char runtime[246];
				read_int_bytes( shotfile, &(runtime[0]) );  // Shot number
				read_short_bytes( shotfile, &(runtime[4]) );  // GPIB channel
				read_string( shotfile, &(runtime[6]) );  // Configuration name
				read_string( shotfile, &(runtime[126]) );  // GPIB string
				fclose( shotfile );


				// *********************************
				// *** Process the run-time list ***
				//
				// You have to:
				//   1) look for the dataset
				//   2) if found, extend it
				//   3) if not found, create it
				//   4) write the shot into it
				//   5) close the id's that were opened along the way
				//
				err = find_dataset( file_id, "/Raw data + config/GPIB interface", "Run time list", &group_id, &found );  if ( err < 0 ) return -1;
				err = extend_create_header_dataset( "Run time list", "GPIB interface",
					found, group_id, index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
				if ( err < 0 ) return -1;
				err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, runtime );
				if ( err < 0 ) { H5Eprint( stderr ); return -1; }
				H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );

				fprintf( stdout, "Finished GPIB interface\n" );
				fflush( stdout );
			} // End GPIB interface case


			// Remote device
			//
			else
			{
				// Open the shot file.
				//
				fprintf( stdout, "Processing %s...\n", device_name );
				fflush( stdout );
				char shotfile_name[2048];
				sprintf( shotfile_name, "%s/%s.%s/%s.%s.shot%06d.dat",
					experiment_folder, data_run, device_name, data_run, device_name, *shot_number );
				fprintf( stdout, "shotfile_name = %s\n", shotfile_name );
				fflush( stdout );

				FILE* shotfile = fopen( shotfile_name, "rb" );


				// Now read the remote device type file.  Remote digitizers are handled differently 
				// than non-digitizers.
				//
				char typefile_name[2048];
				sprintf( typefile_name, "%s/%s.%s/%s.%s device type.dat",
					experiment_folder, data_run, device_name, data_run, device_name );

				FILE* typefile = fopen( typefile_name, "rb" );
				char device_type[1024];
				read_string( typefile, device_type );
				int data_type = read_int( typefile );
				fclose( typefile );
				fprintf( stdout, "device_type = %s\n", device_type );
				fprintf( stdout, "data_type = %d\n", data_type );
				fflush( stdout );


				// Remote digitizer
				//
				if ( strcmp( device_type, "Data acquisition" ) == 0 )
				{
					fprintf( stdout, "Remote digitizer\n" );
					fflush( stdout );
					char *descriptor = "NULL"; // NULL place holder

					int header_bytes;
					int datum_bytes;
					hid_t HDF5_data_type;
					if ( data_type == 2 )
					{
						header_bytes = 17;
						datum_bytes = 2;
						HDF5_data_type = H5T_NATIVE_SHORT;
					}
					if ( data_type == 3 )
					{
						header_bytes = 21;
						datum_bytes = 4;
						HDF5_data_type = H5T_NATIVE_INT;
					}
					if ( data_type == 9 )
					{
						header_bytes = 21;
						datum_bytes = 4;
						HDF5_data_type = H5T_NATIVE_FLOAT;
					}
					else
					{
						fprintf( stderr, "Unrecognized digitizer data type: %d.\n", data_type );
						return -1;
					}


					// Remote digitizer header looks like this:
					//
					// Channel name
					//		-character count	|	int (4)
					//		-character array	|	[ char ]
					// Point count		|	int (4)
					// Vertical scale	|	flt (4)
					// Vertical offset	|	flt (4)
					// Min		|	{ int (2), int (4), or flt (4) }
					// Max		|	{ int (2), int (4), or flt (4) }
					// Clipped		|	int (1)
					//
					char *header = (char *)malloc( header_bytes );
					for ( int i=0; i<4; i++ ) header[i] = shot_number_bytes[i];

					// Note: size_t fread( void *buffer, size_t size, size_t count, FILE *stream );
					int character_count;
					fread( &character_count, 4, 1, shotfile );
					char *configuration_name = (char *)malloc( character_count+1 );
					fread( configuration_name, 1, character_count, shotfile );
					configuration_name[character_count] = '\0';  // Configuration name
					fprintf( stdout, "configuration_name: %s\n", configuration_name );
					fflush( stdout );
					int channel_count;
					fread( &channel_count, 4, 1, shotfile );  // Channel count
					fprintf( stdout, "channel_count: %d\n", channel_count );
					fflush( stdout );
					for ( int i=0; i<channel_count; i++ )
					{
						// Read the header and data for this channel
						//
						fread( &character_count, 4, 1, shotfile );
						char *channel_name = (char *)malloc( character_count+1 );
						fread( channel_name, 1, character_count, shotfile );
						channel_name[character_count] = '\0';    // Channel name
						fprintf( stdout, "channel_name: %s\n", channel_name );
						fflush( stdout );
						int point_count;
						fread( &point_count, 4, 1, shotfile );  // Point count
						fread( &(header[4]), 1, header_bytes-4, shotfile );  // Scale, Offset, Min, Max, Clipped

						int data_bytes = point_count*datum_bytes;
						char *data_buffer = (char *)malloc( data_bytes );
						fprintf( stdout, "data_bytes: %d\n", data_bytes );
						fflush( stdout );
						fread( data_buffer, 1, data_bytes, shotfile );


						// Create the data and header dataset names
						//
						char dataset_name[2048];
						char header_name[2048];
						sprintf( dataset_name, "%s [%s]", configuration_name, channel_name );
						sprintf( header_name, "%s headers", dataset_name );
						fprintf( stdout, "dataset_name = %s\n", dataset_name );
						fflush( stdout );
						char group_name[1024];
						sprintf( group_name, "/Raw data + config/%s", device_name );


						// ********************************************
						// *** Process the data and header datasets ***
						//
						// You have to:
						//   1) look for the dataset
						//   2) if found, extend it
						//   3) if not found, create it
						//   4) write the shot into it
						//   5) close the id's that were opened along the way
						//
						err = find_dataset( file_id, group_name, dataset_name, &group_id, &found );  if ( err < 0 ) return -1;
						err = extend_create_2D_dataset( dataset_name, HDF5_data_type,
							found, group_id, index, point_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
						if ( err < 0 ) return -1;
						err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, data_buffer );
						if ( err < 0 ) { H5Eprint( stderr ); return -1; }
						H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );

						err = find_dataset( file_id, group_name, header_name, &group_id, &found );  if ( err < 0 ) return -1;
						err = extend_create_remote_header( header_name, device_type, data_type, descriptor, header_bytes,
							found, group_id, index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
						if ( err < 0 ) return -1;
						err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, header );
						if ( err < 0 ) { H5Eprint( stderr ); return -1; }
						H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );

						free( data_buffer );
						free( channel_name );
					} // End looping through data channels
					free( configuration_name );
					free( header );
				}


				// All other remote device types (i.e. non-digitizers)
				//
				else
				{
					// Remote device shot file structure, for non-digitizers, is specified by a descriptor.
					// Read descriptor into a string which will be gone through several times.
					//
					char descriptorfile_name[2048];
					sprintf( descriptorfile_name, "%s/%s.%s/%s.%s descriptor.dat",
						experiment_folder, data_run, device_name, data_run, device_name );

					FILE* descriptorfile = fopen( descriptorfile_name, "rb" );
					int nshorts_total = read_int( descriptorfile );
					int nbytes_total = nshorts_total * 2;
					char *descriptor = (char *)malloc( nbytes_total );
					fread( descriptor, 1, nbytes_total, descriptorfile );
					fclose( descriptorfile );


					// Read the main LabVIEW type.
					//
					int buffer_size = 0;
					int descriptor_index = 2;
					char LV_type[2];
					sread_short_bytes( descriptor, LV_type, &descriptor_index );


					// Make sure descriptor file contains a cluster
					//
					if ( LV_type[0] != 0x50 )
					{
						fprintf( stderr, "Remote device run-time record must be a cluster.\n" );
						return -1;
					}


					// Go through descriptor to figure out size of buffer
					//
					int nelements = sread_short( descriptor, &descriptor_index );
					for ( int i=0; i<nelements; i++ )
					{
						short nbytes = sread_short( descriptor, &descriptor_index );
						sread_short_bytes( descriptor, LV_type, &descriptor_index );
						descriptor_index = descriptor_index + nbytes - 4;

						buffer_size = buffer_size + buffer_bytes[ (int)LV_type[0] ];
					}
					fprintf( stdout, "buffer_size = %d\n", buffer_size );
					fflush( stdout );
					if ( buffer_size == 0 )
					{
						fprintf( stderr, "Buffer size 0.\n" );
						return -1;
					}
					char *buffer = (char *)malloc( buffer_size );


					// Now go through descriptor and shot file at the same time to load up the data
					//
					int shotfile_length = read_int( shotfile );
					descriptor_index = 6;
					int buffer_index = 0;
					for ( int i=0; i<nelements; i++ )
					{
						short nbytes = sread_short( descriptor, &descriptor_index );
						sread_short_bytes( descriptor, LV_type, &descriptor_index );
						descriptor_index = descriptor_index + nbytes - 4;

						// 8-bit integer types + boolean
						if ( (LV_type[0] == 0x01) || (LV_type[0] == 0x05) || (LV_type[0] == 0x15) || (LV_type[0] == 0x21) )
						{
							read_bool_byte( shotfile, &(buffer[buffer_index]) );
							buffer_index = buffer_index + 1;
						}
						
						// 16-bit integer types
						else if ( (LV_type[0] == 0x02) || (LV_type[0] == 0x06) || (LV_type[0] ==  0x16) )
						{
							read_short_bytes( shotfile, &(buffer[buffer_index]) );
							buffer_index = buffer_index + 2;
						}
						
						// 32-bit integer types
						else if ( (LV_type[0] == 0x03) || (LV_type[0] == 0x07) || (LV_type[0] == 0x17) )
						{
							read_int_bytes( shotfile, &(buffer[buffer_index]) );
							buffer_index = buffer_index + 4;
						}
						
						// 32-bit float types
						else if ( (LV_type[0] == 0x09) || (LV_type[0] == 0x19) )
						{
							read_float_bytes( shotfile, &(buffer[buffer_index]) );
							buffer_index = buffer_index + 4;
						}
						
						// 64-bit double types
						else if ( (LV_type[0] == 0x0A) || (LV_type[0] == 0x1A) )
						{
							read_double_bytes( shotfile, &(buffer[buffer_index]) );
							buffer_index = buffer_index + 8;
						}
						
						// string
						else if ( (LV_type[0] == 0x30) )
						{
							read_string( shotfile, &(buffer[buffer_index]) );
							buffer_index = buffer_index + 120;
						}

						else
						{
							fprintf( stderr, "Unrecognized sub-type within run-time record.\n" );
							return -1;
						}
					} // end loading up data


					// *********************************
					// *** Process the run-time list ***
					//
					// You have to:
					//   1) look for the dataset
					//   2) if found, extend it
					//   3) if not found, create it
					//   4) write the shot into it
					//   5) close the id's that were opened along the way
					//
					char group_name[1024];
					sprintf( group_name, "/Raw data + config/%s", device_name );

					err = find_dataset( file_id, group_name, "Run time list", &group_id, &found );  if ( err < 0 ) return -1;
					err = extend_create_remote_header( "Run time list", device_type, data_type, descriptor, buffer_size,
						found, group_id, index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
					if ( err < 0 ) return -1;
					err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, buffer );
					if ( err < 0 ) { H5Eprint( stderr ); return -1; }
					H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );

					free( buffer );
					free( descriptor );
				}

				fclose( shotfile );
				fprintf( stdout, "Finished %s...\n", device_name );
				fflush( stdout );
			} // End remote device case
		} // End looping through all devices

		free( shot_number );
		H5Fclose( file_id );
		*/
	}

	return 0;
}


