//========================================================================================
// Test.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"
#include <iostream>       // std::cout
#include <algorithm>      // std::copy
#include <vector>         // std::vector
#include <windows.h>

#define N 5


//========================================================================================
//----------------------------------------------------------------------------------------
// Main
//
int _tmain(int argc, _TCHAR* argv[])
{
	int list[] = {400,401,402,403,404};
    std::vector<int> vec(N);

	for (std::vector<int>::iterator it=std::copy( list, list+N, vec.begin() )-N; it<vec.end(); ++it) std::cout << *it << '\n';
 
    Sleep(5000);

    return 0;
}


