//========================================================================================
// TimeSlice.cpp : Defines the entry point for the console application.
//
// Usage:
//   TimeSlice.exe <infile> <motion list> <digitizer> <channel> <start> <end> <outfile>
//
// <infile>: full path of HDF5 file produced by ACQ II system
// <motion list>: name of motion list group as listed in <infile>
// <digitizer>: name of data acquisition device group as listed in <infile>
// <channel>: name of configuration/channel dataset as listed in <infile>
// <start>: starting timestep of chosen <channel>
// <end>: ending timestep of chosen <channel>
// <outfile>: full path of output HDF5 file
//
// The output HDF5 file will contain:
//   "Time slice attributes" group  
//   <channel> dataset with dimensions: (nt, ny, nx)
//
//
// In this file:
//   struct mrtl
//   struct ih
//   struct fh
//   mrtl readMotionRTList( hid_t rt_list_id, int list_index )
//   ih readIntHeader( hid_t header_id, int header_index )
//   fh readFloatHeader( hid_t header_id, int header_index )
//   int _tmain(int argc, _TCHAR* argv[])


#include "stdafx.h"
#include "hdf5.h"


//----------------------------------------------------------------------------------------
// Motion run-time list structure
//
struct mrtl
{
	int shot_number;
	double x;
	double y;
	double z;
	double theta;
	double phi;
	char motion_list[120];
	char probe_name[120];
};


//----------------------------------------------------------------------------------------
// Int header structure
//
struct ih
{
	int shot_number;
	double scale;
	double offset;
	short min;
	short max;
	char clipped;
};


//----------------------------------------------------------------------------------------
// Float header structure
//
struct fh
{
	int shot_number;
	double scale;
	double offset;
	float min;
	float max;
	char clipped;
};


//----------------------------------------------------------------------------------------
// Read motion run-time list
//
mrtl readMotionRTList( hid_t rt_list_id, int list_index )
{
	// Read the datatype, read the file space, and create the mem space
	//
	hid_t rt_list_type_id = H5Dget_type( rt_list_id );

	hid_t rt_list_filespace_id = H5Dget_space( rt_list_id );

	int rt_list_mem_rank = 1;
	hsize_t rt_list_mem_dims[1] = { 1 };
	hsize_t rt_list_mem_maxdims[1] = { 1 };
	hid_t rt_list_memspace_id =
		H5Screate_simple( rt_list_mem_rank, rt_list_mem_dims, rt_list_mem_maxdims );


	// Set up the hyperslab selection, this is needed to read a single row
	//
	hsize_t start[1] = { list_index };
	hsize_t stride[1] = { 1 };
	hsize_t count[1] = { 1 };
	hsize_t block[1] = { 1 };
	H5Sselect_hyperslab(
		rt_list_filespace_id,
		H5S_SELECT_SET,
		start,
		stride,
		count,
		block );


	// Read it into a char buffer.  For some reason the reading of compound datatypes is
	// not working correctly.
	//
	char rt_list_chars[284];
	H5Dread(
		rt_list_id,
		rt_list_type_id,
		rt_list_memspace_id,
		rt_list_filespace_id,
		H5P_DEFAULT,
		&rt_list_chars );


	// Flip the byte order for ints and doubles, copy the strings directly
	//
	char fixed_rt_list_chars[284];
	for ( int i=0; i<4; i++ ) fixed_rt_list_chars[3-i] = rt_list_chars[i];
	for ( int j=0; j<5; j++ )
	{
		int offset = 4 + j*8;
		for ( int i=0; i<8; i++ ) fixed_rt_list_chars[offset+7-i] = rt_list_chars[offset+i];
	}
	for ( int i=44; i<284; i++ ) fixed_rt_list_chars[i] = rt_list_chars[i];

	int *pShot_number = (int *)&(fixed_rt_list_chars[0]);
	double *pX = (double *)&(fixed_rt_list_chars[4]);
	double *pY = (double *)&(fixed_rt_list_chars[12]);
	double *pZ = (double *)&(fixed_rt_list_chars[20]);
	double *pTheta = (double *)&(fixed_rt_list_chars[28]);
	double *pPhi = (double *)&(fixed_rt_list_chars[36]);

	mrtl motion_rt_list;
	motion_rt_list.shot_number = *pShot_number;
	motion_rt_list.x = *pX;
	motion_rt_list.y = *pY;
	motion_rt_list.z = *pZ;
	motion_rt_list.theta = *pTheta;
	motion_rt_list.phi = *pPhi;
	strcpy( motion_rt_list.motion_list, &(fixed_rt_list_chars[44]) );
	strcpy( motion_rt_list.probe_name, &(fixed_rt_list_chars[164]) );

	H5Tclose( rt_list_type_id );
	H5Sclose( rt_list_filespace_id );
	H5Sclose( rt_list_memspace_id );

	return motion_rt_list;
};


//----------------------------------------------------------------------------------------
// Read integer header
//
ih readIntHeader( hid_t header_id, int header_index )
{
	// Read the datatype, read the file space, and create the mem space
	//
	hid_t header_type_id = H5Dget_type( header_id );

	hid_t header_file_space_id = H5Dget_space( header_id );

	int header_mem_rank = 1;
	hsize_t header_mem_dims[1] = { 1 };
	hsize_t header_mem_maxdims[1] = { 1 };
	hid_t header_mem_space_id =
		H5Screate_simple( header_mem_rank, header_mem_dims, header_mem_maxdims );


	// Set up the hyperslab selection, this is needed to read a single row
	//
	hsize_t start[1] = { header_index };
	hsize_t stride[1] = { 1 };
	hsize_t count[1] = { 1 };
	hsize_t block[1] = { 1 };
	H5Sselect_hyperslab(
		header_file_space_id,
		H5S_SELECT_SET,
		start,
		stride,
		count,
		block );


	// Read it into a char buffer.  For some reason the reading of compound datatypes is
	// not working correctly.
	//
	char header_chars[25];
	H5Dread(
		header_id,
		header_type_id,
		header_mem_space_id,
		header_file_space_id,
		H5P_DEFAULT,
		&header_chars );


	// Flip the byte order for ints and doubles, copy the strings directly
	//
	char fixed_header_chars[25];
	for ( int i=0; i<4; i++ ) fixed_header_chars[3-i] = header_chars[i];
	for ( int j=0; j<2; j++ )
	{
		int offset = 4 + j*8;
		for ( int i=0; i<8; i++ ) fixed_header_chars[offset+7-i] = header_chars[offset+i];
	}
	for ( int j=0; j<2; j++ )
	{
		int offset = 20 + j*2;
		for ( int i=0; i<2; i++ ) fixed_header_chars[offset+1-i] = header_chars[offset+i];
	}

	int *pShot_number = (int *)&(fixed_header_chars[0]);
	double *pScale = (double *)&(fixed_header_chars[4]);
	double *pOffset = (double *)&(fixed_header_chars[12]);
	short *pMin = (short *)&(fixed_header_chars[20]);
	short *pMax = (short *)&(fixed_header_chars[22]);
	char *pClipped = (char *)&(fixed_header_chars[24]);

	ih int_header;
	int_header.shot_number = *pShot_number;
	int_header.scale = *pScale;
	int_header.offset = *pOffset;
	int_header.min = *pMin;
	int_header.max = *pMax;
	int_header.clipped = *pClipped;

	H5Tclose( header_type_id );
	H5Sclose( header_file_space_id );
	H5Sclose( header_mem_space_id );

	return int_header;
}


//----------------------------------------------------------------------------------------
// Read float header
//
fh readFloatHeader( hid_t header_id, int header_index )
{
	// Read the datatype, read the file space, and create the mem space
	//
	hid_t header_type_id = H5Dget_type( header_id );

	hid_t header_file_space_id = H5Dget_space( header_id );

	int header_mem_rank = 1;
	hsize_t header_mem_dims[1] = { 1 };
	hsize_t header_mem_maxdims[1] = { 1 };
	hid_t header_mem_space_id =
		H5Screate_simple( header_mem_rank, header_mem_dims, header_mem_maxdims );


	// Set up the hyperslab selection, this is needed to read a single row
	//
	hsize_t start[1] = { header_index };
	hsize_t stride[1] = { 1 };
	hsize_t count[1] = { 1 };
	hsize_t block[1] = { 1 };
	H5Sselect_hyperslab(
		header_file_space_id,
		H5S_SELECT_SET,
		start,
		stride,
		count,
		block );


	// Read it into a char buffer.  For some reason the reading of compound datatypes is
	// not working correctly.
	//
	char header_chars[29];
	H5Dread(
		header_id,
		header_type_id,
		header_mem_space_id,
		header_file_space_id,
		H5P_DEFAULT,
		&header_chars );


	// Flip the byte order for ints and doubles, copy the strings directly
	//
	char fixed_header_chars[29];
	for ( int i=0; i<4; i++ ) fixed_header_chars[3-i] = header_chars[i];
	for ( int j=0; j<2; j++ )
	{
		int offset = 4 + j*8;
		for ( int i=0; i<8; i++ ) fixed_header_chars[offset+7-i] = header_chars[offset+i];
	}
	for ( int j=0; j<2; j++ )
	{
		int offset = 20 + j*4;
		for ( int i=0; i<4; i++ ) fixed_header_chars[offset+3-i] = header_chars[offset+i];
	}

	int *pShot_number = (int *)&(fixed_header_chars[0]);
	double *pScale = (double *)&(fixed_header_chars[4]);
	double *pOffset = (double *)&(fixed_header_chars[12]);
	float *pMin = (float *)&(fixed_header_chars[20]);
	float *pMax = (float *)&(fixed_header_chars[24]);
	char *pClipped = (char *)&(fixed_header_chars[28]);

	fh float_header;
	float_header.shot_number = *pShot_number;
	float_header.scale = *pScale;
	float_header.offset = *pOffset;
	float_header.min = *pMin;
	float_header.max = *pMax;
	float_header.clipped = *pClipped;

	H5Tclose( header_type_id );
	H5Sclose( header_file_space_id );
	H5Sclose( header_mem_space_id );

	return float_header;
}


//========================================================================================
//----------------------------------------------------------------------------------------
// Main
//
int _tmain(int argc, _TCHAR* argv[])
{
	//FILE* debug = fopen( "C:/Temp/TimeSlice.txt", "w" );


	// Expecting "TimeSlice.exe" plus 7 arguments
	//
	if ( argc != 8 )
	{
		fprintf( stderr, "argc = %d\n", argc );
		return -1;
	}


	// Read in the arguments
	//
	char infile_name[1024];
	char motion_list[1024];
	char digitizer[1024];
	char channel[1024];
	int start;
	int end;
	char outfile_name[1024];

	strcpy( infile_name, argv[1] );
	strcpy( motion_list, argv[2] );
	strcpy( digitizer, argv[3] );
	strcpy( channel, argv[4] );

	start = atoi(argv[5]);
	end = atoi(argv[6]);
	if ( end < start ) end = start;

	strcpy( outfile_name, argv[7] );


	// Open the input and output HDF5 files.
	//
	fprintf( stdout, "About to create input file: %s\n", infile_name );
	hid_t infile_id = H5Fopen( infile_name, 0, 0 );  // hid_t H5Fopen(const char *name, unsigned flags, hid_t access_id )
	if ( infile_id < 0 ) { H5Eprint( stderr ); return -1; }

	fprintf( stdout, "About to create output file: %s\n", outfile_name );
	hid_t outfile_id = H5Fcreate( outfile_name, 0, 0, 0 );  // hid_t H5Fcreate(const char *name, unsigned flags, hid_t create_id, hid_t access_id )
	if ( outfile_id < 0 ) { H5Eprint( stderr ); return -1; }


	// ************************************************
	// *** Create the "Time slice attributes" group ***
	//
	fprintf( stdout, "\nAbout to create <Time slice attributes> group\n" );
	hid_t group_id = H5Gcreate( outfile_id, "Time slice attributes", 1024 );
	if ( group_id < 0 ) { H5Eprint( stderr ); return -1; }

	hid_t attr_space_id = H5Screate( H5S_SCALAR );
	if ( attr_space_id < 0 ) { H5Eprint( stderr ); return -1; }


	// Add string attributes to attributes group: motion list, digitizer, channel
	//
	hid_t string_type_id = H5Tcopy( H5T_C_S1 );
	if ( string_type_id < 0 ) { H5Eprint( stderr ); return -1; }

	// Motion list
	fprintf( stdout, "Motion list: <%s>\n", motion_list );
	H5Tset_size( string_type_id, strlen(motion_list)+1 );
	hid_t attr_id = H5Acreate( group_id, "Motion list", string_type_id, attr_space_id, 0 );
	if ( attr_id < 0 ) { H5Eprint( stderr ); return -1; }

	H5Awrite( attr_id, string_type_id, motion_list );
	H5Aclose( attr_id );

	// Digitizer
	fprintf( stdout, "Digitizer: <%s>\n", digitizer );
	H5Tset_size( string_type_id, strlen(digitizer)+1 );
	attr_id = H5Acreate( group_id, "Digitizer", string_type_id, attr_space_id, 0 );
	if ( attr_id < 0 ) { H5Eprint( stderr ); return -1; }

	H5Awrite( attr_id, string_type_id, digitizer );
	H5Aclose( attr_id );

	// Channel
	fprintf( stdout, "Channel: <%s>\n", channel );
	H5Tset_size( string_type_id, strlen(channel)+1 );
	attr_id = H5Acreate( group_id, "Channel", string_type_id, attr_space_id, 0 );
	if ( attr_id < 0 ) { H5Eprint( stderr ); return -1; }

	H5Awrite( attr_id, string_type_id, channel );
	H5Aclose( attr_id );

	H5Tclose( string_type_id );


	// Add integer attributes to attributes group: start and end timesteps
	//
	fprintf( stdout, "Start timestep: %d\n", start );
	hid_t int_type_id = H5Tcopy( H5T_NATIVE_INT );
	if ( int_type_id < 0 ) { H5Eprint( stderr ); return -1; }

	// Start timestep
	attr_id = H5Acreate( group_id, "Start timestep", int_type_id, attr_space_id, 0 );
	if ( attr_id < 0 ) { H5Eprint( stderr ); return -1; }

	H5Awrite( attr_id, int_type_id, &start );
	H5Aclose( attr_id );

	// End timestep
	fprintf( stdout, "End timestep: %d\n", end );
	attr_id = H5Acreate( group_id, "End timestep", int_type_id, attr_space_id, 0 );
	if ( attr_id < 0 ) { H5Eprint( stderr ); return -1; }

	H5Awrite( attr_id, int_type_id, &end );
	H5Aclose( attr_id );

	H5Tclose( int_type_id );

	H5Sclose( attr_space_id );
	H5Gclose( group_id );


	// *************************************
	// *** Create the time slice dataset ***
	//
	// First, figure out the dimensions for the time slice dataset:
	//   ( nt, ny, nx )
	//   nt is specified by the start and end timesteps
	//   ny and nx are attributes of the motion list group
	//
	fprintf( stdout, "\nFiguring out dimensions for time slice dataset...\n" );
	int nt = end - start + 1;
	int ny = 0;
	int nx = 0;
	char group_name[1024];
	strcpy( group_name, "/Raw data + config/6K Compumotor/" );
	strcat( group_name, motion_list );
	group_id = H5Gopen( infile_id, group_name );
	if ( group_id < 0 ) { H5Eprint( stderr ); return -1; }

	int_type_id = H5Tcopy( H5T_NATIVE_INT );
	if ( int_type_id < 0 ) { H5Eprint( stderr ); return -1; }

	// Ny
	attr_id = H5Aopen_name( group_id, "Ny" );
	if ( attr_id < 0 ) { H5Eprint( stderr ); return -1; }

	H5Aread( attr_id, int_type_id, &ny );
	H5Aclose( attr_id );

	// Nx
	attr_id = H5Aopen_name( group_id, "Nx" );
	if ( attr_id < 0 ) { H5Eprint( stderr ); return -1; }

	H5Aread( attr_id, int_type_id, &nx );
	H5Aclose( attr_id );

	fprintf( stdout, "nt: %d\n", nt );
	fprintf( stdout, "ny: %d\n", ny );
	fprintf( stdout, "nx: %d\n", nx );

	H5Gclose( group_id );


	// Now, create the channel, memory, and output file dataspaces, etc. using the dimensions found above
	//
	// First, the channel dataspace
	//
	fprintf( stdout, "\nAbout to create channel dataspace\n" );
	char dataset_name[1024];
	strcpy( dataset_name, "/Raw data + config/" );
	strcat( dataset_name, digitizer );
	strcat( dataset_name, "/" );
	strcat( dataset_name, channel );
	hid_t channel_dataset_id = H5Dopen( infile_id, dataset_name );
	if ( channel_dataset_id < 0 ) { H5Eprint( stderr ); return -1; }

	hid_t channel_space_id = H5Dget_space( channel_dataset_id );
	if ( channel_space_id < 0 ) { H5Eprint( stderr ); return -1; }

	int channel_rank = 2;
	hsize_t channel_dims[2];
	hsize_t channel_maxdims[2];
	H5Sget_simple_extent_dims( channel_space_id, channel_dims, channel_maxdims );
	fprintf( stdout, "Channel dims: %d", channel_dims[0] );
	fprintf( stdout, " X %d\n", channel_dims[1] );

	// Get the channel data type, while we're at it
	hid_t channel_data_type_id = H5Dget_type( channel_dataset_id );
	if ( channel_data_type_id < 0 ) { H5Eprint( stderr ); return -1; }

	H5T_class_t channel_data_class = H5Tget_class( channel_data_type_id );
	if ( channel_data_class == H5T_INTEGER ) fprintf( stdout, "Channel data class: H5T_INTEGER\n" );
	else if ( channel_data_class == H5T_FLOAT ) fprintf( stdout, "Channel data class: H5T_FLOAT\n" );
	else
	{
		fprintf( stderr, "Channel data class: not H5T_INTEGER or H5T_FLOAT\n" );
		H5Eprint( stderr ); return -1;
	}


	// Second, the memory dataspace
	//
	fprintf( stdout, "\nAbout to create memory dataspace\n" );
	int mem_rank = 1;
	hsize_t mem_dims[1] = { nt };
	hsize_t mem_maxdims[1] = { nt };
	hid_t mem_space_id = H5Screate_simple( mem_rank, mem_dims, mem_maxdims );
	if ( mem_space_id < 0 ) { H5Eprint( stderr ); return -1; }

	fprintf( stdout, "Memory dims: %d\n", mem_dims[0] );


	// Third, the output dataspace
	//
	fprintf( stdout, "\nAbout to create output file dataspace\n" );
	int outfile_rank = 3;

	hsize_t outfile_dims[3] = { nt, ny, nx };
	hsize_t outfile_maxdims[3] = { nt, ny, nx };
	hid_t outfile_space_id = H5Screate_simple( outfile_rank, outfile_dims, outfile_maxdims );
	if ( outfile_space_id < 0 ) { H5Eprint( stderr ); return -1; }

	fprintf( stdout, "Output file dims: %d", outfile_dims[0] );
	fprintf( stdout, " X %d", outfile_dims[1] );
	fprintf( stdout, " X %d\n", outfile_dims[2] );


	// Remember, we're still working on creating the output file dataset.  The last part is the 
	// dataset creation property list
	//
	fprintf( stdout, "\nAbout to create output file dataset creation property list\n" );
	hid_t plist_id = H5Pcreate( H5P_DATASET_CREATE );
	if ( plist_id < 0 ) { H5Eprint( stderr ); return -1; }

	hsize_t outfile_chunk[3] = { nt, 1, 1 };
	int compression = 4;
	H5Pset_chunk( plist_id, outfile_rank, outfile_chunk );
	H5Pset_deflate( plist_id, compression );

	fprintf( stdout, "Output file chunk: %d", outfile_chunk[0] );
	fprintf( stdout, " X %d", outfile_chunk[1] );
	fprintf( stdout, " X %d\n", outfile_chunk[2] );
	fprintf( stdout, "Output file compression: %d\n", compression );


	// Finally!  Everything is set up so now create the time slice dataset
	//
	hid_t outfile_dataset_id = H5Dcreate( outfile_id, "Time slice", channel_data_type_id, outfile_space_id, plist_id );
	if ( outfile_dataset_id < 0 ) { H5Eprint( stderr ); return -1; }


	// ************************************
	// *** Write the time slice dataset ***
	//
	// Open the 6K Compumotor run-time list (used to match up motion and data)
	// and get the number of rows in the list
	//
	fprintf( stdout, "\nOpening 6K Compumotor run-time list\n" );
	hid_t rt_list_id =
		H5Dopen( infile_id, "/Raw data + config/6K Compumotor/Run time list" );
	if ( rt_list_id < 0 ) { H5Eprint( stderr ); return -1; }

	hid_t rt_list_filespace_id = H5Dget_space( rt_list_id );
	if ( rt_list_filespace_id < 0 ) { H5Eprint( stderr ); return -1; }

	hsize_t rt_list_dims[1];
	hsize_t rt_list_maxdims[1];
	H5Sget_simple_extent_dims( rt_list_filespace_id, rt_list_dims, rt_list_maxdims );
	H5Sclose( rt_list_filespace_id );

	fprintf( stdout, "Run-time list dims: %d\n", rt_list_dims[0] );


	// Open the channel header list (needed for the matching process mentioned above)
	// and get the number of rows in the header list
	//
	fprintf( stdout, "\nOpening channel header: " );
	char header_name[1024];
	strcpy( header_name, dataset_name );
	strcat( header_name, " headers" );
	fprintf( stdout, "%s\n", header_name );
	hid_t header_id = H5Dopen( infile_id, header_name );
	if ( header_id < 0 ) { H5Eprint( stderr ); return -1; }

	hid_t header_file_space_id = H5Dget_space( header_id );
	hsize_t header_dims[1];
	hsize_t header_maxdims[1];
	H5Sget_simple_extent_dims( header_file_space_id, header_dims, header_maxdims );
	H5Sclose( header_file_space_id );

	fprintf( stdout, "Header dims: %d\n", header_dims[0] );


	// Now open the datatype and test the "Min" element to see what structure type
	// to use
	hid_t header_type_id = H5Dget_type( header_id );
	if ( header_type_id < 0 ) { H5Eprint( stderr ); return -1; }

	int minIndex = H5Tget_member_index( header_type_id, "Min" );
	H5T_class_t header_class = H5Tget_member_class( header_type_id, minIndex );
	H5Tclose( header_type_id );

	if ( header_class == H5T_INTEGER ) fprintf( stdout, "Header <Min> data class: H5T_INTEGER\n" );
	else if ( header_class == H5T_FLOAT ) fprintf( stdout, "Header <Min> data class: H5T_FLOAT\n" );
	else 
	{
		fprintf( stderr, "Header <Min> data class: not H5T_INTEGER or H5T_FLOAT\n" );
		H5Eprint( stderr );
		return -1;
	}


	// Loop through the 6K Compumotor run-time list.  Whenever the motion list matches the chosen motion
	// list, look for a shot number match in the header list.  Write this data into the time slice dataset.
	// This is how the data are organized into a spatial grid.
	//
	// The code has to split depending on the header class
	//
	// ******************
	// *** int header ***
	//
	if ( header_class == H5T_INTEGER )
	{
		short *channel_data = (short *)malloc( (size_t)nt * 2 );

		// Pre-read all the motion list items and all header items.  Seems like you can only be
		// reading from one dataset at a time.
		//
		mrtl *motion_rt_list = (mrtl *)malloc( (size_t)rt_list_dims[0] * sizeof mrtl );
		for ( int i=0; i<rt_list_dims[0]; i++ )
			motion_rt_list[ i ] = readMotionRTList( rt_list_id, i );
		H5Dclose( rt_list_id );

		ih *int_headers = (ih *)malloc( (size_t)header_dims[0] * sizeof ih );
		for ( int i=0; i<header_dims[0]; i++ )
			int_headers[ i ] = readIntHeader( header_id, i );
		H5Dclose( header_id );
		int header_index = 0;


		// Loop through the run-time list (int header)
		//
		int ix = 0;
		int iy = 0;
		for ( int list_index=0; list_index<rt_list_dims[0]; list_index++ )
		{
			fprintf( stdout, "\n%d)", list_index );
			fprintf( stdout, " 6K Compumotor shot number: %d", motion_rt_list[list_index].shot_number );
			char compare_motion_list[1024] = "Motion list: ";
			strcat( compare_motion_list, motion_rt_list[list_index].motion_list );
			int strcmp_result = strcmp(motion_list, compare_motion_list);
			if ( strcmp_result == 0 )
			{
				// This is the chosen motion list (int header)
				fprintf( stdout, "Motion list name match...\n" );
				fprintf( stdout, "Channel shot number: %d\n", int_headers[header_index].shot_number );

				while ( int_headers[header_index].shot_number < motion_rt_list[list_index].shot_number )
				{
					// Read through channel headers until shot number catches up (int header)
					header_index++;
					if ( header_index >= header_dims[0] )
					{
						fprintf( stderr, "Fewer data records than motion list records\n" );
						return -1;
					}
					fprintf( stdout, "Channel shot number: %d\n", int_headers[header_index].shot_number );
				}
				if ( int_headers[header_index].shot_number == motion_rt_list[list_index].shot_number )
				{
					// Shot number match (float header)
					//
					fprintf( stdout, "Shot number match...\n" );
					fprintf( stdout, "Processing (iy, ix) = (%d", iy );
					fprintf( stdout, ", %d)\n", ix );


					// Read channel data into memory
					//
					hsize_t channel_start[2] = { header_index, start };
					hsize_t channel_stride[2] = { 1, 1 };
					hsize_t channel_count[2] = { 1, nt };
					hsize_t channel_block[2] = { 1, 1 };
					herr_t err = H5Sselect_hyperslab(
						channel_space_id,
						H5S_SELECT_SET,
						channel_start,
						channel_stride,
						channel_count,
						channel_block );
					if ( err < 0 ) { H5Eprint( stderr ); return -1; }

					err = H5Dread(
						channel_dataset_id,
						H5T_NATIVE_SHORT,
						mem_space_id,
						channel_space_id,
						H5P_DEFAULT,
						channel_data );
					if ( err < 0 ) { H5Eprint( stderr ); return -1; }


					// Write memory to output file (with different ordering
					//
					hsize_t output_start[3] = { 0, iy, ix };
					hsize_t output_stride[3] = { 1, 1, 1 };
					hsize_t output_count[3] = { nt, 1, 1 };
					hsize_t output_block[3] = { 1, 1, 1 };
					err = H5Sselect_hyperslab(
						outfile_space_id,
						H5S_SELECT_SET,
						output_start,
						output_stride,
						output_count,
						output_block );
					if ( err < 0 ) { H5Eprint( stderr ); return -1; }

					err = H5Dwrite(
						outfile_dataset_id,
						H5T_NATIVE_SHORT,
						mem_space_id,
						outfile_space_id,
						H5P_DEFAULT,
						channel_data );
					if ( err < 0 ) { H5Eprint( stderr ); return -1; }
				}
				ix++;
				if ( ix == nx ) { ix = 0; iy++; }
			}
		}
		free( int_headers );
		free( motion_rt_list );
		free( channel_data );
	}


	// ********************
	// *** float header ***
	//
	if ( header_class == H5T_FLOAT )
	{
		float *channel_data = (float *)malloc( (size_t)nt * 4 );

		// Pre-read all the motion list items and all header items.  Seems like you can only be
		// reading from one dataset at a time.
		//
		mrtl *motion_rt_list = (mrtl *)malloc( (size_t)rt_list_dims[0] * sizeof mrtl );
		for ( int i=0; i<rt_list_dims[0]; i++ )
			motion_rt_list[ i ] = readMotionRTList( rt_list_id, i );
		H5Dclose( rt_list_id );

		fh *float_headers = (fh *)malloc( (size_t)header_dims[0] * sizeof fh );
		for ( int i=0; i<header_dims[0]; i++ )
			float_headers[ i ] = readFloatHeader( header_id, i );
		H5Dclose( header_id );
		int header_index = 0;


		// Loop through the run-time list (float header)
		//
		int ix = 0;
		int iy = 0;
		for ( int list_index=0; list_index<rt_list_dims[0]; list_index++ )
		{
			fprintf( stdout, "\n%d)", list_index );
			fprintf( stdout, " 6K Compumotor shot number: %d", motion_rt_list[list_index].shot_number );
			char compare_motion_list[1024] = "Motion list: ";
			strcat( compare_motion_list, motion_rt_list[list_index].motion_list );
			int strcmp_result = strcmp(motion_list, compare_motion_list);
			if ( strcmp_result == 0 )
			{
				// This is the chosen motion list (float header)
				fprintf( stdout, "Motion list name match...\n" );
				fprintf( stdout, "Channel shot number: %d\n", float_headers[header_index].shot_number );

				while ( float_headers[header_index].shot_number < motion_rt_list[list_index].shot_number )
				{
					// Read through channel headers until shot number catches up (float header)
					header_index++;
					if ( header_index >= header_dims[0] )
					{
						fprintf( stderr, "Fewer data records than motion list records\n" );
						return -1;
					}
					fprintf( stdout, "Channel shot number: %d\n", float_headers[header_index].shot_number );
				}
				if ( float_headers[header_index].shot_number == motion_rt_list[list_index].shot_number )
				{
					// Shot number match (float header)
					//
					fprintf( stdout, "Shot number match...\n" );
					fprintf( stdout, "Processing (iy, ix) = (%d", iy );
					fprintf( stdout, ", %d)\n", ix );


					// Read channel data into memory
					//
					hsize_t channel_start[2] = { header_index, start };
					hsize_t channel_stride[2] = { 1, 1 };
					hsize_t channel_count[2] = { 1, nt };
					hsize_t channel_block[2] = { 1, 1 };
					herr_t err = H5Sselect_hyperslab(
						channel_space_id,
						H5S_SELECT_SET,
						channel_start,
						channel_stride,
						channel_count,
						channel_block );
					if ( err < 0 ) { H5Eprint( stderr ); return -1; }

					err = H5Dread(
						channel_dataset_id,
						H5T_NATIVE_FLOAT,
						mem_space_id,
						channel_space_id,
						H5P_DEFAULT,
						channel_data );
					if ( err < 0 ) { H5Eprint( stderr ); return -1; }


					// Write memory to output file (with different ordering
					//
					hsize_t output_start[3] = { 0, iy, ix };
					hsize_t output_stride[3] = { 1, 1, 1 };
					hsize_t output_count[3] = { nt, 1, 1 };
					hsize_t output_block[3] = { 1, 1, 1 };
					err = H5Sselect_hyperslab(
						outfile_space_id,
						H5S_SELECT_SET,
						output_start,
						output_stride,
						output_count,
						output_block );
					if ( err < 0 ) { H5Eprint( stderr ); return -1; }

					err = H5Dwrite(
						outfile_dataset_id,
						H5T_NATIVE_FLOAT,
						mem_space_id,
						outfile_space_id,
						H5P_DEFAULT,
						channel_data );
					if ( err < 0 ) { H5Eprint( stderr ); return -1; }
				}
				ix++;
				if ( ix == nx ) { ix = 0; iy++; }
			}
		}
		free( float_headers );
		free( motion_rt_list );
		free( channel_data );
	}
	fprintf ( stdout, "Finished loop.  About to close ids\n" );

	H5Dclose( outfile_dataset_id );
	H5Tclose( channel_data_type_id );

	H5Pclose( plist_id );
	H5Sclose( mem_space_id );
	H5Sclose( outfile_space_id );

	H5Fclose( outfile_id );
	H5Fclose( infile_id );

	return 0;
}


