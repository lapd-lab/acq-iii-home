//========================================================================================
// Color tables.cpp : Defines the entry point for the console application.
//
// Usage: Color tables.exe <Color tables folder>
//
// Output: <Color tables folder>/Color table list.txt
//         <Color tables folder>/<Color table 1>.txt
//         <Color tables folder>/...
//
//

#include "stdafx.h"
#include "hdf5.h"
#include "H5Fpublic.h"


//----------------------------------------------------------------------------------------
// Color table structure
//
struct ct
{
	float table_x;
	float table_y;
	float table_z;
};


//----------------------------------------------------------------------------------------
// Read color table
//
ct readColorTable( hid_t dataset_id, int index )
{
	// Read the datatype, read the file space, and create the mem space
	//
	hid_t type_id = H5Dget_type( dataset_id );

	hid_t file_space_id = H5Dget_space( dataset_id );

	int mem_rank = 1;
	hsize_t mem_dims[1] = { 1 };
	hsize_t mem_maxdims[1] = { 1 };
	hid_t mem_space_id =
		H5Screate_simple( mem_rank, mem_dims, mem_maxdims );


	// Set up the hyperslab selection, this is needed to read a single row
	//
	hsize_t start[1] = { index };
	hsize_t stride[1] = { 1 };
	hsize_t count[1] = { 1 };
	hsize_t block[1] = { 1 };
	H5Sselect_hyperslab(
		file_space_id,
		H5S_SELECT_SET,
		start,
		stride,
		count,
		block );


	// Read it into a char buffer.  For some reason the reading of compound datatypes is
	// not working correctly.
	//
	char struct_chars[12];
	H5Dread(
		dataset_id,
		type_id,
		mem_space_id,
		file_space_id,
		H5P_DEFAULT,
		&struct_chars );

	float *pTable_x = (float *)&(struct_chars[0]);
	float *pTable_y = (float *)&(struct_chars[4]);
	float *pTable_z = (float *)&(struct_chars[8]);

	ct color_table;
	color_table.table_x = *pTable_x;
	color_table.table_y = *pTable_y;
	color_table.table_z = *pTable_z;

	H5Tclose( type_id );
	H5Sclose( file_space_id );
	H5Sclose( mem_space_id );

	return color_table;
}


//========================================================================================
//----------------------------------------------------------------------------------------
// Main
//
int _tmain(int argc, _TCHAR* argv[])
{
	if ( argc != 2 ) { fprintf( stderr, "argc = %d\n", argc ); return -1; }


	// Open the output stream and the input HDF5 file.
	//
	char txt_filename[1024];
	strcpy( txt_filename, argv[1] );
	strcat( txt_filename, "/Color table list.txt" );
	FILE *txt_stream = fopen( txt_filename, "w" );

	char HDF5_filename[1024];
	strcpy( HDF5_filename, argv[1] );
	strcat( HDF5_filename, "/Color tables plus 3.hdf5" );
	hid_t file_id = H5Fopen( HDF5_filename, H5F_ACC_RDONLY, 0 );  // hid_t H5Fopen(const char *name, unsigned flags, hid_t access_id )
	if ( file_id < 0 )
	{
		H5Eprint2( H5E_DEFAULT, stderr );
		return -1;
	}


	// Open the /Color tables group.  The goal is to list all the color tables, then write each one out to file.
	//
	hid_t group_id = H5Gopen( file_id, "/Color tables", H5P_DEFAULT );
	if ( group_id < 0 )
	{
		H5Eprint2( H5E_DEFAULT, stderr );
		return -1;
	}
	else
	{
		hsize_t num_objs;
		herr_t err = H5Gget_num_objs(group_id, &num_objs);
		if ( err < 0 ) num_objs = 0;

		int color_table_count = 0;
		char name[120];

		// Loop through names to make list
		//
		for ( hsize_t idx=0; idx<num_objs; idx++ )
		{
			ssize_t size = H5Gget_objname_by_idx(group_id, idx, name, 120 );
			fprintf( txt_stream, "%s\n", name );
		}
		fclose( txt_stream );

		// Loop through names again to write out each color table
		//
		for ( hsize_t idx=0; idx<num_objs; idx++ )
		{
			ssize_t size = H5Gget_objname_by_idx(group_id, idx, name, 120 );
			strcpy( txt_filename, argv[1] );
			strcat( txt_filename, "/" );
			strcat( txt_filename, name );
			strcat( txt_filename, ".txt" );
			FILE *txt_stream = fopen( txt_filename, "w" );

			// Open the current color table
			hid_t dataset_id = H5Dopen( group_id, name, H5P_DEFAULT );
			if ( dataset_id < 0 ) { H5Eprint2( H5E_DEFAULT, stderr ); return -1; }

			// Get the dimensions
			hid_t file_space_id = H5Dget_space( dataset_id );
			hsize_t dims[1];
			hsize_t maxdims[1];
			H5Sget_simple_extent_dims( file_space_id, dims, maxdims );
			H5Sclose( file_space_id );

			ct color_table;
			for ( int i=0; i<dims[0]; i++ )
			{
				color_table = readColorTable( dataset_id, i );
				fprintf( txt_stream, "%f %f %f\n", color_table.table_x, color_table.table_y, color_table.table_z );
			}

			H5Dclose( dataset_id );
			fclose( txt_stream );
		}

		H5Gclose( group_id );
	}


	fclose( txt_stream );
	H5Fclose( file_id );

	return 0;
}

