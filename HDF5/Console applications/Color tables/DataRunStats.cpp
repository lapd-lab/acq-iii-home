//========================================================================================
// DataRunStats.cpp : Defines the entry point for the console application.
//
// Usage: DataRunStats.exe <HDF5 filename>
//
// Output: C:/Temp/DataRunStats.txt
//
// Motion list count
//   -for each motion list:
//    Motion list name
//
// Digitizer count
//   -for each digitizer:
//    Digitizer name
//    Data channel count
//      -for each channel
//       Data channel name
//       Timesteps
//

#include "stdafx.h"
#include "hdf5.h"
#include "H5Fpublic.h"

int _tmain(int argc, _TCHAR* argv[])
{
	if ( argc != 2 ) { fprintf( stderr, "argc = %d\n", argc ); return -1; }


	// Open the output stream and the input HDF5 file.
	//
	FILE *stream = fopen( "C:/Temp/DataRunStats.txt", "w" );
	hid_t file_id = H5Fopen( argv[1], 0, 0 );  // hid_t H5Fopen(const char *name, unsigned flags, hid_t access_id )
	if ( file_id < 0 )
	{
		fprintf( stream, "0\n" );
		fprintf( stream, "0\n" );
		fclose( stream );

		H5Eprint( stderr );
		return -1;
	}


	// Open the /Raw data + config/6K Compumotor group.  The goal is to find all the motion lists
	//
	hid_t group_id = H5Gopen( file_id, "/Raw data + config/6K Compumotor" );
	if ( group_id < 0 )
	{
		fprintf( stream, "0\n" );
	}
	else
	{
		hsize_t num_objs;
		herr_t err = H5Gget_num_objs(group_id, &num_objs);
		if ( err < 0 ) num_objs = 0;

		int motion_list_count = 0;
		bool isMotionList[1000];
		char name[120];
		char namePrefix[14];
		for ( hsize_t idx=0; idx<num_objs; idx++ )
		{
			isMotionList[idx] = false;
			ssize_t size = H5Gget_objname_by_idx(group_id, idx, name, 120 );
			for ( int i=0; i<13; i++ ) namePrefix[i] = name[i];
			namePrefix[13] = '\0';
			if ( strcmp(namePrefix, "Motion list: ") == 0 )
			{
				isMotionList[idx] = true;
				motion_list_count++;
			}
		}
		fprintf( stream, "%d\n", motion_list_count );

		for ( hsize_t idx=0; idx<num_objs; idx++ )
		{
			ssize_t size = H5Gget_objname_by_idx(group_id, idx, name, 120 );
			if ( isMotionList[idx] ) fprintf( stream, "%s\n", name );
		}
		H5Gclose( group_id );
	}


	// Open /Raw data + config group.  The goal is to find all the data acquisition devices.
	//
	group_id = H5Gopen( file_id, "/Raw data + config" );
	if ( group_id < 0 )
	{
		fprintf( stream, "0\n" );
	}
	else
	{
		// Find the digitizers and count them.
		//
		hsize_t num_objs;
		herr_t err = H5Gget_num_objs(group_id, &num_objs);
		if ( err < 0 ) num_objs = 0;

		int digitizer_count = 0;
		bool isDigitizer[1000];
		char name[120];
		char attribute[120];
		for ( hsize_t idx=0; idx<num_objs; idx++ )
		{
			isDigitizer[idx] = false;
			ssize_t size = H5Gget_objname_by_idx(group_id, idx, name, 120 );
			hid_t device_group_id = H5Gopen( group_id, name );
			if ( device_group_id >= 0 )
			{
				hid_t attr_id = H5Aopen_name( device_group_id, "Type" );
				if ( attr_id >= 0 )
				{
					hid_t attr_type_id = H5Aget_type( attr_id );
					if ( attr_type_id >= 0 )
					{
						err = H5Aread( attr_id, attr_type_id, attribute );
						if ( err < 0 ) strcpy( attribute, "");
						if ( strcmp(attribute, "Data acquisition") == 0 )
						{
							isDigitizer[idx] = true;
							digitizer_count++;
						}
						H5Tclose( attr_type_id );
					}
					H5Aclose( attr_id );
				}
				H5Gclose( device_group_id );
			}
		}
		fprintf( stream, "%d\n", digitizer_count );

		for ( hsize_t idx=0; idx<num_objs; idx++ )
		{
			if ( isDigitizer[idx] )
			{
				char name[120];
				ssize_t size = H5Gget_objname_by_idx(group_id, idx, name, 120 );
				fprintf( stream, "%s\n", name );
			}
		}


		// Now, for each digitizer get the data channels and the timesteps for each channel.
		//
		for ( hsize_t idx=0; idx<num_objs; idx++ )
		{
			if ( isDigitizer[idx] )
			{
				// This object is a digitizer group...
				char name[120];
				ssize_t size = H5Gget_objname_by_idx(group_id, idx, name, 120 );
				hid_t device_group_id = H5Gopen( group_id, name );
				if ( device_group_id >= 0 )
				{
					// Digitizer group opened correctly...
					hsize_t num_digitizer_objs;
					int num_channels = 0;
					bool isChannel[1000];
					hsize_t timesteps[1000];
					herr_t err = H5Gget_num_objs(device_group_id, &num_digitizer_objs);
					if ( err < 0 ) num_digitizer_objs = 0;
					for ( hsize_t idx2=0; idx2<num_digitizer_objs; idx2++ )
					{
						// Now for each object in the digitizer group...
						isChannel[idx2] = false;
						int obj_type = H5Gget_objtype_by_idx(device_group_id, idx2 );
						if ( obj_type == 2 )  // 2 == H5G_DATASET
						{
							// This is a dataset...
							char dataset_name[120];
							ssize_t size = H5Gget_objname_by_idx(device_group_id, idx2, dataset_name, 120 );
							hid_t dataset_id = H5Dopen( device_group_id, dataset_name );
							hid_t space_id = H5Dget_space( dataset_id );

							hsize_t maxdims[1000];
							hsize_t dims[1000];
							int ndims = H5Sget_simple_extent_dims( space_id, dims, maxdims );
							if ( ndims == 2 ) 
							{
								// This is an actual data channel, as opposed to a data header...
								isChannel[idx2] = true;
								timesteps[idx2] = dims[1];
								num_channels++;
							}
							H5Sclose( space_id );
							H5Dclose( dataset_id );
						}
					}
					fprintf( stream, "%d\n", num_channels );

					for ( hsize_t idx2=0; idx2<num_digitizer_objs; idx2++ )
					{
						if ( isChannel[idx2] )
						{
							char dataset_name[120];
							ssize_t size = H5Gget_objname_by_idx(device_group_id, idx2, dataset_name, 120 );
							fprintf( stream, "%s\n", dataset_name );
							fprintf( stream, "%d\n", timesteps[idx2] );
						}
					}

					H5Gclose( device_group_id );
				}
			}
		}
		H5Gclose( group_id );
	}

	fclose( stream );
	H5Fclose( file_id );

	return 0;
}

