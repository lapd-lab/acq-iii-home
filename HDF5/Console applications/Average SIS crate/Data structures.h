//========================================================================================
// Data structures.h : a data structure to hold one channel of SIS crate average shot data,
//   plus various other random things.
//
//


struct subChannelStruct
{
	int ndata;
	unsigned int *data;  // has to be int to allow for summing without overflow
};


struct channelStruct
{
	char channel_name[1000];
	unsigned short channel_mode;
	short average_count;
	short average_shot_number;
	float vertical_scale;
	float vertical_offset;
	char enabled_code;
	int nU32;
	subChannelStruct sub_channels[4];
};


static char* channel_modes[4] =
{
	"4 sub-channels possible",
	"2 sub-channels possible",
	"1 sub-channel possible",
	"1 sub-channel possible"
};


static char* enabled_codes[16] =
{
	"0000",
	"0001",
	"0010",
	"0011",
	"0100",
	"0101",
	"0110",
	"0111",
	"1000",
	"1001",
	"1010",
	"1011",
	"1100",
	"1101",
	"1110",
	"1111"
};


static char* sub_channel_suffixes[4] =
{
	" ch 1",
	" ch 2",
	" ch 3",
	" ch 4"
};

