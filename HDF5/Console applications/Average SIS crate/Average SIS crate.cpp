//========================================================================================
// Average SIS crate.cpp : Defines the entry point for the console application.
//
// Usage:
//   Average_SIS_crate.exe <experiment set> <experiment> <data run> <start shot> <end shot>
//
// <experiment set> <experiment> <data run>: 3 strings which locate the shot files
// <start shot> <end shot>: Shot numbers to process (first shot is #1).
//
// This is based on H5D_Insert_entries.cpp.  It was modified to process the
// "SIS crate 08.SIS crate.shot000001.avg1.dat" type files.  It averages the data in these
// files and writes a single "SIS crate 08.SIS crate.shot000001.dat" file in a format
// similar to the TDS6154C files, except that the data are U16.  Once the average is
// successful for a shot, the original "shot.avg" files are deleted.
//


#include "stdafx.h"
#include "Data structures.h"

#define MAX_CHANS 400
#define BLOCK_SIZE_3305 16


//----------------------------------------------------------------------------------------
// Init SIS 3302 sub_channel data array
//
void init_3302_data( 
	channelStruct &channel )
{
	channel.sub_channels[0].ndata = 2 * channel.nU32;
	channel.sub_channels[0].data = (unsigned int *)malloc( 4 * channel.sub_channels[0].ndata );
}


//----------------------------------------------------------------------------------------
// Init SIS 3305 sub_channel data array
//
void init_3305_data( 
	channelStruct &channel )
{
	int samples_per_block;
	if ( channel.channel_mode == 0 ) samples_per_block = 12;
	if ( channel.channel_mode == 1 ) samples_per_block = 24;
	if ( channel.channel_mode == 2 ) samples_per_block = 48;

	for ( int j=0; j<4; j++ )
	{
		if ( (channel.enabled_code & (1<<j)) > 0 )  // check flag bits
		{
			channel.sub_channels[j].ndata = samples_per_block * channel.nU32 / BLOCK_SIZE_3305;
			channel.sub_channels[j].data = (unsigned int *)malloc( 4 * channel.sub_channels[j].ndata );
		}
	}  // end looping through sub-channels
}


//----------------------------------------------------------------------------------------
// Assign SIS 3302 sub_channel data array
//
void assign_3302_data( 
	channelStruct &channel,
	int *raw_U32_data )
{
	for ( int i=0; i<channel.nU32; i++ )
	{
		channel.sub_channels[0].data[i*2  ] = raw_U32_data[i] & 0x0000FFFF;
		channel.sub_channels[0].data[i*2+1] = (raw_U32_data[i] >> 16) & 0x0000FFFF;
	}
}


//----------------------------------------------------------------------------------------
// Sum SIS 3302 sub_channel data array
//
void sum_3302_data( 
	channelStruct &channel,
	int *raw_U32_data )
{
	for ( int i=0; i<channel.nU32; i++ )
	{
		channel.sub_channels[0].data[i*2  ] += raw_U32_data[i] & 0x0000FFFF;
		channel.sub_channels[0].data[i*2+1] += (raw_U32_data[i] >> 16) & 0x0000FFFF;
	}
}


//----------------------------------------------------------------------------------------
// Assign SIS 3305 sub_channel data array
//
void assign_3305_data( 
	channelStruct &channel,
	int *raw_U32_data )
{
	int iraw;     // raw data index
	int ib_blk;   // data block index base
	int ib_blk2;  // data block secondary index base
	int ib_sub;   // sub-channel data index base
	int ib_sub2;  // sub-channel data secondary index base

	int data_block[48];


	// Step through raw data array in blocks of 16 U32's
	//
	for ( int i=0; i<channel.nU32/16; i++ )
	{
		for ( int ii=0; ii<16; ii++ )  // loop through this block of 16 U32's
		{
			// translate the 16 U32's into 48 numbers
			iraw = i*16 + ii;
			ib_blk = ii*3;

			data_block[ib_blk  ] = (raw_U32_data[iraw] >> 20) & 0x000003FF;
			data_block[ib_blk+1] = (raw_U32_data[iraw] >> 10) & 0x000003FF;
			data_block[ib_blk+2] = raw_U32_data[iraw] & 0x000003FF;
		}

		// -------------------
		// 4 channels per FPGA
		//   16 U32's turn into 48 numbers
		//   data block:  0-11 --> sub-channel 1: 0-11
		//   data block: 12-23 --> sub-channel 2: 0-11
		//   data block: 24-35 --> sub-channel 3: 0-11
		//   data block: 36-47 --> sub-channel 4: 0-11
		if ( channel.channel_mode == 0 )
		{
			ib_sub = i*12;  // index base for sub-channel data
			for ( int j=0; j<4; j++ )  // loop through 4 sub-channels
			{
				// distribute the data block into the sub-channels
				if ( (channel.enabled_code & (1<<j)) > 0 )  // check flag bits to see if this sub-channel enabled
				{
					ib_blk = j*12;  // index base for data block
					for ( int k=0; k<12; k++ )
					{
						channel.sub_channels[j].data[ib_sub+k] = data_block[ib_blk+k];
					}
				}  // end if sub-channel enabled
			}  // end looping through sub-channels
		}  // end if 4 channels per FPGA


		// -------------------
		// 2 channels per FPGA
		//   16 U32's turn into 48 numbers
		//   data block:  0-11 --> sub-channel 1: 0,2,4,...,22
		//   data block: 12-23 --> sub-channel 1: 1,3,5,...,23
		//   data block: 24-35 --> sub-channel 3: 0,2,4,...,22
		//   data block: 36-47 --> sub-channel 3: 1,3,5,...,23
		if ( channel.channel_mode == 1 )
		{
			ib_sub = i*24;  // index base for sub-channel data
			for ( int j=0; j<4; j=j+2 )  // process sub-channels 1 and 3
			{
				if ( (channel.enabled_code & (1<<j)) > 0 )  // check flag bits
				{
					ib_blk = j*12;  // index base for raw data
					for ( int k=0; k<12; k++ )
					{
						ib_blk2 = ib_blk+k;
						ib_sub2 = ib_sub+k*2;
						channel.sub_channels[j].data[ib_sub2  ] = data_block[ib_blk2   ];
						channel.sub_channels[j].data[ib_sub2+1] = data_block[ib_blk2+12];
					}
				}  // end if sub-channel enabled
			}  // end processing sub-channels 1 and 3
		}  // end if 2 channels per FPGA


		// ------------------
		// 1 channel per FPGA
		//   16 U32's turn into 48 numbers
		//   data block:  0-11 --> sub-channel 1: 0,4, 8,...,44
		//   data block: 12-23 --> sub-channel 1: 2,6,10,...,46
		//   data block: 24-35 --> sub-channel 1: 1,5, 9,...,45
		//   data block: 36-47 --> sub-channel 1: 3,7,11,...,47
		if ( channel.channel_mode == 2 )
		{
			ib_sub = i*48;  // index base for sub-channel data

			// Note: no need to check enabled sub-channels, we know that sub-channel 1
			// (and only sub-channel 1) is enabled in order to cause raw data to be written in
			// the first place.

			for ( int k=0; k<12; k++ )
			{
				ib_sub2 = ib_sub+k*4;
				channel.sub_channels[0].data[ib_sub2  ] = data_block[k   ];
				channel.sub_channels[0].data[ib_sub2+2] = data_block[k+12];
				channel.sub_channels[0].data[ib_sub2+1] = data_block[k+24];
				channel.sub_channels[0].data[ib_sub2+3] = data_block[k+36];
			}
		}  // end if 1 channel per FPGA
	}  // end looping through raw data array

}


//----------------------------------------------------------------------------------------
// Sum SIS 3305 sub_channel data array
//
void sum_3305_data( 
	channelStruct &channel,
	int *raw_U32_data )
{
	int iraw;     // raw data index
	int ib_blk;   // data block index base
	int ib_blk2;  // data block secondary index base
	int ib_sub;   // sub-channel data index base
	int ib_sub2;  // sub-channel data secondary index base

	int data_block[48];


	// Step through raw data array in blocks of 16 U32's
	//
	for ( int i=0; i<channel.nU32/16; i++ )
	{
		for ( int ii=0; ii<16; ii++ )  // loop through this block of 16 U32's
		{
			// translate the 16 U32's into 48 numbers
			iraw = i*16 + ii;
			ib_blk = ii*3;

			data_block[ib_blk  ] = (raw_U32_data[iraw] >> 20) & 0x000003FF;
			data_block[ib_blk+1] = (raw_U32_data[iraw] >> 10) & 0x000003FF;
			data_block[ib_blk+2] = raw_U32_data[iraw] & 0x000003FF;
		}

		// -------------------
		// 4 channels per FPGA
		//   16 U32's turn into 48 numbers
		//   data block:  0-11 --> sub-channel 1: 0-11
		//   data block: 12-23 --> sub-channel 2: 0-11
		//   data block: 24-35 --> sub-channel 3: 0-11
		//   data block: 36-47 --> sub-channel 4: 0-11
		if ( channel.channel_mode == 0 )
		{
			ib_sub = i*12;  // index base for sub-channel data
			for ( int j=0; j<4; j++ )  // loop through 4 sub-channels
			{
				// distribute the data block into the sub-channels
				if ( (channel.enabled_code & (1<<j)) > 0 )  // check flag bits to see if this sub-channel enabled
				{
					ib_blk = j*12;  // index base for data block
					for ( int k=0; k<12; k++ )
					{
						channel.sub_channels[j].data[ib_sub+k] += data_block[ib_blk+k];
					}
				}  // end if sub-channel enabled
			}  // end looping through sub-channels
		}  // end if 4 channels per FPGA


		// -------------------
		// 2 channels per FPGA
		//   16 U32's turn into 48 numbers
		//   data block:  0-11 --> sub-channel 1: 0,2,4,...,22
		//   data block: 12-23 --> sub-channel 1: 1,3,5,...,23
		//   data block: 24-35 --> sub-channel 3: 0,2,4,...,22
		//   data block: 36-47 --> sub-channel 3: 1,3,5,...,23
		if ( channel.channel_mode == 1 )
		{
			ib_sub = i*24;  // index base for sub-channel data
			for ( int j=0; j<4; j=j+2 )  // process sub-channels 1 and 3
			{
				if ( (channel.enabled_code & (1<<j)) > 0 )  // check flag bits
				{
					ib_blk = j*12;  // index base for raw data
					for ( int k=0; k<12; k++ )
					{
						ib_blk2 = ib_blk+k;
						ib_sub2 = ib_sub+k*2;
						channel.sub_channels[j].data[ib_sub2  ] += data_block[ib_blk2   ];
						channel.sub_channels[j].data[ib_sub2+1] += data_block[ib_blk2+12];
					}
				}  // end if sub-channel enabled
			}  // end processing sub-channels 1 and 3
		}  // end if 2 channels per FPGA


		// ------------------
		// 1 channel per FPGA
		//   16 U32's turn into 48 numbers
		//   data block:  0-11 --> sub-channel 1: 0,4, 8,...,44
		//   data block: 12-23 --> sub-channel 1: 2,6,10,...,46
		//   data block: 24-35 --> sub-channel 1: 1,5, 9,...,45
		//   data block: 36-47 --> sub-channel 1: 3,7,11,...,47
		if ( channel.channel_mode == 2 )
		{
			ib_sub = i*48;  // index base for sub-channel data

			// Note: no need to check enabled sub-channels, we know that sub-channel 1
			// (and only sub-channel 1) is enabled in order to cause raw data to be written in
			// the first place.

			for ( int k=0; k<12; k++ )
			{
				ib_sub2 = ib_sub+k*4;
				channel.sub_channels[0].data[ib_sub2  ] += data_block[k   ];
				channel.sub_channels[0].data[ib_sub2+2] += data_block[k+12];
				channel.sub_channels[0].data[ib_sub2+1] += data_block[k+24];
				channel.sub_channels[0].data[ib_sub2+3] += data_block[k+36];
			}
		}  // end if 1 channel per FPGA
	}  // end looping through raw data array

}


//----------------------------------------------------------------------------------------
// Find channel
//
int find_channel( 
	channelStruct *channels,
	int full_channel_count,
	char *channel_name )
{
	int channel_index = -1;
	for ( int i=0; i<full_channel_count; i++ )
	{
		if ( strcmp(channels[i].channel_name, channel_name) == 0 ) channel_index = i;
	}

	return channel_index;
}


//----------------------------------------------------------------------------------------
// Form average
//
void form_average( 
	channelStruct *channels,
	int full_channel_count )
{
	for ( int i=0; i<full_channel_count; i++ )
	{
		for ( int j=0; j<4; j++ )
		{
			int ndata = channels[i].sub_channels[j].ndata;
			for ( int n=0; n<ndata; n++ )
			{
				channels[i].sub_channels[j].data[n] = 
					channels[i].sub_channels[j].data[n] / channels[i].average_count;
			}
		}
	}
}


//----------------------------------------------------------------------------------------
// Write shot file
//
int write_shot_file( 
	FILE *shot_file,
	FILE *log_file,
	char *configuration_name,
	channelStruct *channels,
	int full_channel_count )
{
	size_t count;
	int err = 0;

	float vertical_scale;
	float vertical_offset;
	int ndata;
	unsigned short *buffer;

	int string_length;
	string_length = strlen( configuration_name );
	count = fwrite( &string_length, 4, 1, shot_file );  if ( count != 1 ) err = -1;
	count = fwrite( configuration_name, 1, string_length, shot_file );  if ( count != string_length ) err = -1;

	int full_sub_channel_count = 0;
	for ( int i=0; i<full_channel_count; i++ )
		for ( int j=0; j<4; j++ )
			if ( channels[i].sub_channels[j].ndata > 0 ) full_sub_channel_count++;

	count = fwrite( &full_sub_channel_count, 4, 1, shot_file );  if ( count != 1 ) err = -1;

	for ( int i=0; i<full_channel_count; i++ )
	{
		// SIS 3302
		//
		if ( channels[i].channel_mode == 3 )
		{
			// We know that the 3302 has only one sub-channel
			string_length = strlen( channels[i].channel_name );
			count = fwrite( &string_length, 4, 1, shot_file );  if ( count != 1 ) err = -1;
			count = fwrite( channels[i].channel_name, 1, string_length, shot_file );  if ( count != string_length ) err = -1;

			vertical_scale = channels[i].vertical_scale;
			count = fwrite( &vertical_scale, 4, 1, shot_file );  if ( count != 1 ) err = -1;
			vertical_offset = channels[i].vertical_offset;
			count = fwrite( &vertical_offset, 4, 1, shot_file );  if ( count != 1 ) err = -1;

			ndata = channels[i].sub_channels[0].ndata;
			count = fwrite( &ndata, 4, 1, shot_file );  if ( count != 1 ) err = -1;

			buffer = (unsigned short *)malloc( 2*ndata );
			for ( int n=0; n<ndata; n++ ) buffer[n] = channels[i].sub_channels[0].data[n];
			count = fwrite( buffer, 2, ndata, shot_file );  if ( count != ndata ) err = -1;
			free( buffer );
		}  // end if SIS 3302

		// SIS 3305
		//
		if ( (channels[i].channel_mode == 0) || (channels[i].channel_mode == 1) || (channels[i].channel_mode == 2) )
		{
			string_length = strlen( channels[i].channel_name ) + 5;  // e.g. + " ch 1"

			// The 3305 can have multiple sub-channels
			for ( int j=0; j<4; j++ )
			{
				ndata = channels[i].sub_channels[j].ndata;
				if ( ndata > 0 )
				{
					count = fwrite( &string_length, 4, 1, shot_file );  if ( count != 1 ) err = -1;
					count = fwrite( channels[i].channel_name, 1, string_length-5, shot_file );  if ( count != string_length-5 ) err = -1;
					count = fwrite( sub_channel_suffixes[j], 1, 5, shot_file );  if ( count != 5 ) err = -1;

					vertical_scale = channels[i].vertical_scale;
					count = fwrite( &vertical_scale, 4, 1, shot_file );  if ( count != 1 ) err = -1;
					vertical_offset = channels[i].vertical_offset;
					count = fwrite( &vertical_offset, 4, 1, shot_file );  if ( count != 1 ) err = -1;

					count = fwrite( &ndata, 4, 1, shot_file );  if ( count != 1 ) err = -1;

					buffer = (unsigned short *)malloc( 2*ndata );
					for ( int n=0; n<ndata; n++ ) buffer[n] = channels[i].sub_channels[j].data[n];
					count = fwrite( buffer, 2, ndata, shot_file );  if ( count != ndata ) err = -1;
					free( buffer );
				}  // end if this sub-channel has data
			}  // end looping through sub-channels
		}  // end if SIS 3305

	}  // end looping through channels

	if ( err != 0 )
	{
		fprintf( stdout, "Error writing shot file\n" );
		fprintf( log_file, "Error writing shot file\n" );
	}
	return err;
}


//----------------------------------------------------------------------------------------
// Read average shot file
//
int read_average_shot_file(
	FILE *average_shot_file,
	FILE *log_file,
	char *prev_configuration_name,
	channelStruct *channels,
	int &full_channel_count,
	short average_shot_number_in,
	short &max_average_count )
{
	// Things we expect to read in a shot average file
	//
	char configuration_name[1000];
	int channel_count;
	char channel_name[1000];
	unsigned short channel_mode;
	short average_count;
	short average_shot_number;
	float vertical_scale;
	float vertical_offset;
	char enabled_code;
	int nU32;
	int *raw_U32_data;  // int or unsigned int shouldn't matter

	bool new_configuration = false;
	int channel_index;

	// Read configuration name to see if it has changed
	int string_length;
	fread( &string_length, 4, 1, average_shot_file );
	fread( configuration_name, 1, string_length, average_shot_file );
	configuration_name[string_length] = '\0';
	if ( strcmp(configuration_name, prev_configuration_name) != 0 )
	{
		// Configuration name has changed, new data structure must be determined and allocated
		new_configuration = true;
		fprintf( stdout, "Configuration name has changed.\n" );
		fprintf( log_file, "Configuration name has changed.\n" );

		strcpy_s( prev_configuration_name, 1000, configuration_name );
		max_average_count = 0;

		// Deallocate memory here
		for ( int i=0; i<full_channel_count; i++ )
			for ( int j=0; j<4; j++ )
				if ( channels[i].sub_channels[j].ndata > 0 )
				{
					channels[i].sub_channels[j].ndata = 0;
					free( channels[i].sub_channels[j].data );
				}
	}

	// Continue reading the file
	fread( &channel_count, 4, 1, average_shot_file );
	if ( new_configuration )
	{
		fprintf( stdout, "%s, nchans=%d\n", configuration_name, channel_count );
		fprintf( log_file, "%s, nchans=%d\n", configuration_name, channel_count );
		full_channel_count = channel_count;
	}

	for ( int ichan=0; ichan<channel_count; ichan++ )
	{
		fread( &string_length, 4, 1, average_shot_file );
		fread( channel_name, 1, string_length, average_shot_file );
		channel_name[string_length] = '\0';

		fread( &channel_mode, 2, 1, average_shot_file );
		fread( &average_count, 2, 1, average_shot_file );
		fread( &average_shot_number, 2, 1, average_shot_file );
		fread( &vertical_scale, 4, 1, average_shot_file );
		fread( &vertical_offset, 4, 1, average_shot_file );
		fread( &enabled_code, 1, 1, average_shot_file );
		fread( &nU32, 4, 1, average_shot_file );
		raw_U32_data = (int *)malloc( 4*nU32 );
		fread( raw_U32_data, 4, nU32, average_shot_file );

		if ( average_shot_number != average_shot_number_in )
		{
			fprintf( stdout, "Error: average shot number mismatch: expected %d, read %d\n", 
				average_shot_number_in, average_shot_number );
			fprintf( log_file, "Error: average shot number mismatch: expected %d, read %d\n", 
				average_shot_number_in, average_shot_number );
			return -1;
		}

		if ( new_configuration )
		{
			if ( average_shot_number != 1 )
			{
				fprintf( stdout, "Error: new configuration found but average shot number not equal to 1\n" );
				fprintf( log_file, "Error: new configuration found but average shot number not equal to 1\n" );
				return -1;
			}
			if ( average_count > max_average_count ) max_average_count = average_count;
		}


		// SIS 3302
		//
		if ( channel_mode == 3 )
		{
			// Channel_mode 3 means that it's the 3302 -- no sub-channels

			if ( new_configuration )
			{
				// New configuration, must allocate then assign data
				fprintf( stdout, "  %d: %s\n", ichan+1, channel_name );
				fprintf( log_file, "  %d: %s\n", ichan+1, channel_name );

				fprintf( stdout, "     cm=%d ac=%d asn=%d vs=%f vo=%f ec=%d nu=%d\n",
					channel_mode, average_count, average_shot_number, vertical_scale, vertical_offset, enabled_code, nU32 );
				fprintf( log_file, "     cm=%d ac=%d asn=%d vs=%f vo=%f ec=%d nu=%d\n",
					channel_mode, average_count, average_shot_number, vertical_scale, vertical_offset, enabled_code, nU32 );

				channel_index = ichan;
				strcpy_s( channels[channel_index].channel_name, 1000, channel_name );
				channels[channel_index].channel_mode = channel_mode;
				channels[channel_index].average_count = average_count;
				channels[channel_index].average_shot_number = average_shot_number;
				channels[channel_index].vertical_scale = vertical_scale;
				channels[channel_index].vertical_offset = vertical_offset;
				channels[channel_index].enabled_code = enabled_code;
				channels[channel_index].nU32 = nU32;
				init_3302_data( channels[channel_index] );
				assign_3302_data( channels[channel_index], raw_U32_data );

				free( raw_U32_data );
			}
			if ( !new_configuration )
			{
				// Same configuration, no allocation needed; assign if first shot number in average,
				// otherwise sum 
				channel_index = find_channel( channels, full_channel_count, channel_name );
				if ( channel_index < 0 )
				{
					fprintf( stdout, "Error: channel %s not found\n", channel_name );
					fprintf( log_file, "Error: channel %s not found\n", channel_name );
					return -1;
				}

				channels[channel_index].average_shot_number = average_shot_number;

				if ( average_shot_number == 1 ) 
					assign_3302_data( channels[channel_index], raw_U32_data );
				else
					sum_3302_data( channels[channel_index], raw_U32_data );

				free( raw_U32_data );
			}  // end checking whether it's a new configuration or not
		}  // end SIS 3302


		// SIS 3305
		//
		if ( (channel_mode == 0) || (channel_mode == 1) || (channel_mode == 2) )
		{
			// channel mode 0: 3305, 4 sub-channels possible
			// channel mode 1: 3305, 2 sub-channels possible
			// channel mode 2: 3305, 1 sub-channels possible
			if ( new_configuration )
			{
				// New configuration, must allocate then assign data
				for ( int j=0; j<4; j++ )
				{
					if ( (enabled_code & (1<<j)) > 0 )  // check flag bits
					{
						fprintf( stdout, "  %d: %s%s\n", ichan+1, channel_name, sub_channel_suffixes[j] );
						fprintf( log_file, "  %d: %s%s\n", ichan+1, channel_name, sub_channel_suffixes[j] );

						fprintf( stdout, "     cm=%d ac=%d asn=%d vs=%f vo=%f ec=%d nu=%d\n",
							channel_mode, average_count, average_shot_number, vertical_scale, vertical_offset, enabled_code, nU32 );
						fprintf( log_file, "     cm=%d ac=%d asn=%d vs=%f vo=%f ec=%d nu=%d\n",
							channel_mode, average_count, average_shot_number, vertical_scale, vertical_offset, enabled_code, nU32 );
					}
				}  // end looping through sub-channels

				channel_index = ichan;
				strcpy_s( channels[channel_index].channel_name, 1000, channel_name );
				channels[channel_index].channel_mode = channel_mode;
				channels[channel_index].average_count = average_count;
				channels[channel_index].average_shot_number = average_shot_number;
				channels[channel_index].vertical_scale = vertical_scale;
				channels[channel_index].vertical_offset = vertical_offset;
				channels[channel_index].enabled_code = enabled_code;
				channels[channel_index].nU32 = nU32;
				init_3305_data( channels[channel_index] );
				assign_3305_data( channels[channel_index], raw_U32_data );

				free( raw_U32_data );
			}
			if ( !new_configuration )
			{
				// Same configuration, no allocation needed; assign if first shot number in average,
				// otherwise sum 
				channel_index = find_channel( channels, full_channel_count, channel_name );
				if ( channel_index < 0 )
				{
					fprintf( stdout, "Error: channel %s not found\n", channel_name );
					fprintf( log_file, "Error: channel %s not found\n", channel_name );
					return -1;
				}

				channels[channel_index].average_shot_number = average_shot_number;

				if ( average_shot_number == 1 ) 
					assign_3305_data( channels[channel_index], raw_U32_data );
				else
					sum_3305_data( channels[channel_index], raw_U32_data );

				free( raw_U32_data );
			}  // end checking whether it's a new configuration or not
		}  // end SIS 3305
	}  // end looping through channels

	fflush( stdout );
	fflush( log_file );

	return 0;
}

	
//========================================================================================
//----------------------------------------------------------------------------------------
// Main
//
int _tmain(int argc, _TCHAR* argv[])
{
	char *version = "September 1 2011";


	// Expecting "Average_SIS_crate.exe" plus 6 arguments
	//
	// Read in the 5 arguments
	//
	char root_folder[1024];
	char experiment_set[1024];
	char experiment[1024];
	char data_run[1024];
	int start_shot;
	int end_shot;

	strcpy_s( root_folder, 1024, argv[1] );
	strcpy_s( experiment_set, 1024, argv[2] );
	strcpy_s( experiment, 1024, argv[3] );
	strcpy_s( data_run, 1024, argv[4] );
	start_shot = atoi( argv[5] );
	end_shot = atoi( argv[6] );

	char experiment_folder[1024];
	char data_run_folder[1024];
	sprintf_s( experiment_folder, 1024, "%s/%s/%s", root_folder, experiment_set, experiment );
	sprintf_s( data_run_folder, 1024, "%s/%s.SIS crate", experiment_folder, data_run );


	// Print out the arguments (to stdout)
	//
	fprintf( stdout, "Version date: %s\n", version );
	fprintf( stdout, "Average_SIS_crate.exe beginning...\n\n" );
	fprintf( stdout, "Experiment set: %s\n", experiment_set );
	fprintf( stdout, "Experiment: %s\n", experiment );
	fprintf( stdout, "Data run: %s\n", data_run );
	fprintf( stdout, "Start shot: %d\n", start_shot );
	fprintf( stdout, "End shot: %d\n", end_shot );
	fflush( stdout );


	// Try to open logfile
	//
	char log_filename[1024];
	sprintf_s( log_filename, 1024, "%s/Log.C++.dat", data_run_folder );
	FILE *log_file;
	errno_t err = fopen_s( &log_file, log_filename, "w" );  if ( err ) return -1;
	int sleep_count = 0;
	int max_sleep_count = 100000;

	while ( (log_file == NULL) && (sleep_count < max_sleep_count) )
	{
		if ( sleep_count == 0 )
		{
			fprintf( stdout, "Could not open log file %s.\nWill keep trying...\n", log_filename );
			fflush( stdout );
		}
		Sleep( 1000 );
		sleep_count++;
		err = fopen_s( &log_file, log_filename, "w" );  if ( err ) return -1;
	}

	if ( sleep_count >= max_sleep_count )
	{
		fprintf( stdout, "Error exit.\n" );
		fflush( stdout );
		return -1;
	}

	sleep_count = 0;


	// Print out the arguments to the logfile
	//
	fprintf( log_file, "Version date: %s\n", version );
	fprintf( log_file, "Average_SIS_crate.exe beginning...\n\n" );
	fprintf( log_file, "Experiment set: %s\n", experiment_set );
	fprintf( log_file, "Experiment: %s\n", experiment );
	fprintf( log_file, "Data run: %s\n", data_run );
	fprintf( log_file, "Start shot: %d\n", start_shot );
	fprintf( log_file, "End shot: %d\n", end_shot );

	fflush( log_file );


	// Setup for the loop
	//
	channelStruct channels[MAX_CHANS];
	for ( int i=0; i<MAX_CHANS; i++ )
		for ( int j=0; j<4; j++ )
			channels[i].sub_channels[j].ndata = 0;

	int full_channel_count = 0;  // The number of channels may decrease as the average shot number increases
	int shot_number = start_shot;
	int average_shot_number = 1;
	short max_average_shots = 1;
	char configuration_name[1000] = "";

	char average_shot_filename[1024];
	char shot_filename[1024];
	char shot_finished_filename[1024];

	FILE *average_shot_file;
	FILE *shot_file;
	FILE *shot_finished_file = NULL;
	err = 0;

	bool proceed;


	// Loop through selected shot numbers, processing "shot######.avg#.dat" files.  
	// Keep track of time since the last file appeared and stop when the time is too long, 
	// say 100,000 seconds, i.e. a little more than 1 day.  Otherwise, stop when the shot
	// number is greater than end_shot.
	//
	while ( (sleep_count < max_sleep_count) && (shot_number <= end_shot) && (err == 0) )
	{
		// Look for the "shot finished" file
		//
		if ( shot_finished_file == NULL )
		{
			sprintf_s( shot_finished_filename, 1024, "%s/%s.SIS crate.shot%06d.finished",
				data_run_folder, data_run, shot_number );
			err = fopen_s( &shot_finished_file, shot_finished_filename, "rb" );
		}

		if ( (shot_finished_file == NULL) || (err != 0) )
		{
			// "Shot finished" file was not found, sleep for one second
			//
			proceed = false;
			err = 0;  // Reset error when it comes from an attempt to open the "Shot finished" file
			Sleep( 1000 );
			sleep_count++;
		}
		else
		{
			// "Shot finished" file was found.  Open the current average shot file.
			//
			sprintf_s( average_shot_filename, 1024, "%s/%s.SIS crate.shot%06d.avg%d.dat",
				data_run_folder, data_run, shot_number, average_shot_number );
			err = fopen_s( &average_shot_file, average_shot_filename, "rb" );
			if ( (average_shot_file != NULL) && (err == 0) ) proceed = true;
			else proceed = false;
		}

		if ( proceed == true )
		{
			// Current "shot finished" file was found and current average shot file was successfully
			// opened.  Read the current average shot file.
			//
			sleep_count = 0;
			fprintf( stdout, "Shot %d: avg %d: ", shot_number, average_shot_number );
			fprintf( log_file, "Shot %d: avg %d: ", shot_number, average_shot_number );

			err = read_average_shot_file(
				average_shot_file,
				log_file,
				configuration_name,
				channels,
				full_channel_count,
				average_shot_number,
				max_average_shots );

			fclose( average_shot_file );
			

			// Do averaging if max_average_shots is reached
			//
			if ( average_shot_number == max_average_shots )
			{
				// Form average
				if ( err == 0 ) form_average( channels, full_channel_count );

				// Write shot file
				if ( err == 0 )
				{
					sprintf_s( shot_filename, 1024, "%s/%s.SIS crate.shot%06d.dat",
						data_run_folder, data_run, shot_number );
					err = fopen_s( &shot_file, shot_filename, "wb" );
					if ( (shot_file != NULL) && (err == 0) )
					{
						err = write_shot_file( shot_file, log_file, configuration_name, channels, full_channel_count );
						fclose( shot_file );
					}
				}

				// Remove average shot files
				for ( int iavg_shot=1; iavg_shot<=max_average_shots; iavg_shot++ )
				{
					sprintf_s( average_shot_filename, 1024, "%s/%s.SIS crate.shot%06d.avg%d.dat",
						data_run_folder, data_run, shot_number, iavg_shot );
					if ( err == 0 ) err = remove( average_shot_filename );
				}

				// Reset shot number and average shot number
				shot_number++;
				average_shot_number = 1;

				// Remove "shot finished" file
				fclose( shot_finished_file );
				shot_finished_file = NULL;
				if ( err == 0 ) err = remove( shot_finished_filename );
			}


			// Else (max_average_shots not reached), just increment the average_shot_number
			//
			else
			{
				average_shot_number++;
			}


			// Report on the success or failure of processing this average shot
			if ( err == 0 )
			{
				fprintf( stdout, "OK\n" );
				fprintf( log_file, "OK\n" );
			}
			else
			{
				fprintf( stdout, "Error\n" );
				fprintf( log_file, "Error\n" );
			}
			fflush( stdout );
			fflush( log_file );

		} // End if "shot finished" file found and current average shot file opened successfully
	} // End main while loop


	if ( err == 0 )
	{
		fprintf( stdout, "Normal exit.\n" );
		fprintf( log_file, "Normal exit.\n" );
	}
	else
	{
		fprintf( stdout, "Error exit.\n" );
		fprintf( log_file, "Error exit.\n" );
	}
	fflush( stdout );
	fclose( log_file );

	return err;
}


