
// GDI1View.cpp : implementation of the CGDI1View class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "GDI1.h"
#endif

#include "GDI1Doc.h"
#include "GDI1View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CGDI1View

IMPLEMENT_DYNCREATE(CGDI1View, CView)

BEGIN_MESSAGE_MAP(CGDI1View, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CGDI1View::OnFilePrintPreview)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
END_MESSAGE_MAP()

// CGDI1View construction/destruction

CGDI1View::CGDI1View()
{
	// TODO: add construction code here

}

CGDI1View::~CGDI1View()
{
}

BOOL CGDI1View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CGDI1View drawing

void CGDI1View::OnDraw(CDC* pDC)
{
	CGDI1Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	pDC->MoveTo(150, 206);
	pDC->LineTo(210, 338);
}


// CGDI1View printing


void CGDI1View::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL CGDI1View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CGDI1View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CGDI1View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CGDI1View::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CGDI1View::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CGDI1View diagnostics

#ifdef _DEBUG
void CGDI1View::AssertValid() const
{
	CView::AssertValid();
}

void CGDI1View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CGDI1Doc* CGDI1View::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGDI1Doc)));
	return (CGDI1Doc*)m_pDocument;
}
#endif //_DEBUG


// CGDI1View message handlers
