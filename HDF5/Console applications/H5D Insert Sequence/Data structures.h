//========================================================================================
// Data structures.h : Various data structures to match the LabVIEW clusters.
//
//


struct sequenceLineStruct
{
	char line[120];
	int shot_number;
	short expanded_line_number;
	char line_status[20];
	double time_stamp;
};


