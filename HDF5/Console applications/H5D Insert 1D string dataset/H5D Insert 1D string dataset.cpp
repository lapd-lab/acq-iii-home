//========================================================================================
// H5D Insert 1D string dataset.cpp : Defines the entry point for the console application.
//
// Usage:
//   H5D_Insert_1D_string_dataset.exe <infile> <group> <dataset> <index> <string>
//
// <infile>: full path of the HDF5 file produced by the ACQ II system
// <group>: name of the group that will contain the 1D string dataset
// <dataset>: name of the 1D string dataset
// <index>: location within the dataset to insert the string
// <string>: string to be inserted
//
// No output file.
//
//
// In this file:
//   int _tmain(int argc, _TCHAR* argv[])


#include "stdafx.h"
#include "hdf5.h"


//========================================================================================
//----------------------------------------------------------------------------------------
// Main
//
int _tmain(int argc, _TCHAR* argv[])
{
	//FILE* debug = fopen( "C:/Temp/H5D_Insert_1D_string_dataset.txt", "w" );


	// Expecting "H5D_Insert_1D_string_dataset.exe" plus 5 arguments
	//
	if ( argc != 6 )
	{
		fprintf( stderr, "argc = %d\n", argc );
		return -1;
	}


	// Read in the arguments
	//
	char infile_name[1024];
	char group[1024];
	char dataset[1024];
	int index;
	char string[4096];

	strcpy( infile_name, argv[1] );
	strcpy( group, argv[2] );
	strcpy( dataset, argv[3] );
	index = atoi(argv[4]);
	strcpy( string, argv[5] );


	// Open the input HDF5 file.
	//
	fprintf( stdout, "About to open input file: %s\n", infile_name );
	hid_t file_id = H5Fopen( infile_name, H5F_ACC_RDWR, H5P_DEFAULT );  // hid_t H5Fopen(const char *name, unsigned flags, hid_t access_id )
	if ( file_id < 0 ) { H5Eprint( stderr ); return -1; }


	// ****************************
	// *** Look for the dataset ***
	//
	// Open the containing group and get the number of objects
	//
	fprintf( stdout, "\nLooking for dataset...\n" );
	hid_t group_id = H5Gopen( file_id, group );
	if ( group_id < 0 ) { H5Eprint( stderr ); return -1; }

	hsize_t num_obj;
	herr_t err = H5Gget_num_objs( group_id, &num_obj );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }

	// Loop through objects, looking for a match with the dataset name
	//
	char obj_name[1024];
	bool found = false;
	for ( hsize_t idx=0; idx<num_obj; idx++ )
	{
		ssize_t size = H5Gget_objname_by_idx( group_id, idx, obj_name, 1024 );
		if ( size < 0 ) { H5Eprint( stderr ); return -1; }
		if ( strcmp(obj_name, dataset) == 0 ) { found = true; break; }
	}


	// Declare the id's that we'll need to write into the dataset
	//
	hid_t dataset_id;
	hid_t file_space_id;
	hid_t mem_space_id;
	hid_t mem_type_id;


	// ************************************************
	// *** If the dataset does not exist, create it ***
	//
	if ( found == false )
	{
		fprintf( stdout, "\nDataset not found, creating it...\n" );

		// File dataspace
		//
		int rank = 1;
		hsize_t file_dims[1] = { index+1 };
		hsize_t file_maxdims[1] = { 4294967295 };
		file_space_id = H5Screate_simple( rank, file_dims, file_maxdims );
		if ( file_space_id < 0 ) { H5Eprint( stderr ); return -1; }

		hsize_t start[1] = { index };
		hsize_t stride[1] = { 1 };
		hsize_t count[1] = { 1 };
		hsize_t block[1] = { 1 };
		err = H5Sselect_hyperslab( file_space_id, H5S_SELECT_SET, start, stride, count, block );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }


		// Memory dataspace
		//
		hsize_t mem_dims[1] = { 1 };
		hsize_t mem_maxdims[1] = { 1 };
		mem_space_id = H5Screate_simple( rank, mem_dims, mem_maxdims );
		if ( mem_space_id < 0 ) { H5Eprint( stderr ); return -1; }


		// Dataset creation property list
		//
		hid_t plist_id = H5Pcreate( H5P_DATASET_CREATE );
		if ( plist_id < 0 ) { H5Eprint( stderr ); return -1; }

		hsize_t file_chunk[1] = { 1 };
		int compression = 4;
		H5Pset_chunk( plist_id, rank, file_chunk );
		H5Pset_deflate( plist_id, compression );


		// Memory datatype
		//
		mem_type_id = H5Tcopy( H5T_C_S1 );
		if ( mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }

		err = H5Tset_size( mem_type_id, 4096 );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }


		// Everything is set up so now create the 1D string dataset
		//
		dataset_id = H5Dcreate( group_id, dataset, mem_type_id, file_space_id, plist_id );
		if ( dataset_id < 0 ) { H5Eprint( stderr ); return -1; }

		H5Pclose( plist_id );
	}


	// ********************************************
	// *** If the dataset does exist, extend it ***
	//
	if ( found == true )
	{
		fprintf( stdout, "\nDataset found, extending it...\n" );
		dataset_id = H5Dopen( group_id, dataset );
		if ( dataset_id < 0 ) { H5Eprint( stderr ); return -1; }


		// File dataspace
		//
		file_space_id = H5Dget_space( dataset_id );
		if ( file_space_id < 0 ) { H5Eprint( stderr ); return -1; }

		int rank = 1;
		hsize_t file_dims[1] = { index+1 };
		hsize_t file_maxdims[1] = { 4294967295 };
		err = H5Sset_extent_simple( file_space_id, rank, file_dims, file_maxdims );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }

		hsize_t start[1] = { index };
		hsize_t stride[1] = { 1 };
		hsize_t count[1] = { 1 };
		hsize_t block[1] = { 1 };
		err = H5Sselect_hyperslab( file_space_id, H5S_SELECT_SET, start, stride, count, block );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }


		// Memory dataspace
		//
		hsize_t mem_dims[1] = { 1 };
		hsize_t mem_maxdims[1] = { 1 };
		mem_space_id = H5Screate_simple( rank, mem_dims, mem_maxdims );
		if ( mem_space_id < 0 ) { H5Eprint( stderr ); return -1; }


		// Extend the dataset
		//
		err = H5Dextend( dataset_id, file_dims );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }


		// Memory datatype
		//
		mem_type_id = H5Tcopy( H5T_C_S1 );
		if ( mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }

		err = H5Tset_size( mem_type_id, 4096 );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}


	// *****************************************
	// *** Write the string into the dataset ***
	//
	err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, string );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }

	H5Dclose( dataset_id );
	H5Tclose( mem_type_id );

	H5Sclose( mem_space_id );
	H5Sclose( file_space_id );

	H5Gclose( group_id );
	H5Fclose( file_id );

	return 0;
}


