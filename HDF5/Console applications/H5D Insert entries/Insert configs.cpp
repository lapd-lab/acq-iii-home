//========================================================================================
// Insert configs.cpp
//

#include "stdafx.h"


//----------------------------------------------------------------------------------------
// Get pointer size
//
size_t getCharPtrSize( char *ptr )
{
   return sizeof( ptr );
}


//----------------------------------------------------------------------------------------
// Check descriptor file
//
// Read the descriptor file and make sure it describes a cluster of the
// following form:
//
// Cluster descriptor:
//   1) string descriptor
//   2) Variant descriptor
//
// In detail:
// 00-03: number of shorts (16 bit words) to follow --> "00 0D"
// 04-05: number of bytes in current section (including these two) --> "00 1A"
// 06-07: LabVIEW type code for a cluster --> "00 50"
// 08-09: number of elements --> "00 02"
// 0A-0B: number of bytes for current element (including these two) --> "00 08"
// 0C-0D: LabVIEW type code for a string --> "00 30"
// 0E-11: length of string (unlimited) --> "FF FF FF FF"
// 12-13: number of bytes for current element (including these two) --> "00 0C"
// 14-15: LabVIEW type code for a Variant --> "40 53"
//    16: number of characters to follow --> "07"
// 17-1D: the string "Variant" --> "56 61 72 69 61 6E 74"
//
// Is there any point in checking this in detail?  It's a nice code example which shows
// how to do it and it might change in the future, so I'm going to keep it.
//
herr_t check_descriptor_file(
	FILE* descriptor_file,
	hid_t file_id,
	char* efn,
	int* error_index,
	int shot_number )
{
	herr_t err = 0;

	int nshorts;
	char LV_type[2];
	short nbytes;
	int nelements;
	char error_string[1024];

	// 00-03: number of shorts (16 bit words) to follow --> "00 0D"
	if ( err == 0)
		nshorts = read_int( descriptor_file );

	// 04-05: number of bytes in current section (including these two) --> "00 1A"
	// 06-07: LabVIEW type code for a cluster --> "00 50"
	if ( err == 0)
	{
		nbytes = read_short( descriptor_file );

		read_short_bytes( descriptor_file, LV_type );
		if ( LV_type[0] != 0x50 )
		{
			sprintf_s( error_string, 1024, "Config file must contain a cluster." );
			write_error( efn, shot_number, error_string );
			insert_error( file_id, error_index, shot_number, error_string );
			err = -1;
		}
	}

	// 08-09: number of elements --> "00 02"
	if ( err == 0 )
	{
		nelements = read_short( descriptor_file );
		if ( nelements != 2 )
		{
			sprintf_s( error_string, 1024, "Config file cluster must contain exactly 2 elements." );
			write_error( efn, shot_number, error_string );
			insert_error( file_id, error_index, shot_number, error_string );
			err = -1;
		}
	}

	// 0A-0B: number of bytes for current element (including these two) --> "00 08"
	// 0C-0D: LabVIEW type code for a string --> "00 30"
	// 0E-11: length of string (unlimited) --> "FF FF FF FF"
	if ( err == 0 )
	{
		nbytes = read_short( descriptor_file );
		read_short_bytes( descriptor_file, LV_type );
		fseek( descriptor_file, nbytes-4, SEEK_CUR );  // skip to next element descriptor

		if ( (LV_type[0] != 0x30) )
		{
			sprintf_s( error_string, 1024, "First element of config file cluster must be a string." );
			write_error( efn, shot_number, error_string );
			insert_error( file_id, error_index, shot_number, error_string );
			err = -1;
		}
	}

	// 12-13: number of bytes for current element (including these two) --> "00 0C"
	// 14-15: LabVIEW type code for a Variant --> "40 53"
	//    16: number of characters to follow --> "07"
	// 17-1D: the string "Variant" --> "56 61 72 69 61 6E 74"
	if ( err == 0 )
	{
		nbytes = read_short( descriptor_file );
		read_short_bytes( descriptor_file, LV_type );

		if ( (LV_type[0] != 0x53) )
		{
			sprintf_s( error_string, 1024, "Second element of config file cluster must be a Variant." );
			write_error( efn, shot_number, error_string );
			insert_error( file_id, error_index, shot_number, error_string );
			err = -1;
		}
	}

	return err;
}


//----------------------------------------------------------------------------------------
// Process 08 20 80 20 section
//
// The structure of the config cluster, or a Variant contained within, is described in a
// section which starts with the 4 bytes: "08 20 80 20".  See process_config_file() below
// for a more complete description.
//
// Note that, in the case of an array of Variants, this function will get called multiple
// times with the same reference_list.  The assumption is that the Variant structure is the
// same for each array element.  If it is not, this approach will fail.
//

herr_t process_08_20_80_02_section(
	FILE* config_file,
	dataElement** ptr_to_reference_list,
	short* descriptor_count,
	short* main_cluster_index )
{
	char LV_type[2];
	short nbytes;

	short descriptor_section;
	short bytes_read;
	hid_t group_id = 0;

	herr_t err = 0;

	dataElement* reference_list = (*ptr_to_reference_list);
	bool allocate_memory = true;


	// 23-26: ???, descriptor header? --> "08 20 80 02"
	// 27-28: section 0, descriptor section --> "00 00"
	fseek( config_file, 4, SEEK_CUR );  // skip over "08 20 80 02"
	descriptor_section = read_short( config_file );
	if ( descriptor_section != 0 )
	{
		err = -11;
		return err;
	}

	// 29-2A: number of descriptors to follow (18) --> "00 12"
	(*descriptor_count) = read_short( config_file );
	if ( (*descriptor_count) <= 0 )
	{
		err = -12;
		return err;
	}
	if ( reference_list == NULL )
	{
		reference_list = (dataElement* )malloc( (*descriptor_count) * sizeof dataElement );
		//fprintf( stdout, "reference_list = malloc( %d )\n", (*descriptor_count) * sizeof dataElement );
	}
	else allocate_memory = false;

	// 2B-2C: number of bytes for current descriptor (including these two) --> "00 16"
	// 2D-2E: LabVIEW type code (string) --> "40 30"
	// 2F-32: string length (unlimited) --> "FF FF FF FF"
	//    33: length of element name (13) --> "0D"
	// 34-40: element name --> "Configuration"
	//
	// ... The remaining descriptors follow.  Note that the descriptor format varies
	//     according to the element type.  See LabVIEW Help >> Fundamentals >>
	//     How LabVIEW Stores Data in Memory >> Concepts >> Type Descriptors.  Clusters
	//     are defined by referring to sub-elements defined previously.  The full cluster
	//     comes last.
	for ( short i=0; i<(*descriptor_count); i++ )
	{
		nbytes = read_short( config_file );
		read_short_bytes( config_file, LV_type );
		bytes_read = 4;

		// regular numeric types
		if ( ((LV_type[0] >= 0x01) && (LV_type[0] <= 0x0E)) || (LV_type[0] == 0x21) )
		{
			reference_list[i].LV_type = LV_type[0];
			reference_list[i].dims = 0;
			reference_list[i].enum_count = 0;
			reference_list[i].sub_element_count = 0;
			reference_list[i].sub_reference_list = NULL;
			reference_list[i].sub_descriptor_count = 0;
		}
		
		// enumerated types
		else if ( (LV_type[0] >= 0x15) && (LV_type[0] <= 0x17) )
		{
			reference_list[i].LV_type = LV_type[0];
			reference_list[i].dims = 0;
			reference_list[i].sub_element_count = 0;
			reference_list[i].enum_count = read_short( config_file );  bytes_read = bytes_read + 2;
			char* char_ptr = (char*) malloc( 1 );  // monkey business to get the size of a pointer
			int char_ptr_size = getCharPtrSize( char_ptr );
			free( char_ptr );
			if ( allocate_memory )
			{
				reference_list[i].enum_names =
					(char**) malloc( reference_list[i].enum_count * char_ptr_size );
				//fprintf( stdout, "reference_list[%d].enum_names = malloc( %d )\n", i, reference_list[i].enum_count * char_ptr_size );
			}
			for ( short j=0; j<reference_list[i].enum_count; j++ )
			{
				short str_length = (short) read_char( config_file );  bytes_read = bytes_read + 1;
				if ( allocate_memory )
				{
					reference_list[i].enum_names[j] = (char*) malloc( str_length+1 );
					//fprintf( stdout, "reference_list[%d].enum_names[%d] = malloc( %d )\n", i, j, str_length+1 );
				}
				if ( str_length > 0 )
				{
					read_char_array( config_file, reference_list[i].enum_names[j], str_length );
					bytes_read = bytes_read + str_length;
				}
				reference_list[i].enum_names[j][str_length] = '\0';
			}
			reference_list[i].sub_reference_list = NULL;
			reference_list[i].sub_descriptor_count = 0;
		}
		
		// string
		else if ( (LV_type[0] == 0x30) )
		{
			reference_list[i].LV_type = LV_type[0];
			reference_list[i].dims = 0;
			reference_list[i].enum_count = 0;
			reference_list[i].sub_element_count = 0;
			reference_list[i].sub_reference_list = NULL;
			reference_list[i].sub_descriptor_count = 0;
			fseek( config_file, 4, SEEK_CUR );  bytes_read = bytes_read + 4;
		}

		// array
		else if ( (LV_type[0] == 0x40) )
		{
			reference_list[i].LV_type = LV_type[0];
			reference_list[i].dims = read_short( config_file );  bytes_read = bytes_read + 2;
			reference_list[i].enum_count = 0;
			reference_list[i].sub_element_count = 0;
			if ( reference_list[i].dims == 1 )
			{
				fseek( config_file, 4, SEEK_CUR );  bytes_read = bytes_read + 4;
				reference_list[i].sub_element_count = 1;
				if ( allocate_memory )
				{
					reference_list[i].sub_elements = (short* )malloc( 1 * sizeof(short) );
					//fprintf( stdout, "reference_list[%d].sub_elements = malloc( %d )\n", i, 1 * sizeof(short) );
				}
				reference_list[i].sub_elements[0] = read_short( config_file );  bytes_read = bytes_read + 2;
			}
			else
			{
				err = -13;
				return err;
			}
			reference_list[i].sub_reference_list = NULL;
			reference_list[i].sub_descriptor_count = 0;
		}

		// cluster
		else if ( (LV_type[0] == 0x50) )
		{
			reference_list[i].LV_type = LV_type[0];
			reference_list[i].dims = 1;
			reference_list[i].enum_count = 0;
			reference_list[i].sub_element_count = read_short( config_file );  bytes_read = bytes_read + 2;
			if ( allocate_memory )
			{
				reference_list[i].sub_elements = 
					(short* ) malloc( reference_list[i].sub_element_count * sizeof(short) );
				//fprintf( stdout, "reference_list[%d].sub_elements = malloc( %d )\n", i, reference_list[i].sub_element_count * sizeof(short) );
			}
			for ( short j=0; j<reference_list[i].sub_element_count; j++ )
			{
				reference_list[i].sub_elements[j] = read_short( config_file );
				bytes_read = bytes_read + 2;
			}
			reference_list[i].sub_reference_list = NULL;
			reference_list[i].sub_descriptor_count = 0;
		}

		// variant
		else if ( (LV_type[0] == 0x53) )
		{
			reference_list[i].LV_type = LV_type[0];
			reference_list[i].dims = 0;
			reference_list[i].enum_count = 0;
			reference_list[i].sub_element_count = 0;
			reference_list[i].sub_reference_list = NULL;
			reference_list[i].sub_descriptor_count = 0;
			// sub_reference_list will be created later in call to H5A_create_write_compound()
		}

		else
		{
			err = -14;  // Unrecognized sub-element (could be file structure does not match expectations)
			return err;
		}

		// read the name
		short leftover_bytes = nbytes - bytes_read;
		if ( leftover_bytes >= 2 )
		{
			// deal with byte alignment
			short remainder = leftover_bytes - (leftover_bytes/2) * 2;
			if ( remainder == 1 )
				{ fseek( config_file, 1, SEEK_CUR );  bytes_read = bytes_read + 1; }

			short str_length = (short) read_char( config_file );  bytes_read = bytes_read + 1;
			if ( allocate_memory )
			{
				reference_list[i].name = (char* ) malloc( str_length+1 );
				//fprintf( stdout, "reference_list[%d].name = malloc( %d )\n", i, str_length+1 );
			}
			if ( str_length > 0 )
			{
				read_char_array( config_file, reference_list[i].name, str_length );
				bytes_read = bytes_read + str_length;
			}
			reference_list[i].name[str_length] = '\0';
			leftover_bytes = nbytes - bytes_read;
		}
		else
		{
			// No name specified, just put in a blank for now
			if ( allocate_memory )
			{
				reference_list[i].name = (char* ) malloc( 1 );
				//fprintf( stdout, "reference_list[%d].name = malloc( %d )\n", i, 1 );
			}
			reference_list[i].name[0] = '\0';
		}
		if ( leftover_bytes > 0 ) fseek( config_file, leftover_bytes, SEEK_CUR );
	} // End looping through descriptors


	// Reference_list is now built, that is the cluster structure has been determined.  Read the
	// last few bytes.
	//
	// 287-288: section 1, data section --> "00 01"
	descriptor_section = read_short( config_file );
	if ( descriptor_section != 1 )
	{
		err = -15;
		return err;
	}

	// 289-28A: ???, probably the index of the main cluster (17) --> "00 11"
	(*main_cluster_index) = read_short( config_file );
	if ( (*main_cluster_index) != (*descriptor_count) - 1 )
	{
		err = -16;
		return err;
	}


	(*ptr_to_reference_list) = reference_list;
	return err;
}


//----------------------------------------------------------------------------------------
// Process config file
//
// The config file should be of the following form:
//
// Cluster data:
//   1) string data == name of configuration group
//   2) Variant data == the configuration information
//
// SIS 3301 example:
// 00-03: string length (31) --> "00 00 00 1F"
// 04-22: string --> "Configuration: SIS b0 c0-1 2048"
// 23-26: ???, last byte may be the number of sections to follow (2) --> "08 20 80 02"
// 27-28: section 0, descriptor section --> "00 00"
// 29-2A: number of descriptors to follow (18) --> "00 12"
// 2B-2C: number of bytes for current descriptor (including these two) --> "00 16"
// 2D-2E: LabVIEW type code (string) --> "40 30"
// 2F-32: string length (unlimited) --> "FF FF FF FF"
//    33: length of element name (13) --> "0D"
// 34-40: element name --> "Configuration"
//
// ... The remaining descriptors follow.  Note that the descriptor format varies
//     according to the element type.  See LabVIEW Help >> Fundamentals >>
//     How LabVIEW Stores Data in Memory >> Concepts >> Type Descriptors.  Clusters
//     are defined by referring to sub-elements defined previously.  The full cluster
//     comes last.
//
// 287-288: section 1, data section --> "00 01"
// 289-28A: ???, probably the index of the main cluster (17) --> "00 11"
// 28B-28E: string length (16) --> "00 00 00 10"
// 28F-29E: string --> "SIS b0 c0-1 2048"
//
// ... The remaining data values follow.  Array data is prepended with the array length.
//
// 2EA-2ED: ??? --> "00 00 00 00"
//
// Two pass process.  First pass to make a hierarchical structure.  Second pass to traverse this
// structure and translate it into HDF5 groups and attributes
//

herr_t process_config_file(
	FILE* config_file,
	char* device_name,
	hid_t file_id,
	char* efn,
	int* error_index,
	int shot_number )
{
	char config_name[120];
	struct dataElement main_cluster;
	struct dataElement* reference_list = NULL;
	short descriptor_count = 0;
	short main_cluster_index;
	hid_t group_id = 0;

	herr_t err = 0;
	char error_string[1024];


	// 00-03: string length (31) --> "00 00 00 1F"
	// 04-22: string --> "Configuration: SIS b0 c0-1 2048"
	if ( err == 0 ) read_string( config_file, config_name, 120 );

	// 23-28A: the full 08 20 80 02 section
	if ( err == 0 )
	{
		err = process_08_20_80_02_section(
			config_file,
			&reference_list,
			&descriptor_count,
			&main_cluster_index );

		if ( err < 0 )
		{
			sprintf_s( error_string, 1024, "Problem in process_08_20_80_02_section().  Error = %d.\n", err );
			write_error( efn, shot_number, error_string );
			insert_error( file_id, error_index, shot_number, error_string );
		}
	}

	// What about 28B-28E: string length (16) --> "00 00 00 10"
	// and 28F-29E: string --> "SIS b0 c0-1 2048"?

	// Create config group
	char group_name[4096];
	if ( err == 0 )
	{
		sprintf_s( group_name, 4096, "/Raw data + config/%s/%s", device_name, config_name );
		group_id = H5Gcreate2( file_id, group_name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
		if ( group_id < 0 )
		{
			sprintf_s( error_string, 1024, "Could not create config group: %s.", group_name );
			write_error( efn, shot_number, error_string );
			insert_error( file_id, error_index, shot_number, error_string );
			err = -1;
		}
	}

	// Add attributes and sub-groups
	if ( err == 0 )
	{
		main_cluster = reference_list[main_cluster_index];
		err = H5A_create_write_compound( group_id, main_cluster, reference_list, config_file );
		if ( err < 0 )
		{
			sprintf_s( error_string, 1024, "Problem in H5A_create_write_compound().  Error = %d.\n", err );
			write_error( efn, shot_number, error_string );
			insert_error( file_id, error_index, shot_number, error_string );
		}
	}

	// Cleanup
	H5Gclose( group_id );
	if ( reference_list != NULL )
	{
		for ( int i=0; i<descriptor_count; i++ )
		{
			if ( reference_list[i].enum_count > 0 )
			{
				for ( int j=0; j<reference_list[i].enum_count; j++ )
				{
					free( reference_list[i].enum_names[j] );
					//fprintf( stdout, "free( reference_list[%d].enum_names[%d] )\n", i, j );
				}
				free( reference_list[i].enum_names );
				//fprintf( stdout, "free( reference_list[%d].enum_names )\n", i );
			}
			if ( reference_list[i].sub_element_count > 0 )
			{
				free( reference_list[i].sub_elements );
				//fprintf( stdout, "free( reference_list[%d].sub_elements )\n", i );
			}
			if ( reference_list[i].sub_reference_list != NULL )
			{
				// Have to step through sub-reference list...
				for ( int j=0; j<reference_list[i].sub_descriptor_count; j++ )
				{
					if ( reference_list[i].sub_reference_list[j].enum_count > 0 )
					{
						for ( int k=0; k<reference_list[i].sub_reference_list[j].enum_count; k++ )
						{
							free( reference_list[i].sub_reference_list[j].enum_names[k] );
							//fprintf( stdout, "free( reference_list[%d].sub_reference_list[%d],enum_names[%d] )\n", i, j, k );
						}
						free( reference_list[i].sub_reference_list[j].enum_names );
						//fprintf( stdout, "free( reference_list[%d].sub_reference_list[%d].enum_names )\n", i, j );
					}
					if ( reference_list[i].sub_reference_list[j].sub_element_count > 0 )
					{
						free( reference_list[i].sub_reference_list[j].sub_elements );
						//fprintf( stdout, "free( reference_list[%d].sub_reference_list[%d].sub_elements )\n", i, j );
					}
					// Deeper nesting is not allowed
				}
				free( reference_list[i].sub_reference_list );
				//fprintf( stdout, "free( reference_list[%d].sub_reference_list )\n", i );
			}
		}
		free( reference_list );
		//fprintf( stdout, "free( reference_list )\n" );
	}

	return err;
}


//----------------------------------------------------------------------------------------
// H5A create write compound
//
// This is a recursive function that adds attributes and sub-groups mimicking the LabVIEW
// data element contained in config_file, and described in descriptor_file.  The top-level
// call should be with the config group id and the main config cluster.
//
herr_t H5A_create_write_compound(
	hid_t group_id,
	dataElement element,
	dataElement* reference_list,
	FILE* config_file )
{
	herr_t err = 0;
	dataElement sub_element;


	// Must be a cluster
	//
	if ( element.LV_type == 0x50 )
	{
		// loop through cluster elements
		for ( int i=0; i<element.sub_element_count; i++ )
		{
			short reference_index = element.sub_elements[i];
			sub_element = reference_list[reference_index];


			// array
			//
			if ( (sub_element.LV_type == 0x40) && (err == 0) )
			{
				short array_type_ref_index = sub_element.sub_elements[0];
				dataElement array_type_sub_element = reference_list[array_type_ref_index];


				// array type is "cluster"
				// this is complicated, must make a sub-group for each array element
				//
				if ( (array_type_sub_element.LV_type == 0x50) && (err == 0) )
				{
					int count = read_int( config_file );
					for ( int j=0; j<count; j++ )
					{
						// create sub-group
						char sub_group_name[4096];
						sprintf_s( sub_group_name, 4096, "%s[%d]", sub_element.name, j );
						hid_t sub_group_id = H5Gcreate2( group_id, sub_group_name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
						if ( sub_group_id < 0 ) err = -1;

						// call recursively
						if ( err == 0)
							err = H5A_create_write_compound(
								sub_group_id, array_type_sub_element, reference_list, config_file );

						H5Gclose( sub_group_id );
					} // end looping through array elements
				} // end if array type is "cluster"


				// array type is "Variant"
				// this is even more complicated: read it in, then make a sub-group for each array element
				// even if they are not clusters
				//
				else if ( (array_type_sub_element.LV_type == 0x53) && (err == 0) )
				{
					int count = read_int( config_file );
					for ( int j=0; j<count; j++ )
					{
						short variant_reference_index;
						short variant_descriptor_count;
						struct dataElement variant;
						err = process_08_20_80_02_section(
							config_file,
							&(array_type_sub_element.sub_reference_list),
							&variant_descriptor_count,
							&variant_reference_index );
		
						array_type_sub_element.sub_descriptor_count = variant_descriptor_count;
						variant = array_type_sub_element.sub_reference_list[variant_reference_index];

						// create sub-group
						hid_t sub_group_id = -1;
						if ( err == 0 )
						{
							char sub_group_name[4096];
							sprintf_s( sub_group_name, 4096, "%s[%d]", sub_element.name, j );
							sub_group_id = H5Gcreate2( group_id, sub_group_name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
							if ( sub_group_id < 0 ) err = -1;
						}

						// call the appropriate H5A_create_write function (may be recursive)
						if ( err == 0)
						{
							if ( variant.LV_type == 0x50 )  // cluster
								err = H5A_create_write_compound(
									sub_group_id, variant, array_type_sub_element.sub_reference_list, config_file );
							else  // anything other than a cluster
							{
								err = H5A_create_write(
									sub_group_id, variant, array_type_sub_element.sub_reference_list, config_file );
							}
						}

						if ( sub_group_id >= 0 ) H5Gclose( sub_group_id );

						//skip over 4 extra bytes
						if ( err == 0 ) fseek( config_file, 4, SEEK_CUR );

					} // end looping through array elements
				} // end if array type is "cluster"


				// array type is simple
				//
				else
				{
					H5A_create_write( group_id, sub_element, reference_list, config_file );
				}
			}


			// cluster
			//
			else if ( (sub_element.LV_type == 0x50) && (err == 0) )
			{
				// create sub-group
				hid_t sub_group_id = H5Gcreate2( group_id, sub_element.name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
				if ( sub_group_id < 0 ) err = -1;

				// call recursively
				if ( err == 0)
					err = H5A_create_write_compound( sub_group_id, sub_element, reference_list, config_file );

				H5Gclose( sub_group_id );
			}


			// Variant
			//
			else if ( (sub_element.LV_type == 0x53) && (err == 0) )
			{
				short variant_reference_index;
				short variant_descriptor_count;
				struct dataElement variant;
				err = process_08_20_80_02_section(
					config_file,
					&(sub_element.sub_reference_list),
					&variant_descriptor_count,
					&variant_reference_index );

				sub_element.sub_descriptor_count = variant_descriptor_count;
				variant = sub_element.sub_reference_list[variant_reference_index];

				// create sub-group
				hid_t sub_group_id = -1;
				if ( err == 0 )
				{
					sub_group_id = H5Gcreate2( group_id, sub_element.name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
					if ( sub_group_id < 0 ) err = -1;
				}

				// call the appropriate H5A_create_write function (may be recursive)
				if ( err == 0)
				{
					if ( variant.LV_type == 0x50 )  // cluster
						err = H5A_create_write_compound(
							sub_group_id, variant, sub_element.sub_reference_list, config_file );
					else  // anything other than a cluster
					{
						err = H5A_create_write(
							sub_group_id, variant, sub_element.sub_reference_list, config_file );
					}
				}
 
				if ( sub_group_id >= 0 ) H5Gclose( sub_group_id );

				//skip over 4 extra bytes
				if ( err == 0 ) fseek( config_file, 4, SEEK_CUR );
			}


			// regular element
			//
			else if ( err == 0 )
			{
				err = H5A_create_write( group_id, sub_element, reference_list, config_file );
			}
		} // end looping through cluster elements

	} // end if element is a cluster


	// Anything other than a cluster is an error
	//
	else
	{
		err = -1;
	}


	return err;
}

					
//----------------------------------------------------------------------------------------
// H5A create write
//
// Adds one attribute to the group referenced by group_id.
//
herr_t H5A_create_write(
	hid_t group_id,
	dataElement element,
	dataElement* reference_list,
	FILE* config_file )
{
	herr_t err = 0;
	hid_t type_id = -1;
	hid_t dataspace_id = -1;
	hid_t attribute_id = -1;
	char* buffer = NULL;
	dataElement sub_element;

	// Set the type id
	//
	if ( element.LV_type == 0x01 )
	{
		type_id = H5Tcopy( H5T_NATIVE_CHAR );
		dataspace_id = H5Screate( H5S_SCALAR );
		buffer = (char*) malloc( 1 );
		//fprintf( stdout, "buffer = malloc( 1 )\n" );
		read_bool_byte( config_file, buffer );
	}

	else if ( element.LV_type == 0x02 )
	{
		type_id = H5Tcopy( H5T_NATIVE_SHORT );
		dataspace_id = H5Screate( H5S_SCALAR );
		buffer = (char*) malloc( 2 );
		//fprintf( stdout, "buffer = malloc( 2 )\n" );
		read_short_bytes( config_file, buffer );
	}

	else if ( element.LV_type == 0x03 )
	{
		type_id = H5Tcopy( H5T_NATIVE_INT );
		dataspace_id = H5Screate( H5S_SCALAR );
		buffer = (char*) malloc( 4 );
		//fprintf( stdout, "buffer = malloc( 4 )\n" );
		read_int_bytes( config_file, buffer );
	}

	else if ( element.LV_type == 0x05 )
	{
		type_id = H5Tcopy( H5T_NATIVE_UCHAR );
		dataspace_id = H5Screate( H5S_SCALAR );
		buffer = (char*) malloc( 1 );
		//fprintf( stdout, "buffer = malloc( 1 )\n" );
		read_bool_byte( config_file, buffer );
	}

	else if ( element.LV_type == 0x06 )
	{
		type_id = H5Tcopy( H5T_NATIVE_USHORT );
		dataspace_id = H5Screate( H5S_SCALAR );
		buffer = (char*) malloc( 2 );
		//fprintf( stdout, "buffer = malloc( 2 )\n" );
		read_short_bytes( config_file, buffer );
	}

	else if ( element.LV_type == 0x07 )
	{
		type_id = H5Tcopy( H5T_NATIVE_UINT );
		dataspace_id = H5Screate( H5S_SCALAR );
		buffer = (char*) malloc( 4 );
		//fprintf( stdout, "buffer = malloc( 4 )\n" );
		read_int_bytes( config_file, buffer );
	}

	else if ( element.LV_type == 0x09 )
	{
		type_id = H5Tcopy( H5T_NATIVE_FLOAT );
		dataspace_id = H5Screate( H5S_SCALAR );
		buffer = (char*) malloc( 4 );
		//fprintf( stdout, "buffer = malloc( 4 )\n" );
		read_float_bytes( config_file, buffer );
	}

	else if ( element.LV_type == 0x0A )
	{
		type_id = H5Tcopy( H5T_NATIVE_DOUBLE );
		dataspace_id = H5Screate( H5S_SCALAR );
		buffer = (char*) malloc( 8 );
		//fprintf( stdout, "buffer = malloc( 8 )\n" );
		read_double_bytes( config_file, buffer );
	}

	else if ( element.LV_type == 0x15 )
	{
		type_id = H5Tcopy( H5T_C_S1 );  // enum U8
		dataspace_id = H5Screate( H5S_SCALAR );
		short value = (short) read_char( config_file );
		int str_length = strlen( element.enum_names[value] );
		H5Tset_size( type_id, str_length );
		buffer = (char*) malloc( str_length );
		//fprintf( stdout, "buffer = malloc( %d )\n", str_length );
		strcpy_s( buffer, str_length, element.enum_names[value] );
	}

	else if ( element.LV_type == 0x16 )
	{
		// JB: 2013-02-04
		// Made some changes in here in an attempt to solve problem (2) in the 2013-01 fixes list.
		// In fact, the problem was that the element name was blank for some reason.  See below for
		// actual fix
		//
		type_id = H5Tcopy( H5T_C_S1 );  // enum U16
		dataspace_id = H5Screate( H5S_SCALAR );
		short value = read_short( config_file );
		int str_length = strlen( element.enum_names[value] );
		H5Tset_size( type_id, str_length );
		buffer = (char*) malloc( str_length );
		for ( int i=0; i<str_length; i++ )
		{
			buffer[i] = element.enum_names[value][i];
		}

//		fprintf( stderr, "type_id is H5T_NATIVE_USHORT (enum U16).\n" );
//		type_id = H5Tcopy( H5T_NATIVE_USHORT );  // enum U16
//		dataspace_id = H5Screate( H5S_SCALAR );
//		buffer = (char*) malloc( 2 );
//		//fprintf( stdout, "buffer = malloc( 2 )\n" );
//		read_short_bytes( config_file, buffer );
	}

	else if ( element.LV_type == 0x17 )
	{
		type_id = H5Tcopy( H5T_C_S1 );  // enum U32
		dataspace_id = H5Screate( H5S_SCALAR );
		int value = read_int( config_file );
		int str_length = strlen( element.enum_names[value] );
		H5Tset_size( type_id, str_length );
		buffer = (char*) malloc( str_length+1 );
		//fprintf( stdout, "buffer = malloc( %d )\n", str_length+1 );
		strcpy_s( buffer, str_length+1, element.enum_names[value] );
	}

	else if ( element.LV_type == 0x21 )
	{
		type_id = H5Tcopy( H5T_C_S1 );  // boolean
		dataspace_id = H5Screate( H5S_SCALAR );
		bool value = read_bool( config_file );
		int str_length = 6;
		H5Tset_size( type_id, str_length );
		buffer = (char*) malloc( str_length );
		//fprintf( stdout, "buffer = malloc( %d )\n", str_length );
		if ( value == true ) strcpy_s( buffer, str_length, "TRUE" );
		else strcpy_s( buffer, str_length, "FALSE" );
	}

	else if ( element.LV_type == 0x30 )
	{
		type_id = H5Tcopy( H5T_C_S1 );  // string
		dataspace_id = H5Screate( H5S_SCALAR );
		int str_length = read_int( config_file );
		if ( str_length > 0 )
		{
			buffer = (char*) malloc( str_length );
			//fprintf( stdout, "buffer = malloc( %d )\n", str_length );
			read_char_array( config_file, buffer, str_length );
			H5Tset_size( type_id, str_length );
		}
		else
		{
			buffer = (char*) malloc( 1 );
			//fprintf( stdout, "buffer = malloc( %d )\n", 1 );
			buffer[0] = ' ';
			H5Tset_size( type_id, 1 );
		}
	}

	else if ( element.LV_type == 0x40 )
	{
		// array, must be 1-dimensional
		short index = element.sub_elements[0];
		sub_element = reference_list[index];
		int sub_element_count = read_int( config_file );
		int rank = element.dims;
		hsize_t dims[1], max_dims[1];
		dims[0] = sub_element_count;
		max_dims[0] = sub_element_count;
		dataspace_id = H5Screate_simple( rank, dims, max_dims );  // this is redone for string arrays

		if ( sub_element.LV_type == 0x01 )
		{
			type_id = H5Tcopy( H5T_NATIVE_CHAR );
			buffer = (char*) malloc( sub_element_count*1 );
			//fprintf( stdout, "buffer = malloc( %d )\n", sub_element_count*1 );
			read_char_array( config_file, buffer, sub_element_count );
		}

		else if ( sub_element.LV_type == 0x02 )
		{
			type_id = H5Tcopy( H5T_NATIVE_SHORT );
			buffer = (char*) malloc( sub_element_count*2 );
			//fprintf( stdout, "buffer = malloc( %d )\n", sub_element_count*2 );
			read_short_array( config_file, (short*) buffer, sub_element_count );
		}

		else if ( sub_element.LV_type == 0x03 )
		{
			type_id = H5Tcopy( H5T_NATIVE_INT );
			buffer = (char*) malloc( sub_element_count*4 );
			//fprintf( stdout, "buffer = malloc( %d )\n", sub_element_count*4 );
			read_int_array( config_file, (int*) buffer, sub_element_count );
		}

		else if ( sub_element.LV_type == 0x05 )
		{
			type_id = H5Tcopy( H5T_NATIVE_UCHAR );
			buffer = (char*) malloc( sub_element_count*1 );
			//fprintf( stdout, "buffer = malloc( %d )\n", sub_element_count*1 );
			read_char_array( config_file, buffer, sub_element_count );
		}

		else if ( sub_element.LV_type == 0x06 )
		{
			type_id = H5Tcopy( H5T_NATIVE_USHORT );
			buffer = (char*) malloc( sub_element_count*2 );
			//fprintf( stdout, "buffer = malloc( %d )\n", sub_element_count*2 );
			read_short_array( config_file, (short*) buffer, sub_element_count );
		}

		else if ( sub_element.LV_type == 0x07 )
		{
			type_id = H5Tcopy( H5T_NATIVE_UINT );
			buffer = (char*) malloc( sub_element_count*4 );
			//fprintf( stdout, "buffer = malloc( %d )\n", sub_element_count*4 );
			read_int_array( config_file, (int*) buffer, sub_element_count );
		}

		else if ( sub_element.LV_type == 0x09 )
		{
			type_id = H5Tcopy( H5T_NATIVE_FLOAT );
			buffer = (char*) malloc( sub_element_count*4 );
			//fprintf( stdout, "buffer = malloc( %d )\n", sub_element_count*4 );
			read_float_array( config_file, (float*) buffer, sub_element_count );
		}

		else if ( sub_element.LV_type == 0x0A )
		{
			type_id = H5Tcopy( H5T_NATIVE_DOUBLE );
			buffer = (char*) malloc( sub_element_count*8 );
			//fprintf( stdout, "buffer = malloc( %d )\n", sub_element_count*8 );
			read_double_array( config_file, (double*) buffer, sub_element_count );
		}

		else if ( sub_element.LV_type == 0x15 )
		{
			type_id = H5Tcopy( H5T_NATIVE_UCHAR );  // enum U8
			buffer = (char*) malloc( sub_element_count*1 );
			//fprintf( stdout, "buffer = malloc( %d )\n", sub_element_count*1 );
			read_char_array( config_file, buffer, sub_element_count );
		}

		else if ( sub_element.LV_type == 0x16 )
		{
			type_id = H5Tcopy( H5T_NATIVE_USHORT );  // enum U16
			buffer = (char*) malloc( sub_element_count*2 );
			//fprintf( stdout, "buffer = malloc( %d )\n", sub_element_count*2 );
			read_short_array( config_file, (short*) buffer, sub_element_count );
		}

		else if ( sub_element.LV_type == 0x17 )
		{
			type_id = H5Tcopy( H5T_NATIVE_UINT );  // enum U32
			buffer = (char*) malloc( sub_element_count*4 );
			//fprintf( stdout, "buffer = malloc( %d )\n", sub_element_count*4 );
			read_int_array( config_file, (int*) buffer, sub_element_count );
		}

		else if ( sub_element.LV_type == 0x21 )
		{
			type_id = H5Tcopy( H5T_NATIVE_UCHAR );  // boolean
			buffer = (char*) malloc( sub_element_count*1 );
			//fprintf( stdout, "buffer = malloc( %d )\n", sub_element_count*1 );
			read_char_array( config_file, buffer, sub_element_count );
		}

		else if ( sub_element.LV_type == 0x30 )
		{
			type_id = H5Tcopy( H5T_C_S1 );  // string

			// Concatenate array of strings into single string
			H5Dclose( dataspace_id );
			dataspace_id = H5Screate( H5S_SCALAR );
			int max_length = 10240;
			int str_length;
			buffer = (char*) malloc( max_length );
			//fprintf( stdout, "buffer = malloc( %d )\n", max_length );
			int buffer_index = 0;  // next available location
			
			if ( sub_element_count <= 0 )
			{
				buffer[buffer_index] = ' ';
				buffer_index = buffer_index + 1;
			}

			for ( int i=0; i<sub_element_count; i++ )
			{
				str_length = read_int( config_file );
				if ( buffer_index + str_length + 1 <= max_length )
				{
					read_char_array( config_file, &(buffer[buffer_index]), str_length );
					buffer_index = buffer_index + str_length;
					buffer[buffer_index] = ' ';
					buffer_index = buffer_index + 1;
					buffer[buffer_index] = '\n';
					buffer_index = buffer_index + 1;
				}
				else
				{
					fseek( config_file, str_length, SEEK_CUR );
				}
			}
			int buffer_length = buffer_index;
			H5Tset_size( type_id, buffer_length );
		}

		else
		{
			err = -1;  // Unsupported type (as sub-element of an array)
		}
	} // end if it's an array

	else
	{
		// Unsupported type (in general)
		err = -1;
	}
	

	// Write the attribute, free the buffer, and close ids
	//
	if ( err == 0 )
	{
		// JB: 2013-02-04
		// Kludge to handle case where element.name is blank
		if ( strlen(element.name) > 0 )
			attribute_id = H5Acreate2( group_id, element.name, type_id, dataspace_id, H5P_DEFAULT, H5P_DEFAULT );
		else
			attribute_id = H5Acreate2( group_id, "Unnamed", type_id, dataspace_id, H5P_DEFAULT, H5P_DEFAULT );
		err = H5Awrite( attribute_id, type_id, buffer );
	}

	if ( buffer != NULL )
	{
		free( buffer );
		//fprintf( stdout, "free( buffer )\n" );
	}
	if ( attribute_id >= 0 ) H5Aclose( attribute_id );
	if ( dataspace_id >= 0 ) H5Dclose( dataspace_id );
	if ( type_id >= 0 ) H5Tclose( type_id );


	return err;
}

					
//========================================================================================
//----------------------------------------------------------------------------------------
// Insert configs
//
herr_t insert_configs(
	_TCHAR* argv[],
	hid_t file_id,
	int shot_number,
	int* error_index )
{
	// Read in the needed arguments
	//
	int device_count;
	char experiment_set[1024];
	char experiment[1024];
	char data_run[1024];
	char root_folder[1024];

	strcpy_s( experiment_set, 1024, argv[2] );
	strcpy_s( experiment, 1024, argv[3] );
	strcpy_s( data_run, 1024, argv[4] );
	strcpy_s( root_folder, 1024, argv[5] );
	device_count = atoi(argv[6]);

	char experiment_folder[1024];
	char data_run_folder[1024];
	sprintf_s( experiment_folder, 1024, "%s/%s/%s", root_folder, experiment_set, experiment );
	sprintf_s( data_run_folder, 1024, "%s/%s", experiment_folder, data_run );

	char efn[1024];  // efn == error_filename, shortened to allow single line error-handling
	sprintf_s( efn, 1024, "%s/Error.C++.dat", data_run_folder );
	char error_string[1024];

	herr_t err = 0;


	// Loop through devices
	//
	for ( int idevice = 0; idevice<device_count; idevice++ )
	{
		char device_name[1024];
		strcpy_s( device_name, 1024, argv[7+idevice] );


		// *****************************
		// *** Switch on device name ***
		//
		// 6K Compumotor
		// Needs special handling because it has "Probe config" and "Motion list config"
		//
		if ( (strcmp(device_name, "6K Compumotor") == 0) && (err == 0) )
		{
			// ........................................................................................................
			// Try to open the probe ids file.  There could be multiple probes being used during a single shot.
			//
			char probe_ids_filename[2048];
			sprintf_s( probe_ids_filename, 2048, "%s/%s.%s/Probe ids.%06d.dat",
				experiment_folder, data_run, device_name, shot_number );

			FILE* probe_ids_file;
			errno_t fopen_err = fopen_s( &probe_ids_file, probe_ids_filename, "rb" );
			fopen_err = 0; // not an error if file doesn't exist


			// If probe ids file exists, read the probe ids (actually receptacle ids)
			//
			int receptacle_count = 0;
			char *receptacle_ids;
			if ( probe_ids_file != NULL )
			{
				receptacle_count = read_int( probe_ids_file );
				receptacle_ids = (char *)malloc( receptacle_count );
				read_char_array( probe_ids_file, receptacle_ids, receptacle_count );
				fclose( probe_ids_file );
			}


			// Try to open the probe config file(s).
			//
			for ( int ireceptacle=0; ireceptacle<receptacle_count; ireceptacle++ )
			{
				int receptacle_id = (int)receptacle_ids[ireceptacle];
				char probe_config_filename[2048];
				sprintf_s( probe_config_filename, 2048, "%s/%s.%s/Probe config.%d.%06d.dat",
					experiment_folder, data_run, device_name, receptacle_id, shot_number );

				FILE* probe_config_file;
				errno_t fopen_err = fopen_s( &probe_config_file, probe_config_filename, "rb" );
				fopen_err = 0; // not an error if file doesn't exist


				// If probe config file exists, add probe config to HDF5 file
				//
				if ( probe_config_file != NULL )
				{
					// Probe config file structure is specified by a descriptor.  Need to open
					// descriptor file.
					//
					char probe_descriptor_filename[2048];
					sprintf_s( probe_descriptor_filename, 2048, "%s/%s.%s/Probe config descriptor.%d.%06d.dat",
						experiment_folder, data_run, device_name, receptacle_id, shot_number );

					FILE* probe_descriptor_file;
					fopen_err = fopen_s( &probe_descriptor_file, probe_descriptor_filename, "rb" );
					if ( (probe_descriptor_file == NULL) || (fopen_err != 0) )
					{
						sprintf_s( error_string, 1024, "Could not open probe descriptor file: %s.", probe_descriptor_filename );
						write_error( efn, shot_number, error_string );
						insert_error( file_id, error_index, shot_number, error_string );
						err = -1;
					}


					// Check the probe descriptor file structure
					//
					err = check_descriptor_file( probe_descriptor_file, file_id, efn, error_index, shot_number );
					if ( probe_descriptor_file != NULL ) fclose( probe_descriptor_file );


					// If everything is OK, process the probe config file
					//
					if ( err == 0 )
						process_config_file( probe_config_file, device_name, file_id, efn, error_index, shot_number );
				} // end if probe config file exists

				if ( probe_config_file != NULL ) fclose( probe_config_file );
			}
			
			if ( receptacle_count > 0 ) free( receptacle_ids );


			// ........................................................................................................
			// Now do the same for the motion list config...
			// Try to open the motion list ids file.  There could be multiple probes being used during a single shot.
			//
			char motion_list_ids_filename[2048];
			sprintf_s( motion_list_ids_filename, 2048, "%s/%s.%s/Motion list ids.%06d.dat",
				experiment_folder, data_run, device_name, shot_number );

			FILE* motion_list_ids_file;
			fopen_err = fopen_s( &motion_list_ids_file, motion_list_ids_filename, "rb" );
			fopen_err = 0; // not an error if file doesn't exist


			// If motion list ids file exists, read the motion list ids
			//
			int motion_list_count = 0;
			short *motion_list_ids;
			if ( motion_list_ids_file != NULL )
			{
				motion_list_count = read_int( motion_list_ids_file );
				motion_list_ids = (short *)malloc( 2*motion_list_count );
				read_short_array( motion_list_ids_file, motion_list_ids, motion_list_count );
				fclose( motion_list_ids_file );
			}


			// Try to open the motion list config file(s).
			//
			for ( int imotion_list=0; imotion_list<motion_list_count; imotion_list++ )
			{
				int motion_list_id = (int)motion_list_ids[imotion_list];
				char motion_list_config_filename[2048];
				sprintf_s( motion_list_config_filename, 2048, "%s/%s.%s/Motion list config.%d.%06d.dat",
					experiment_folder, data_run, device_name, motion_list_id, shot_number );

				FILE* motion_list_config_file;
				fopen_err =
					fopen_s( &motion_list_config_file, motion_list_config_filename, "rb" );
				fopen_err = 0;  // not an error if file doesn't exist


				// If motion list config file exists, add motion list config to HDF5 file
				//
				if ( motion_list_config_file != NULL )
				{
					// Motion list config file structure is specified by a descriptor.  Need to open
					// descriptor file.
					//
					char motion_list_descriptor_filename[2048];
					sprintf_s( motion_list_descriptor_filename, 2048, "%s/%s.%s/Motion list config descriptor.%d.%06d.dat",
						experiment_folder, data_run, device_name, motion_list_id, shot_number );

					FILE* motion_list_descriptor_file;
					fopen_err = fopen_s( &motion_list_descriptor_file, motion_list_descriptor_filename, "rb" );
					if ( (motion_list_descriptor_file == NULL) || (fopen_err != 0) )
					{
						sprintf_s( error_string, 1024, "Could not open motion list descriptor file: %s.",
							motion_list_descriptor_filename );
						write_error( efn, shot_number, error_string );
						insert_error( file_id, error_index, shot_number, error_string );
						err = -1;
					}


					// Check the motion list descriptor file structure
					//
					err = check_descriptor_file( motion_list_descriptor_file, file_id, efn, error_index, shot_number );
					if ( motion_list_descriptor_file != NULL ) fclose( motion_list_descriptor_file );


					// If everything is OK, process the motion list config file
					//
					if ( err == 0 )
					{
						process_config_file( motion_list_config_file, device_name, file_id, efn, error_index, shot_number );
					}
				} // end if motion list config file exists

				if ( motion_list_config_file != NULL ) fclose( motion_list_config_file );
			}
			
			if ( motion_list_count > 0 ) free( motion_list_ids );
		} // End 6K Compumotor case


		// Any other device, including remote devices
		//
		else if ( err == 0 )
		{
			// Try to open the config file.
			//
			char config_filename[2048];
			sprintf_s( config_filename, 2048, "%s/%s.%s/Config.%06d.dat",
				experiment_folder, data_run, device_name, shot_number );

			FILE* config_file;
			errno_t fopen_err = fopen_s( &config_file, config_filename, "rb" );
			fopen_err = 0;  // not an error if file doesn't exist


			// If config file exists, add config to HDF5 file
			//
			if ( config_file != NULL )
			{
				// Config file structure is specified by a descriptor.  Need to open
				// descriptor file.
				//
				char descriptor_filename[2048];
				sprintf_s( descriptor_filename, 2048, "%s/%s.%s/Config descriptor.%06d.dat",
					experiment_folder, data_run, device_name, shot_number );

				FILE* descriptor_file;
				fopen_err = fopen_s( &descriptor_file, descriptor_filename, "rb" );
				if ( (descriptor_file == NULL) || (fopen_err != 0) )
				{
					sprintf_s( error_string, 1024, "Could not open descriptor file: %s.", descriptor_filename );
					write_error( efn, shot_number, error_string );
					insert_error( file_id, error_index, shot_number, error_string );
					err = -1;
				}


				// Check the descriptor file structure
				//
				err = check_descriptor_file( descriptor_file, file_id, efn, error_index, shot_number );
				if ( descriptor_file != NULL ) fclose( descriptor_file );


				// If everything is OK, process the config file
				//
				if ( err == 0 ) process_config_file( config_file, device_name, file_id, efn, error_index, shot_number );
			} // end if config file exists

			if ( config_file != NULL ) fclose( config_file );
		} // End general case (i.e. devices other than 6K Compumotor)


		// Error has occurred
		//
		else
		{
			// do nothing
		}
	} // End looping through all devices

	return err;
}


