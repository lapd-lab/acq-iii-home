//========================================================================================
// Insert MSI.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"


//----------------------------------------------------------------------------------------
// Extend/Create 2D MSI dataset
//
herr_t extend_create_2D_MSI_dataset(
	char* dataset,
	bool found,
	hid_t group_id,
	int index,
	int data_length,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id )
{
	herr_t err = 0;


	// Open the dataset (if it was found)
	//
	if ( found )
	{
		*dataset_id = H5Dopen2( group_id, dataset, H5P_DEFAULT );
		if ( *dataset_id < 0 ) return -1;
	}


	// File dataspace
	//
	int file_rank = 2;
	hsize_t file_dims[2] = { index+1, data_length };
	hsize_t file_maxdims[2] = { 4294967295, data_length };

	if ( found )
	{
		*file_space_id = H5Dget_space( *dataset_id );
		if ( *file_space_id < 0 ) return -1;
		err = H5Sset_extent_simple( *file_space_id, file_rank, file_dims, file_maxdims );
		if ( err < 0 ) return -1;
	}
	else
	{
		*file_space_id = H5Screate_simple( file_rank, file_dims, file_maxdims );
		if ( *file_space_id < 0 ) return -1;
	}

	hsize_t start[2] = { index, 0 };
	hsize_t stride[2] = { 1, 1 };
	hsize_t count[2] = { 1, 1 };
	hsize_t block[2] = { 1, data_length };
	err = H5Sselect_hyperslab( *file_space_id, H5S_SELECT_SET, start, stride, count, block );
	if ( err < 0 ) return -1;


	// Memory dataspace
	//
	int mem_rank = 1;
	hsize_t mem_dims[1] = { data_length };
	hsize_t mem_maxdims[1] = { data_length };
	*mem_space_id = H5Screate_simple( mem_rank, mem_dims, mem_maxdims );
	if ( *mem_space_id < 0 ) return -1;


	// Memory datatype
	// This is the hard part because there are a number of different datatypes, should have a
	// function call for it.
	//
	*mem_type_id = H5Tcopy( H5T_NATIVE_FLOAT );
	if ( *mem_type_id < 0 ) return -1;


	if ( found )
	{
		// Extend the dataset
		//
		err = H5Dextend( *dataset_id, file_dims );
		if ( err < 0 ) return -1;
	}
	else
	{
		// Dataset creation property list
		//
		hid_t plist_id = H5Pcreate( H5P_DATASET_CREATE );
		if ( plist_id < 0 ) return -1;

		hsize_t file_chunk[2] = { 1, data_length };
		int compression = 4;
		H5Pset_chunk( plist_id, file_rank, file_chunk );
		H5Pset_deflate( plist_id, compression );


		// Everything is set up so now create the 2D dataset
		//
		*dataset_id = H5Dcreate2( group_id, dataset, *mem_type_id, *file_space_id, H5P_DEFAULT, plist_id, H5P_DEFAULT );
		if ( *dataset_id < 0 ) return -1;

		H5Pclose( plist_id );
	}

	return 0;
}


//----------------------------------------------------------------------------------------
// Extend/Create summary dataset
//
herr_t extend_create_summary_dataset(
	char* dataset,
	bool found,
	hid_t group_id,
	int index,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id )
{
	herr_t err = 0;


	// Open the dataset (if it was found)
	//
	if ( found )
	{
		*dataset_id = H5Dopen2( group_id, dataset, H5P_DEFAULT );
		if ( *dataset_id < 0 ) return -1;
	}


	// File dataspace
	//
	int file_rank = 1;
	hsize_t file_dims[1] = { index+1 };
	hsize_t file_maxdims[1] = { 4294967295 };

	if ( found )
	{
		*file_space_id = H5Dget_space( *dataset_id );
		if ( *file_space_id < 0 ) return -1;
		err = H5Sset_extent_simple( *file_space_id, file_rank, file_dims, file_maxdims );
		if ( err < 0 ) return -1;
	}
	else
	{
		*file_space_id = H5Screate_simple( file_rank, file_dims, file_maxdims );
		if ( *file_space_id < 0 ) return -1;
	}

	hsize_t start[1] = { index };
	hsize_t stride[1] = { 1 };
	hsize_t count[1] = { 1 };
	hsize_t block[1] = { 1 };
	err = H5Sselect_hyperslab( *file_space_id, H5S_SELECT_SET, start, stride, count, block );
	if ( err < 0 ) return -1;


	// Memory dataspace
	//
	int mem_rank = 1;
	hsize_t mem_dims[1] = { 1 };
	hsize_t mem_maxdims[1] = { 1 };
	*mem_space_id = H5Screate_simple( mem_rank, mem_dims, mem_maxdims );
	if ( *mem_space_id < 0 ) return -1;


	// Memory datatype
	// This is the hard part because there are a number of different datatypes, should have a
	// function call for it.
	//
	hid_t char_field_id = H5Tcopy( H5T_NATIVE_CHAR );
	hid_t int_field_id = H5Tcopy( H5T_NATIVE_INT );
	hid_t float_field_id = H5Tcopy( H5T_NATIVE_FLOAT );
	hid_t double_field_id = H5Tcopy( H5T_NATIVE_DOUBLE );
	if ( strcmp(dataset, "Discharge summary") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 25 );
		if ( *mem_type_id < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Timestamp", 4, double_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Data valid", 12, char_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Pulse length", 13, float_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Peak current", 17, float_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Bank voltage", 21, float_field_id );  if ( err < 0 ) return -1;
	}

	if ( strcmp(dataset, "Heater summary") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 25 );
		if ( *mem_type_id < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Timestamp", 4, double_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Data valid", 12, char_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Heater current", 13, float_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Heater voltage", 17, float_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Heater temperature", 21, float_field_id );  if ( err < 0 ) return -1;
	}

	if ( strcmp(dataset, "Gas pressure summary") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 22 );
		if ( *mem_type_id < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Timestamp", 4, double_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Ion gauge data valid", 12, char_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "RGA data valid", 13, char_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Fill pressure", 14, float_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Peak AMU", 18, float_field_id );  if ( err < 0 ) return -1;
	}

	if ( strcmp(dataset, "Magnetic field summary") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 17 );
		if ( *mem_type_id < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Timestamp", 4, double_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Data valid", 12, char_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Peak magnetic field", 13, float_field_id );  if ( err < 0 ) return -1;
	}

	if ( strcmp(dataset, "Interferometer summary list") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 17 );
		if ( *mem_type_id < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Timestamp", 4, double_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Data valid", 12, char_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Peak density", 13, float_field_id );  if ( err < 0 ) return -1;
	}

	H5Tclose( char_field_id );  H5Tclose( int_field_id );  H5Tclose( float_field_id );  H5Tclose( double_field_id );


	if ( found )
	{
		// Extend the dataset
		//
		err = H5Dextend( *dataset_id, file_dims );
		if ( err < 0 ) return -1;
	}
	else
	{
		// Dataset creation property list
		//
		hid_t plist_id = H5Pcreate( H5P_DATASET_CREATE );
		if ( plist_id < 0 ) return -1;

		hsize_t file_chunk[1] = { 1 };
		int compression = 4;
		H5Pset_chunk( plist_id, file_rank, file_chunk );
		H5Pset_deflate( plist_id, compression );


		// Everything is set up so now create the 2D dataset
		//
		*dataset_id = H5Dcreate2( group_id, dataset, *mem_type_id, *file_space_id, H5P_DEFAULT, plist_id, H5P_DEFAULT );
		if ( *dataset_id < 0 ) return -1;

		H5Pclose( plist_id );
	}

	return 0;
}


//========================================================================================
//----------------------------------------------------------------------------------------
// Insert MSI
//
herr_t insert_MSI(
	_TCHAR* argv[],
	hid_t file_id,
	int shot_number,
	int* MSI_index,
	int* error_index )
{
	// Read in the arguments
	//
	int index = *MSI_index;
	char experiment_set[1024];
	char experiment[1024];
	char data_run[1024];
	char root_folder[1024];

	strcpy_s( experiment_set, 1024, argv[2] );
	strcpy_s( experiment, 1024, argv[3] );
	strcpy_s( data_run, 1024, argv[4] );
	strcpy_s( root_folder, 1024, argv[5] );

	char experiment_folder[1024];
	char data_run_folder[1024];
	sprintf_s( experiment_folder, 1024, "%s/%s/%s", root_folder, experiment_set, experiment );
	sprintf_s( data_run_folder, 1024, "%s/%s", experiment_folder, data_run );

	char efn[1024];  // efn == error_filename, shortened to allow single line error-handling
	sprintf_s( efn, 1024, "%s/Error.C++.dat", data_run_folder );


	// Deal with shot number
	//
	int* shot_number_ptr = (int*) malloc( sizeof(int) );
	char* shot_number_bytes;
	*shot_number_ptr = shot_number;
	shot_number_bytes = (char*) shot_number_ptr;


	// *****************************************
	// *** Open and read the MSI string file ***
	//
	// Open the MSI string file
	//
	herr_t err = 0;
	char MSI_filename[2048];
	sprintf_s( MSI_filename, 2048, "%s/MSI/MSI.%06d.dat", data_run_folder, shot_number );
	FILE* MSI_file;
	errno_t fopen_err = fopen_s( &MSI_file, MSI_filename, "rb" );  if ( fopen_err ) return -1;
	int reported_length = read_int( MSI_file );

	char *MSI_string = (char *)malloc( reported_length );
	// size_t fread( void *buffer, size_t size, size_t count, FILE *stream );
	size_t nread = fread( MSI_string, 1, reported_length, MSI_file );
	fclose( MSI_file );

	if ( nread != reported_length ) err = -1;


	// Read timestamp
	//
	int str_index = 0;
	double timestamp_value;
	if ( err == 0 )
		timestamp_value = sread_double( MSI_string, &str_index );
	double *timestamp = (double *)malloc( sizeof(double) );
	char *timestep_bytes;
	*timestamp = timestamp_value;
	timestep_bytes = (char *) timestamp;


	// Read discharge
	//
	struct dischargeStruct discharge;
	if ( err == 0 )
	{
		for ( int i=0; i<4; i++ ) discharge.summary_buffer[ 0+i] = shot_number_bytes[i];
		for ( int i=0; i<8; i++ ) discharge.summary_buffer[ 4+i] = timestep_bytes[i];

		sread_bool_byte( MSI_string, &(discharge.summary_buffer[12]), &str_index );  // discharge.data_valid

		discharge.current_count = sread_int( MSI_string, &str_index );
		discharge.current = (float *)malloc( discharge.current_count * sizeof(float) );
		sread_float_array( MSI_string, discharge.current, discharge.current_count, &str_index );

		discharge.voltage_count = sread_int( MSI_string, &str_index );
		discharge.cathode_anode_voltage = (float *)malloc( discharge.voltage_count * sizeof(float) );
		sread_float_array( MSI_string, discharge.cathode_anode_voltage, discharge.voltage_count, &str_index );

		sread_float_bytes( MSI_string, &(discharge.summary_buffer[13]), &str_index );  // discharge.pulse_length 
		sread_float_bytes( MSI_string, &(discharge.summary_buffer[17]), &str_index );  // discharge.peak_current
		sread_float_bytes( MSI_string, &(discharge.summary_buffer[21]), &str_index );  // discharge.bank_voltage
	}
	else
	{
		discharge.current = (float *)malloc( 1024 * sizeof(float) );
		discharge.cathode_anode_voltage = (float *)malloc( 1024 * sizeof(float) );
	}


	// Read heater
	//
	struct heaterStruct heater;
	if ( err == 0 )
	{
		for ( int i=0; i<4; i++ ) heater.summary_buffer[ 0+i] = shot_number_bytes[i];
		for ( int i=0; i<8; i++ ) heater.summary_buffer[ 4+i] = timestep_bytes[i];

		sread_bool_byte( MSI_string, &(heater.summary_buffer[12]), &str_index );  // heater.data_valid
		sread_float_bytes( MSI_string, &(heater.summary_buffer[13]), &str_index );  // heater.current
		sread_float_bytes( MSI_string, &(heater.summary_buffer[17]), &str_index );  // heater.voltage
		sread_float_bytes( MSI_string, &(heater.summary_buffer[21]), &str_index );  // heater.temperature
	}


	// Read gas pressure
	//
	struct pressureStruct pressure;
	if ( err == 0 )
	{
		for ( int i=0; i<4; i++ ) pressure.summary_buffer[ 0+i] = shot_number_bytes[i];
		for ( int i=0; i<8; i++ ) pressure.summary_buffer[ 4+i] = timestep_bytes[i];

		sread_bool_byte( MSI_string, &(pressure.summary_buffer[12]), &str_index );  // pressure.ion_gauge_data_valid
		sread_bool_byte( MSI_string, &(pressure.summary_buffer[13]), &str_index );  // pressure.RGA_data_valid
		sread_float_bytes( MSI_string, &(pressure.summary_buffer[14]), &str_index );  // pressure.fill_pressure
		sread_float_bytes( MSI_string, &(pressure.summary_buffer[18]), &str_index );  // pressure.peak_AMU

		pressure.pressure_count = sread_int( MSI_string, &str_index );
		pressure.RGA_partial_pressure = (float *)malloc( pressure.pressure_count * sizeof(float) );
		sread_float_array( MSI_string, pressure.RGA_partial_pressure, pressure.pressure_count, &str_index );
	}
	else
	{
		pressure.RGA_partial_pressure = (float *)malloc( 1024 * sizeof(float) );
	}


	// Read magnetic field
	//
	struct magneticFieldStruct magnetic_field;
	if ( err == 0 )
	{
		for ( int i=0; i<4; i++ ) magnetic_field.summary_buffer[ 0+i] = shot_number_bytes[i];
		for ( int i=0; i<8; i++ ) magnetic_field.summary_buffer[ 4+i] = timestep_bytes[i];

		sread_bool_byte( MSI_string, &(magnetic_field.summary_buffer[12]), &str_index );  // magnetic_field.data_valid

		magnetic_field.profile_count = sread_int( MSI_string, &str_index );
		magnetic_field.profile = (float *)malloc( magnetic_field.profile_count * sizeof(float) );
		sread_float_array( MSI_string, magnetic_field.profile, magnetic_field.profile_count, &str_index );

		magnetic_field.current_count = sread_int( MSI_string, &str_index );
		magnetic_field.supply_current = (float *)malloc( magnetic_field.current_count * sizeof(float) );
		sread_float_array( MSI_string, magnetic_field.supply_current, magnetic_field.current_count, &str_index );

		sread_float_bytes( MSI_string, &(magnetic_field.summary_buffer[13]), &str_index );  // magnetic_field.peak_field
	}
	else
	{
		magnetic_field.profile = (float *)malloc( 1024 * sizeof(float) );
		magnetic_field.supply_current = (float *)malloc( 1024 * sizeof(float) );
	}


	// Read interferometers
	//
	struct interferometerStruct *interferometer;
	int interferometer_count;
	if ( err == 0 )
	{
		interferometer_count = sread_int( MSI_string, &str_index );
		interferometer = (interferometerStruct *)malloc( interferometer_count * sizeof interferometerStruct );
		for ( int j=0; j<interferometer_count; j++ )
		{
			for ( int i=0; i<4; i++ ) interferometer[j].summary_buffer[ 0+i] = shot_number_bytes[i];
			for ( int i=0; i<8; i++ ) interferometer[j].summary_buffer[ 4+i] = timestep_bytes[i];

			sread_bool_byte( MSI_string, &(interferometer[j].summary_buffer[12]), &str_index );  // interferometer[j].data_valid

			interferometer[j].density_count = sread_int( MSI_string, &str_index );
			interferometer[j].density = (float *)malloc( interferometer[j].density_count * sizeof(float) );
			sread_float_array( MSI_string, interferometer[j].density, interferometer[j].density_count, &str_index );

			sread_float_bytes( MSI_string, &(interferometer[j].summary_buffer[13]), &str_index );  // interferometer[j].peak_density
		}
	}
	else
	{
		interferometer_count = 1;
		interferometer = (interferometerStruct *)malloc( interferometer_count * sizeof interferometerStruct );
		interferometer[0].density = (float *)malloc( 1024 * sizeof(float) );
	}


	// *************************************************
	// *** Process each MSI dataset in the HDF5 file ***
	//
	// This involves, for each of the many datasets:
	//   1) looking for the dataset
	//   2) if found, extending it
	//   3) if not found, create it
	//   4) write the shot into it
	//   5) closing the id's that were opened along the way
	//
	// First, though, declare the id's that we'll need
	//
	hid_t group_id;
	hid_t dataset_id;
	hid_t file_space_id;
	hid_t mem_space_id;
	hid_t mem_type_id;

	bool found;


	// :::::::::::::::::::::::::: /MSI/Discharge ::::::::::::::::::::::::::
	// ::::::: /MSI/Discharge/Discharge current
	//
	if ( err == 0 )
		err = find_dataset( file_id, "/MSI/Discharge", "Discharge current", &group_id, &found );
	if ( err == 0 )
		err = extend_create_2D_MSI_dataset( "Discharge current", found, group_id, 
			index, discharge.current_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err == 0 )
		err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, discharge.current );
	if ( err < 0 )
		{ write_hdf5_error( efn, shot_number ); insert_hdf5_error( file_id, error_index, shot_number ); }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );


	// ::::::: /MSI/Discharge/Cathode-anode voltage
	//
	if ( err == 0 )
		err = find_dataset( file_id, "/MSI/Discharge", "Cathode-anode voltage", &group_id, &found );
	if ( err == 0 )
		err = extend_create_2D_MSI_dataset( "Cathode-anode voltage", found, group_id, 
			index, discharge.voltage_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err == 0 )
		err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, discharge.cathode_anode_voltage );
	if ( err < 0 )
		{ write_hdf5_error( efn, shot_number ); insert_hdf5_error( file_id, error_index, shot_number ); }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );


	// ::::::: /MSI/Discharge/Discharge summary
	//
	if ( err == 0 )
		err = find_dataset( file_id, "/MSI/Discharge", "Discharge summary", &group_id, &found );
	if ( err == 0 )
		err = extend_create_summary_dataset( "Discharge summary",
			found, group_id, index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err == 0 )
		err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, discharge.summary_buffer );
	if ( err < 0 )
		{ write_hdf5_error( efn, shot_number ); insert_hdf5_error( file_id, error_index, shot_number ); }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );



	// :::::::::::::::::::::::::: /MSI/Heater ::::::::::::::::::::::::::
	// ::::::: /MSI/Heater/Heater summary
	//
	if ( err == 0 )
		err = find_dataset( file_id, "/MSI/Heater", "Heater summary", &group_id, &found );
	if ( err == 0 )
		err = extend_create_summary_dataset( "Heater summary", found, group_id, 
			index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err == 0 )
		err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, heater.summary_buffer );
	if ( err < 0 )
		{ write_hdf5_error( efn, shot_number ); insert_hdf5_error( file_id, error_index, shot_number ); }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );



	// :::::::::::::::::::::::::: /MSI/Gas pressure ::::::::::::::::::::::::::
	// ::::::: /MSI/Gas pressure/Discharge current
	//
	if ( err == 0 )
		err = find_dataset( file_id, "/MSI/Gas pressure", "RGA partial pressures", &group_id, &found );
	if ( err == 0 )
		err = extend_create_2D_MSI_dataset( "RGA partial pressures", found, group_id, 
			index, pressure.pressure_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err == 0 )
		err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, pressure.RGA_partial_pressure );
	if ( err < 0 )
		{ write_hdf5_error( efn, shot_number ); insert_hdf5_error( file_id, error_index, shot_number ); }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );


	// ::::::: /MSI/Gas pressure/Gas pressure summary
	//
	if ( err == 0 )
		err = find_dataset( file_id, "/MSI/Gas pressure", "Gas pressure summary", &group_id, &found );
	if ( err == 0 )
		err = extend_create_summary_dataset( "Gas pressure summary", found, group_id, 
			index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err == 0 )
		err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, pressure.summary_buffer );
	if ( err < 0 )
		{ write_hdf5_error( efn, shot_number ); insert_hdf5_error( file_id, error_index, shot_number ); }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );



	// :::::::::::::::::::::::::: /MSI/Magnetic field ::::::::::::::::::::::::::
	// ::::::: /MSI/Magnetic field/Magnetic field profile
	//
	if ( err == 0 )
		err = find_dataset( file_id, "/MSI/Magnetic field", "Magnetic field profile", &group_id, &found );
	if ( err == 0 )
		err = extend_create_2D_MSI_dataset( "Magnetic field profile", found, group_id, 
			index, magnetic_field.profile_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err == 0 )
		err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, magnetic_field.profile );
	if ( err < 0 )
		{ write_hdf5_error( efn, shot_number ); insert_hdf5_error( file_id, error_index, shot_number ); }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );


	// ::::::: /MSI/Magnetic field/Magnet power supply currents
	//
	if ( err == 0 )
		err = find_dataset( file_id, "/MSI/Magnetic field", "Magnet power supply currents", &group_id, &found );
	if ( err == 0 )
		err = extend_create_2D_MSI_dataset( "Magnet power supply currents", found, group_id, 
			index, magnetic_field.current_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err == 0 )
		err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, magnetic_field.supply_current );
	if ( err < 0 )
		{ write_hdf5_error( efn, shot_number ); insert_hdf5_error( file_id, error_index, shot_number ); }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );


	// ::::::: /MSI/Magnetic field/Magnetic field summary
	//
	if ( err == 0 )
		err = find_dataset( file_id, "/MSI/Magnetic field", "Magnetic field summary", &group_id, &found );
	if ( err == 0 )
		err = extend_create_summary_dataset( "Magnetic field summary", found, group_id, 
			index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err == 0 )
		err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, magnetic_field.summary_buffer );
	if ( err < 0 )
		{ write_hdf5_error( efn, shot_number ); insert_hdf5_error( file_id, error_index, shot_number ); }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );


	// Interferometer array
	//
	char group_name[1024];
	for ( int j=0; j<interferometer_count; j++ )
	{
		sprintf_s( group_name, 1024, "/MSI/Interferometer array/Interferometer [%d]", j );


		// ::::::: /MSI/Interferometer array/Interferometer[j]/Interferometer trace
		//
		if ( err == 0 )
			err = find_dataset( file_id, group_name, "Interferometer trace", &group_id, &found );
		if ( err == 0 )
			err = extend_create_2D_MSI_dataset( "Interferometer trace", found, group_id, 
				index, interferometer[j].density_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
		if ( err == 0 )
			err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, interferometer[j].density );
		if ( err < 0 )
			{ write_hdf5_error( efn, shot_number ); insert_hdf5_error( file_id, error_index, shot_number ); }
		H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );


		// ::::::: /MSI/Interferometer array/Interferometer[j]/Interferometer summary list
		//
		if ( err == 0 )
			err = find_dataset( file_id, group_name, "Interferometer summary list", &group_id, &found );
		if ( err == 0 )
			err = extend_create_summary_dataset( "Interferometer summary list", found, group_id, 
				index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
		if ( err == 0 )
			err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, interferometer[j].summary_buffer );
		if ( err < 0 )
			{ write_hdf5_error( efn, shot_number ); insert_hdf5_error( file_id, error_index, shot_number ); }
		H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );
	}


	// **********************************************
	// *** Free up HDF5 id's and allocated memory ***
	//
	free( discharge.current );
	free( discharge.cathode_anode_voltage );
	free( pressure.RGA_partial_pressure );
	free( magnetic_field.profile );
	free( magnetic_field.supply_current );
	for ( int j=0; j<interferometer_count; j++ )
		free( interferometer[j].density );
	free( interferometer );

	free( timestamp );
	free( shot_number_ptr );

	H5Gclose( group_id );

	*MSI_index = index + 1;

	return err;
}


