//========================================================================================
// Insert sequence lines.h
//

#include "hdf5.h"


herr_t extend_create_sequence_dataset(
	char* dataset,
	bool found,
	hid_t group_id,
	int index,
	int line_count,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id );

herr_t insert_sequence_lines(
	_TCHAR* argv[],
	hid_t file_id,
	int shot_number,
	int* sequence_lines_index,
	int* error_index );


