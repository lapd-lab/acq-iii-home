
This directory contains the binary (release) distribution of 
HDF5-1.6.5 that was compiled on an IBM PC running Windows XP 
and using .NET 2003 (VS 7.1).

The contents of this directory are:

    COMPILE.txt - Compile information
    COPYING.txt - Copyright notice
    README.txt  - This file
    RELEASE.txt - Detailed information regarding this release
    bin\        - HDF5 utilities
    bindll\     - HDF5 dll utilities
    dll\        - HDF5 dlls (C, C++)
    include\    - HDF5 include files
    lib\        - HDF5 C, C++, and High Level C libraries
  

The binaries were built with ZLIB compression enabled (zlib 1.2.2). Therefore 
you MUST link with the GNU ZLIB library when compiling with these binaries. We 
provide the pre-compiled Windows binary distribution for ZLIB 1.2.2 from 
our ftp server at:

    ftp://ftp.ncsa.uiuc.edu/HDF/lib-external/zlib/1.2/bin/

These binaries were also built with SZIP 2.0 compression (Encoder ENABLED).
When compiling with these binaries, you must link with the SZIP library.  
Information regarding SZIP can be found at:

   http://hdf.ncsa.uiuc.edu/doc_resource/SZIP/

You can obtain the SZIP source code and binaries from our ftp server at:
  
   ftp://ftp.ncsa.uiuc.edu/HDF/lib-external/szip/

For details on how the HDF5 libraries were created, refer to the
INSTALL_Windows.txt file in the HDF5 source code, under:

    ftp://ftp.ncsa.uiuc.edu/HDF/HDF5/current/src/unpacked/release_docs/

Source code can be found on the NCSA ftp server in:

    ftp://ftp.ncsa.uiuc.edu/HDF/HDF5/current/src/

Please send questions, comments, and suggestions to:

    hdfhelp@ncsa.uiuc.edu



