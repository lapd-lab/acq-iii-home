; TODO: make this into a procedure
; TODO: detect number of shots? (for aborted runs)
; TODO: don't rewrite motion list if run is complete.
;
; pro hdf5_repair, datarun=datarun, nshot=nshot
; HDF5_repair.pro
;
; If a datarun is very large and/or incomplete, the LabVIEW HDF5 conversion VI
; will sometimes timeout when creating the datasets, but will write the
; configuration information. This program will take the HDF5 file and the SIS
; file, determine how much of the run was actually completed, update the HDF5
; configuration as needed, and write the datasets.

; ---------------------------------------------------------------------------

!PATH = 'C:\UCLA development\Ideas\hdf5_lapd_simple__define' + ';' + !PATH  ; the separation character ';' is for Windows

; Get filenames and nshot
sis_filename = dialog_pickfile(title = 'Choose SIS data file', filter='*.dat')
if sis_filename eq '' then begin
    print, 'No file selected, exiting...'
    stop
endif

; chop off '.SIS 3301.dat' and check for corresponding HDF5 file
hdf5_filename = strmid(sis_filename, 0, strlen(sis_filename)-13) + '.hdf5'
if not file_test(hdf5_filename) then begin
    print, 'HDF5 file for ' + sis_filename + ' not found, exiting...'
    stop
endif

nshot = ''
read, 'Enter number of shots per point [default=1]: ', nshot
if nshot eq '' then nshot = 1 else nshot = fix(nshot)

; Define structures for headers
header = {data_head,shot_number:long(1),$
                  n_data:long(1),$
                  clipped:fix(1),channel:fix(1),$
                  min:fix(1),max:fix(1),unused1:float(1),unused2:float(1),$
                  unused3:float(1),unused4:float(1),unused5:float(1),$
                  unused6:float(1),unused7:float(1),gain:float(1),$
                  offset:float(1)}
header_size = 52
header_names = ['Shot number', 'Scale', 'Offset', 'Min', 'Max', 'Clipped']
hdf_header = { hdf_head, shot_number: ulong(1), scale: double(1.0), $
               offset: double(0.0), min: fix(1), max: fix(1), clipped: byte(0) }
               
; Get configuration information from the HDF5 file.
hdf5_lapd = obj_new('HDF5_lapd_simple', filename=hdf5_filename, nshot=nshot)
config = hdf5_lapd->get_config()
board_list = hdf5_lapd->get_board_list()
nsamples = hdf5_lapd->get_nsamples()

; Build a list of group names that will contain each dataset in the HDF5 file,
; and the number of samples in each dataset.
channel_names = strarr(32)
channel_samples = lonarr(32)
nchannels = 0
for i = 0, n_elements(board_list) - 1 do begin
    channel_list = hdf5_lapd->get_channel_list(board_list[i])
    for j = 0, n_elements(channel_list) - 1 do begin
        channel_names[nchannels] = string(format='(%"%s [%0i:%0i]")', $
            config.daq_config_name, board_list[i], channel_list[j])
        channel_samples[nchannels] = nsamples[i]
        nchannels++
    endfor
endfor
channel_names = channel_names[0:nchannels - 1]
channel_samples = channel_samples[0:nchannels - 1]

obj_destroy, hdf5_lapd

; Open SIS file and determine the total number of shots from the size of the SIS
; file.
openr, sis_lun, sis_filename, /get_lun
shot_size = total(channel_samples*2 + header_size, /integer)
total_shots = (fstat(sis_lun)).size / shot_size

; Determine which portion of the motion list was actually completed, and update
; the motion list in the HDF5 file
root = obj_new('HDF5_File', filepath=hdf5_filename, operation='Open')
motion_list = root->open_group('Raw data + config/6K Compumotor/Motion list: ' $
                                + config.motion_list_name)
xy = motion_list->read_attribute('[x,y]')
if config.ny eq 1 then begin
    ; x-line
    nx = total_shots / config.nshot
    motion_list->write_attribute, 'Nx', nx
    total_motions = nx * config.ny 
    if nx mod 2 eq 0 then begin
        dx = motion_list->read_attribute('Delta x')
        x0 = xy[0, total_motions/2] - dx / 2.
    endif else begin
        x0 = xy[0, total_motions/2]
    endelse
    motion_list->write_attribute, 'Grid center y', y0
endif else begin
    ; plane and y-lines
    ; for planes, keep only up to the last complete x-line
    ny = total_shots / config.nx / config.nshot 
    motion_list->write_attribute, 'Ny', ny
    total_shots = ny * config.nx * config.nshot
    total_motions = ny * config.nx 
    if ny mod 2 eq 0 then begin
        dy = motion_list->read_attribute('Delta y')
        y0 = xy[1, total_motions/2] + dy / 2.
    endif else begin
        y0 = xy[1, total_motions/2]
    endelse
    motion_list->write_attribute, 'Grid center y', y0
endelse
motion_list->write_attribute, 'Data motion count', total_motions
xy = xy[*,0:total_motions - 1]
motion_list->write_attribute, '[x,y]', xy
theta = motion_list->read_attribute('theta')
theta = theta[0:total_motions-1]
motion_list->write_attribute, 'theta', theta

obj_destroy, motion_list

; Open the SIS group so we can write the datasets to it
daq = root->open_group('Raw data + config/SIS 3301')

for i = 0, nchannels - 1 do begin
    print, 'processing ' + channel_names[i]
    ; seek past first i shots
    if i ne 0 then $
        point_lun, sis_lun, total(channel_samples[0:i-1]*2 + header_size, /integer)
    data_in = intarr(channel_samples[i])
    data = 0
    data = intarr(channel_samples[i], total_shots)
    headers = replicate(hdf_header, total_shots)
    ; Reads one channel's dataset and headers, then seek ahead one full record
    ; length to get next shot for this channel. 
    for shot = 0, total_shots - 1 do begin
        point_lun, -sis_lun, pos
        readu, sis_lun, header, data_in
        data[*, shot] = data_in
        hdf_header.shot_number = header.shot_number
        hdf_header.scale = header.gain
        hdf_header.offset = header.offset
        hdf_header.min = header.min
        hdf_header.max = header.max
        hdf_header.clipped = header.clipped
        headers[shot] = hdf_header
        point_lun, sis_lun, pos + shot_size
    endfor
    
    daq->write_dataset, channel_names[i], data, $
        chunk_dimensions=[channel_samples[i],1], gzip=9
    daq->write_dataset, channel_names[i] + ' headers', headers, $
        member_names=header_names
endfor
obj_destroy, daq
obj_destroy, root
data = 0
close, sis_lun

end
