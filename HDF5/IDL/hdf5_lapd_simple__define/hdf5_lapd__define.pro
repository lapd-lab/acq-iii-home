; NAME:
;   HDF5_LaPD
;
; PURPOSE:
;   This class encapsulates basic I/O operations on LaPD raw data/config 
;   files stored in HDF5 format.  Typically this serves as a base class for 
;   more sophisticated routines (e.g., HDF5_LAPD_SIMPLE)
;
; Example code:
;     ; initially creates the object and open the HDF file
;     HDF5_LaPD = OBJ_NEW('HDF5_LaPD', filename='experiment.hdf')
;
;     ; get an array of names of devices used in the data run
;     device_names = HDF5_LaPD->Read_device_names()   
;     ; returns an array of dataset names for the specified device
;     dataset_names = HDF5_LaPD->Read_dataset_names(device_name) 
;     ; returns the specified dataset for the specified device as an IDL array
;     dataset = HDF5_LaPD->Read_dataset(device_name, dataset_name)
;
;     ; get some error information (this doesn't do much)
;     status = HDF5_LaPD->Get_error_status()          
;     message = HDF5_LaPD->Get_error_message()        
;     call_stack = HDF5_LaPD->Get_error_call_stack()  
;
;     ; close the file and destroy the object
;     OBJ_DESTROY, HDF5_LaPD
;
; ERROR HANDLING:
;   Error handling is specified by the HDF5_error class.  Currently, when an 
;   error is hit information is stored and displayed, then execution is halted.
;   This is also how the underlying HDF5 functions and procedures operate.  
;   Great caution must be exercised if this behavior is ever changed because the
;   code in many places depends on the execution halting.
;
; MODIFICATION HISTORY:
;   Changed by:  Eric Lawrence   August, 2005.
;   Written by:  Jim Bamber  July, 2005.
;   


;----------------------------------------------------------------------------
; HDF5_LaPD::Init
;
; Purpose:
;  Initializes the HDF5_LaPD object, optionally opening the specified file.
;
FUNCTION HDF5_LaPD::Init, FILENAME=filename

    self.error = OBJ_NEW('HDF5_error')
    self.root_group = OBJ_NEW('HDF5_group')
    self.raw_group = OBJ_NEW('HDF5_group')

    IF (N_ELEMENTS(filename) EQ 1) THEN $
         self->Open, filename

    RETURN, 1
END


;----------------------------------------------------------------------------
; HDF5_LaPD::Cleanup
;
; Purpose:
;  Cleans up all memory associated with the HDF5_LaPD object.
;
PRO HDF5_LaPD::Cleanup

    self->Close

    OBJ_DESTROY, self.error
    OBJ_DESTROY, self.root_group
    OBJ_DESTROY, self.raw_group

END


;----------------------------------------------------------------------------
; HDF5_LaPD::Open
;
; Purpose:
;  Opens the specified file.
;
PRO HDF5_LaPD::Open, filename

    self.file_id = H5F_OPEN(filename)
    IF (self.file_id EQ 0) THEN $
         self.error->Handle_error, "File: " + filename $
                        + " could not be opened by H5F_OPEN()"

    self.root_group->Open, self.file_id, "/"
    attribute = self.root_group->Read_attribute("LaPD HDF5 software version")
    self.version = attribute
    IF ( (self.version NE '1.0') AND $
         (self.version NE '1.1') ) THEN $
         self.error->Handle_error, "File version: " + self.version $
                        + " is not supported"

;   File formats under versions 1.0 and 1.1 
;   (i.e. all currently supported versions are identical)
    self.raw_group->Open, self.file_id, "Raw data + config"

END


;----------------------------------------------------------------------------
; HDF5_LaPD::Close
;
; Purpose:
;  Closes the currently open file.
;
PRO HDF5_LaPD::Close

    self.root_group->Close
    self.raw_group->Close

    IF (self.file_id NE 0) THEN $
         H5F_CLOSE, self.file_id

    self.file_id = 0
    self.version = ""

END


;----------------------------------------------------------------------------
; HDF5_LaPD::Get_<property>
;
; Purpose:
;  This is a series of functions to return various internal properties.
;
FUNCTION HDF5_LaPD::Get_error_status
    RETURN, self.error->Get_status()
END

FUNCTION HDF5_LaPD::Get_error_message
    RETURN, self.error->Get_message()
END

FUNCTION HDF5_LaPD::Get_error_call_stack
    RETURN, self.error->Get_call_stack()
END


;----------------------------------------------------------------------------
; HDF5_LaPD::Read_device_names
;
; Purpose:
;  Returns an array of the names of the devices connected for the data run
;  represented by the current LaPD raw HDF5 file.
;
FUNCTION HDF5_LaPD::Read_device_names

    group_names = self.raw_group->Read_group_names()
    IF (group_names[0] EQ '') THEN $
         device_names = STRARR(1) $
    ELSE BEGIN $
         indices = WHERE(group_names NE 'Data run sequence', count)
         IF (count gt 0) THEN $
              device_names = group_names[indices] $
         ELSE $
              device_names = STRARR(1)
    ENDELSE

    RETURN, device_names
END

;----------------------------------------------------------------------------
; HDF5_LaPD::Open_device
; 
; Purpose:
;   Returns the device named device_name as an HDF5_Group object.

function HDF5_LaPD::Open_device, device_name
    device_group = self.raw_group->open_group(device_name)
    return, device_group
end


;----------------------------------------------------------------------------
; HDF5_LaPD::Read_dataset_names
;
; Purpose:
;  Returns an array of the dataset names for the specified device.  Note that
;  the term "dataset" is used in the same sense as the HDF5 dataset; in other
;  words, the 2-D array of digitized data is considered to be a single dataset
;  and the associated headers are considered to be another dataset.  The naming
;  convention under versions 1.0 and 1.1 of the LaPD HDF5 software are like:
;
;    2-D array of digitized data: '3 channels [0:0]'
;    associated headers:          '3 channels [0:0] headers'
;
;  The '3 channels' refers to the configuration used for the device and the 
; '[0:0]' refers to the board and channel numbers, i.e. [<board>:<channel>]
;
FUNCTION HDF5_LaPD::Read_dataset_names, device_name

    device_group = self.raw_group->Open_group(device_name)
    dataset_names = device_group->Read_dataset_names()
    OBJ_DESTROY, device_group

    RETURN, dataset_names
END

;----------------------------------------------------------------------------
; HDF5_LaPD::Read_dataset
;
; Purpose:
;  Reads a dataset (or a slice of it) from a device.
;  The slice selection parameters are documented HDF5_Group::Read_dataset or in
;  the IDL documentation under H5S_SELECT_HYPERSLAB.
;
FUNCTION HDF5_LaPD::Read_dataset, device_name, dataset_name, $
                                  start, $
                                  count, $
                                  block=block, $
                                  stride=stride
    device_group = self.raw_group->Open_group(device_name)
    if n_elements(start) ne 0 then begin
        dataset = device_group->Read_dataset(dataset_name, start, $
                                             count, block=block, stride=stride) 
    endif else begin
        dataset = device_group->read_dataset(dataset_name)
    end
    OBJ_DESTROY, device_group

    RETURN, dataset
END

;----------------------------------------------------------------------------
; HDF5_LaPD::get_dataset_dimensions
; 
; Returns an array specifying the dimensions of a given dataset.
;
FUNCTION HDF5_LaPD::get_dataset_dimensions, device_name, dataset_name
    device_group = self.raw_group->Open_group(device_name)
    dims = device_group->get_dataset_dimensions(dataset_name)
    OBJ_DESTROY, device_group
    return, dims
end

;----------------------------------------------------------------------------
; HDF5_LaPD__define
;
; Purpose:
;  Defines the object structure for an HDF5_LaPD object.
;
PRO HDF5_LaPD__define
    struct = { HDF5_LaPD, $
               file_id: 0L, $
               version: "", $
               root_group: OBJ_NEW('HDF5_group'), $
               raw_group: OBJ_NEW('HDF5_group'), $
               error: OBJ_NEW('HDF5_error') $
             }
END
