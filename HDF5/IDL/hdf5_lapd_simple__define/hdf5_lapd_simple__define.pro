; HDF5_LaPD_Simple class definition
;
; This class inherits HDF5_LaPD to create some I/O routines for accessing
; "simple" LaPD runs. It assumes that you are using the SIS 3301 DAQ and the 
; 6K Compumotor probe drive, and have a fixed number of shots per spatial
; location.  If you are using other devices, or running complicated sequences
; during each shot, then this class may not work.
;
; EXAMPLE:
;   ; instantiate the object and open the HDF5 file
;   hdf = obj_new('HDF5_LaPD_Simple', filename='experimentname.hdf5')
;   ; get the configuration structure 
;   c = hdf->get_config()
;   ; print the list of channels available
;   hdf->print_channels
;
;   ; Suppose we have two channels on board 1 and three channels on board 2.
;   ; If we want all channels on board 1, then use
;   dataset = hdf->read_dataset(1)
;   ; the dimensions of dataset will be 
;   ; [ # samples, # shots, # x-points, # y-points, channel # ]
;   ; if any of these dimensions are of length 1, they will be omitted


;----------------------------------------------------------------------------
; HDF5_LaPD_Simple::Init
;   Initializes an instance of HDF5_Lapd_Simple.  
;
;   filename: the name of the HDF5 file to open.
;   nshot: The number of shots per spatial location.  Normally this does not
;       need to be specified unless the run did not complete.
;   pick: Set this keyword to open a dialog box to pick the hdf file.
;
function HDF5_LaPD_Simple::Init, filename=filename, nshot = nshot, pick = pick
    void = self->HDF5_LaPD::Init()
    if n_elements(filename) eq 1 or keyword_set(pick) then begin
        self->Open, filename, nshot=nshot, pick=pick 
    endif
    return, 1
end

;----------------------------------------------------------------------------
; HDF5_LaPD_Simple::Cleanup
;

pro HDF5_LaPD_Simple::Cleanup
    obj_destroy, self.daq_config
    obj_destroy, self.motion_list
    OBJ_DESTROY, self.daq
    OBJ_DESTROY, self.compumotor
    self->HDF5_LaPD::Cleanup
END

;----------------------------------------------------------------------------
; HDF5_LaPD_Simple::Open
;   Opens the specified file.
;
; Optional Parameters:
;   filename: the name of the HDF file to open
;   nshot: the number of shots per spatial point.  This is only necessary if
;       the run was not completed and the motion list was not repaired.
;   pick: Set this keyword to pop up a dialog box to pick the HDF file
;
pro HDF5_LaPD_Simple::Open, filename, nshot = nshot, pick = pick
    if keyword_set(pick) then $
        filename = DIALOG_PICKFILE()
       
    self->HDF5_LAPD::Open, filename
    ; note that we need to keep self.compumotor and self.daq around, otherwise
    ; we will lose their subgroups (motion_list and daq_config) when the get
    ; destroyed. 
    obj_destroy, self.compumotor
    self.compumotor = self->open_device('6K Compumotor')
    motion_list_names = stregex(self.compumotor->read_group_names(), $
        '^Motion list:.*', /extract)
    motion_list_name = (motion_list_names[where(motion_list_names ne '')])[0]
    ; strip 'Motion list: ' prefix
    motion_list_name = strmid(motion_list_name, 13)
    obj_destroy, self.motion_list
    self.motion_list = self.compumotor->open_group('Motion list: ' $
                                                    + motion_list_name)
    nx = self.motion_list->read_attribute('Nx')
    ny = self.motion_list->read_attribute('Ny')
    ; XXX if nx or ny > 200, error
    xy = self.motion_list->read_attribute('[x,y]')
    xpos = dblarr(200)
    ypos = dblarr(200)
    xpos[0:nx-1] = reform(xy[0,0:nx-1])
    ypos[0:ny-1] = reform(xy[1,0:*:nx])
    
    obj_destroy, self.daq
    self.daq = self->open_device('SIS 3301')
    daq_config_names = stregex(self.daq->read_group_names(), $
                                '^Configuration:.*', /extract)
    daq_config_name = (daq_config_names[where(daq_config_names ne '')])[0]
    ; strip 'Configuration: ' prefix
    daq_config_name = strmid(daq_config_name, 15)
    obj_destroy, self.daq_config
    self.daq_config = self.daq->open_group('Configuration: ' + daq_config_name)
    clock_rate = strsplit(self.daq_config->read_attribute('Clock rate'), $
                                                                /extract)
    samples_avg = $
        strsplit(self.daq_config->read_attribute('Samples to average'), $
                                                                /extract)
    ; note the clock rate is in MHz, so this is in u-secs
    if samples_avg[0] eq 'No' then $
        samples_avg[1] = '1'
    dt = float(samples_avg[1])/float(clock_rate[1])
    nsamples = self->get_nsamples()
    nboards = n_elements(nsamples)
    ; XXX if nboards > 100, error (and sell some for cash)
    nsamples_pad = lonarr(100) 
    nsamples_pad[0:nboards-1] = nsamples
    
    if n_elements(nshot) ne 1 then begin
        seq = self->HDF5_LaPD::read_dataset('Data run sequence', $
                                            'Sequence definition') 
        nshot = (seq[n_elements(seq) - 1].shot_number) - 1
        nshot /= nx*ny
    endif
    theta = self.motion_list->read_attribute('theta')
    total_motions = n_elements(theta)
    theta_pad = dblarr(200L^2)
    theta_pad[0:total_motions-1] = theta
    xy_pad = dblarr(2, 200L^2)
    xy_pad[*, 0:total_motions-1] = xy
    
    self.config = { config, xpos: xpos, ypos: ypos, nx: nx, ny: ny, $
                    xy: xy_pad, theta: theta_pad, nshot: nshot, dt: dt, $
                    nsamples: nsamples_pad, nboards: nboards, $
                    motion_list_name: motion_list_name, $
                    daq_config_name: daq_config_name }
end

;----------------------------------------------------------------------------
; HDF5_LaPD_Simple::get_config
;
; Purpose:
;  Returns a structure containing configuration information for the experiment.
;  Structure contains:
;      xpos, ypos: arrays listing x and y coordinates.  useful for making line
;           and contour plots.
;      nx, ny: number of x and y points.
;      xy: a linear list of the (x,y) coordinates at each point.  Dimensions are
;           [2, nx*ny]
;      theta: a linear list of the angle the probe shaft makes with the x-axis.
;      dt: time between samples, in microseconds
;      nsamples: array listing the number of samples for each board
;      motion_list_name, daq_config_name: name of motion list and daq config

function HDF5_LaPD_Simple::get_config
    xpos = self.config.xpos[0:self.config.nx-1]
    ypos = self.config.ypos[0:self.config.ny-1]
    nsamples = self.config.nsamples[0:self.config.nboards-1]
    total_motions = self.config.nx*self.config.ny 
    theta = self.config.theta[0:total_motions - 1]
    xy = self.config.xy[*, 0:total_motions - 1]
    config = { xpos: xpos, ypos: ypos, $
               nx: self.config.nx, ny: self.config.ny, $
               xy: xy, theta: theta, nshot: self.config.nshot, $
               dt: self.config.dt, $
               nsamples: nsamples, $
               motion_list_name: self.config.motion_list_name, $
               daq_config_name: self.config.daq_config_name } 
    return, config
end                                

;----------------------------------------------------------------------------
; HDF5_LaPD_Simple::print_channels
;   Prints a list of digitizer boards and recorded channels in the HDF file.
;
pro HDF5_LaPD_Simple::print_channels
    ; an iterator for the subgroups would be nice here
    board_tags = self->get_board_tags()
    for i = 0, n_elements(board_tags) - 1 do begin
        board = self.daq_config->open_group(board_tags[i])
        print, format = '(%"board %2i:   channel     name")', $
                board->read_attribute('Board')
        channel_tags = self->get_channel_tags(board)
        for j = 0, n_elements(channel_tags) - 1 do begin
            channel = board->open_group(channel_tags[j])
            print, format = '(%"%16i        %s")', $
                channel->read_attribute('Channel'), $
                channel->read_attribute('Data type')
            obj_destroy, channel
        endfor
        obj_destroy, board
    endfor
end

;----------------------------------------------------------------------------
; HDF5_LaPD_Simple::read_dataset
;   Returns datasets from the HDF file as an IDL array.  The dimensions of the
;   array will be 
;       [ # samples, # shots, # x points, # y points, # channels ]
;   Any dimensions of length 1 will be omitted, unless the keep_dim keyword
;   is set. 
;
;   Required parameters:
;       board_to_get: The number of the digitizer board to read (as given
;           by print_channels)
;   Optional parameters:
;       channels: An integer or integer array specifying which channels to read.
;       times: A two-integer array specifying the range of samples to read.
;       x: The index 
;       y: The index
;       average: If this keyword is set, then the data will be averaged over
;           shots.
;       keep_dim: If this keyword is set, then singlet dimensions won't be
;           reformed away.  This is useful if you want the returned array
;           to have the same number of dimensions for different datasets.
;
function HDF5_LaPD_Simple::read_dataset, board_to_get, $
                                         channels=channels, $
                                         times=times, $
                                         x=x, $
                                         y=y, $
                                         average=average, $
                                         keep_dim=keep_dim                                  
    board = self->open_board(board_to_get)
    nt = board->read_attribute('Board samples')
    obj_destroy, board
    
    if n_elements(channels) eq 0 then $
        channels = self->get_channel_list(board_to_get)
    
    ; Set defaults to read all data in the channels
    ; Recall that the first dimension is time, and the second dimension is
    ; [shot,x,y] flattened out.     
    nx = self.config.nx
    ny = self.config.ny
    start = [0, 0]
    count = [nt, self.config.nshot*nx*ny]
    block = [1, 1]
    stride = [1, 1]
    
    if n_elements(times) ne 0 then begin
        nt = times[1] - times[0] + 1
        start[0] = times[0]
        count[0] = nt
    endif
    ; Calculate block and stride for the requested line 
    if n_elements(y) ne 0 then begin
        start[1] = self.config.nshot * nx * y
        count[1] = self.config.nshot * nx
        ny = 1
    endif
    if n_elements(x) ne 0 then begin
        if n_elements(y) ne 0 then $
            start[1] = self.config.nshot * (y*nx + x) $
        else $
            start[1] = self.config.nshot * x
        count[1] = ny
        block[1] = self.config.nshot
        stride[1] = self.config.nshot * nx
        nx = 1
    endif
    
    total_shots = nx*ny*self.config.nshot
    data = fltarr(nt, self.config.nshot, nx, ny, n_elements(channels))
    for i = 0, n_elements(channels)-1 do begin
        channel_name = string(format = '(%"%s [%i:%i]")', $
            self.config.daq_config_name, board_to_get, channels[i]) 
        dataset = float(self->HDF5_LaPD::read_dataset('SIS 3301', $
                        channel_name, start, count, block=block, stride=stride))
        headers = self->HDF5_LaPD::read_dataset('SIS 3301', $
                       channel_name + ' headers', start[1], count[1], $
                       block=block[1], stride=stride[1])
        for j = 0, total_shots - 1 do begin
            dataset[*,j] = (headers[j]).scale*dataset[*,j] + (headers[j]).offset
        endfor
        data[*,*,*,*,i] = reform(temporary(dataset), nt, self.config.nshot, $
                                 nx, ny)
    endfor

    if keyword_set(average) then begin
        data = total(temporary(data), 2) / self.config.nshot
        if not keyword_set(keep_dim) then data = reform(temporary(data))
        return, data
    endif else begin
        if not keyword_set(keep_dim) then data = reform(temporary(data))
        return, data
    endelse
end

;----------------------------------------------------------------------------
; The following are utility functions and are probably not useful outside the
; object.
;----------------------------------------------------------------------------

; TODO: document
function HDF5_LaPD_Simple::get_board_tags
    board_tags = self.daq_config->read_group_names()
    ; XXX if no boards present, print error message
    board_tags = board_tags[where(stregex(board_tags, 'Boards\[[0-9]\]', $
                                      /boolean))]
    return, board_tags
end

;----------------------------------------------------------------------------
; TODO: document

function HDF5_LaPD_Simple::get_channel_tags, board
    channel_tags = board->read_group_names()
    ; XXX if no channels present, print error message
    channel_tags = channel_tags[where(stregex(channel_tags, 'Channels\[[0-9]\]', $
                                              /boolean))]
    return, channel_tags
end

;----------------------------------------------------------------------------
; TODO: document

function HDF5_LaPD_Simple::open_board, board_to_get
    board_tags = self->get_board_tags()
    ; XXX needs where() error check
    board_name = board_tags[where(self->get_board_list() eq board_to_get)]
    board = self.daq_config->open_group(board_name) 
    return, board
end

;----------------------------------------------------------------------------
; TODO: document

function HDF5_LaPD_Simple::get_board_list
    board_tags = self->get_board_tags()
    board_list = intarr(n_elements(board_tags))
    for i = 0, n_elements(board_tags) - 1 do begin
        board = self.daq_config->open_group(board_tags[i]) 
        board_list[i] = board->read_attribute('Board')
        obj_destroy, board
    endfor
    return, board_list
end

;----------------------------------------------------------------------------
; TODO: document

function HDF5_LaPD_Simple::get_channel_list, board_to_get
    board = self->open_board(board_to_get)
    channel_tags = self->get_channel_tags(board)
    channel_list = intarr(n_elements(channel_tags))
    for i = 0, n_elements(channel_tags) - 1 do begin
        channel = board->open_group(channel_tags[i])
        channel_list[i] = channel->read_attribute('Channel')
        obj_destroy, channel
    endfor
    obj_destroy, board
    return, channel_list
end

;----------------------------------------------------------------------------
; TODO: document

function HDF5_LaPD_Simple::get_nsamples
    board_list = self->get_board_list()
    nsamples = lonarr(n_elements(board_list))
    for i = 0, n_elements(board_list) - 1 do begin
        board = self->open_board(board_list[i])
        nsamples[i] = board->read_attribute('Board samples')
        obj_destroy, board
    endfor
    return, nsamples
end

;----------------------------------------------------------------------------

PRO HDF5_LaPD_Simple__define
    struct = { HDF5_LaPD_Simple, $
               inherits HDF5_LaPD, $
               config: { config, $
                         xpos: dblarr(200), ypos:dblarr(200), $
                         nx: 1L, ny: 1L, $
                         xy: dblarr(2, 200L^2), $
                         theta: dblarr(200L^2), $
                         nshot: 1L, $
                         dt: 0.0, $
                         nsamples: lonarr(100), $
                         nboards: 1L, $
                         motion_list_name: '', $
                         daq_config_name: '' $
                       }, $
               daq: OBJ_NEW('HDF5_group'), $
               compumotor: OBJ_NEW('HDF5_group'), $
               daq_config: OBJ_NEW('HDF5_group'), $
               motion_list: OBJ_NEW('HDF5_group') $
             }
end