;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; DATA TRANSCRIBER
;
; The transcription of the old-style data files to HDF5 format proves to be
; slow when done in LabVIEW.  This script is called at the end of the HDF5
; translation process to do the job instead.
;
; ---------------------------------------------------------------------------

pro Data_transcriber, Infile=hdf5_filename

print,'Doing data transcription for '+hdf5_filename

!PATH = 'C:\ACQ II home\HDF5\IDL\hdf5_lapd_simple__define' + ';' + !PATH  ; the separation character ';' is for Windows

; Define structures for headers
header = {data_head,shot_number:long(1),$
                  n_data:long(1),$
                  clipped:fix(1),channel:fix(1),$
                  min:fix(1),max:fix(1),unused1:float(1),unused2:float(1),$
                  unused3:float(1),unused4:float(1),unused5:float(1),$
                  unused6:float(1),unused7:float(1),gain:float(1),$
                  offset:float(1)}
header_size = 52

; Get configuration information from the HDF5 file.
hdf5_lapd = obj_new('HDF5_lapd', filename=hdf5_filename)
device_names = hdf5_lapd->Read_device_names()
for i = 0, n_elements(device_names) - 1 do begin
   device_name = device_names[i]
   print, 'Current device is '+device_name
   device = hdf5_lapd->Open_device(device_name)

   CASE device_name OF

      'SIS 3301': begin
         config_group_names = device->Read_group_names()
         if (n_elements(config_group_names) ne 1) then begin
            print, 'Warning: this script only works for a single configuration'
         endif else begin
            ; If only one configuration, then proceed...
            config_group_name = config_group_names[0]
            print, 'Current config group name: '+config_group_name
            config = device->Open_group(config_group_name)
            config_name = config->Read_attribute('Configuration')
            print, 'Actual config name is: '+config_name

            ; Build a list of group names that will contain each dataset in the HDF5 file,
            ; and the number of samples in each dataset.
            board_group_names = config->Read_group_names()
            nboards = n_elements(board_group_names)
            board_list = lonarr(nboards)
            nsamples = lonarr(nboards)
            channel_names = strarr(32)
            channel_samples = lonarr(32)
            total_shots = lonarr(32)
            nchannels = 0
            for j=0, nboards-1 do begin
               board = config->Open_group(board_group_names[j])
               board_list[j] = board->Read_attribute('Board')
               nsamples[j] = board->Read_attribute('Board samples')
               channel_group_names = board->Read_group_names()
               nchannels_on_board = n_elements(channel_group_names)
               channel_list = lonarr(nchannels_on_board)
               for k=0, nchannels_on_board-1 do begin
                  channel = board->Open_group(channel_group_names[k])
                  channel_list[k] = channel->Read_attribute('Channel')
                  channel_names[nchannels] = string(format='(%"%s [%0i:%0i]")', $
                     config_name, board_list[j], channel_list[k])
                  channel_samples[nchannels] = nsamples[j]
                  dims = device->get_dataset_dimensions(channel_names[nchannels]+' headers')
                  total_shots[nchannels]=dims[0]
                  nchannels++
                  channel->Close
               endfor
               board->Close
            endfor
            channel_names = channel_names[0:nchannels - 1]
            channel_samples = channel_samples[0:nchannels - 1]
            total_shots = total_shots[0:nchannels - 1]


            ; Now get the actual dataset names
            dataset_names = device->Read_dataset_names()
            for j = 0, n_elements(dataset_names) - 1 do begin
               dataset_name = dataset_names[j]
               print, 'Dataset name: '+dataset_name
            endfor

            ; Replace 'hdf5' suffix with 'SIS 3301.dat' and open data file
            sis_filename = strmid(hdf5_filename, 0, strlen(hdf5_filename)-4) + 'SIS 3301.dat'
            openr, sis_lun, sis_filename, /get_lun
            shot_size = total(channel_samples*2 + header_size, /integer)

            config->Close
            device->Close
            obj_destroy, hdf5_lapd
            root = obj_new('HDF5_File', filepath=hdf5_filename, operation='Open')
            daq = root->open_group('Raw data + config/SIS 3301')


            ; Process each data channel
            for j=0, nchannels-1 do begin
               print, 'Processing '+channel_names[j]
               indices = where(dataset_names EQ channel_names[j], count)
               if (count EQ 1) then begin
                  print, channel_names[j]+' already exists'
               endif else begin
                  ; seek past first j channels in data file
                  if j ne 0 then $
                     point_lun, sis_lun, total(channel_samples[0:j-1]*2 + header_size, /integer)
                  data_in = intarr(channel_samples[j])
                  data = 0
                  data = intarr(channel_samples[j], total_shots[j])
                  ; Reads one channel's dataset and headers, then seeks ahead one full record
                  ; length to get next shot for this channel.
                  for shot = 0, total_shots[j] - 1 do begin
                     point_lun, -sis_lun, pos
                     readu, sis_lun, header, data_in
                     data[*, shot] = data_in
                     point_lun, sis_lun, pos + shot_size
                  endfor ; reading all shots for this channel

                  daq->write_dataset, channel_names[j], data, $
                     chunk_dimensions=[channel_samples[j],1], gzip=9
 
               endelse ; dataset not already done
            endfor ; looping through channels
         endelse ; only one configuration
      end ; SIS 3301

      'TVS645A': begin
          print, 'TVS645A not currently supported'
      end

   ENDCASE

   hdf5_lapd = obj_new('HDF5_lapd', filename=hdf5_filename)
endfor ; looping through devices


obj_destroy, hdf5_lapd


data = 0
close, sis_lun

enter_key = ''
READ, PROMPT='Press <Enter> key to go on: ', enter_key
exit, /NO_CONFIRM

end
