/* Call Library source file */

#include "extcode.h"
#include "hdf5.h"
/* extern unsigned H5P_CLS_FILE_ACCESS_g; */

extern "C"
{
	_declspec(dllexport) long GetMaxErrLen(unsigned propListEnum, unsigned *propListType);
}

_declspec(dllexport) long getPropertyListType(unsigned propListEnum, unsigned *propListType)
{
	switch(propListEnum)
	{
	case 0:
		*propListType = H5P_NO_CLASS;
		return(0);
	case 1:
		*propListType = H5P_FILE_CREATE;
		return(0);
	case 2:
		*propListType = H5P_FILE_ACCESS;
		return(0);
	case 3:
		*propListType = H5P_DATASET_CREATE;
		return(0);
	case 4:
		*propListType = H5P_DATASET_XFER;
		return(0);
	case 5:
		*propListType = H5P_MOUNT;
		return(0);
	default:
		return(-1);
	}

	return(-1);
}