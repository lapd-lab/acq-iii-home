/* Call Library source file */

#include "extcode.h"
#include "hdf5.h"

extern "C"
{
	_declspec( dllexport ) long GetPredefinedTypeId( long definedTypeEnum );
}

_declspec( dllexport ) long GetPredefinedTypeId( long definedTypeEnum )
{
	switch( definedTypeEnum )
	{
	case 0:
		return( 0 );
	case 1:
		return( H5T_IEEE_F32BE );
	case 2:
		return( H5T_IEEE_F32LE );
	case 3:
		return( H5T_IEEE_F64BE );
	case 4:
		return( H5T_IEEE_F64LE );
	case 5:
		return( H5T_NATIVE_CHAR );
	case 6:
		return( H5T_NATIVE_SCHAR );
	case 7:
		return( H5T_NATIVE_UCHAR );
	case 8:
		return( H5T_NATIVE_SHORT );
	case 9:
		return( H5T_NATIVE_USHORT );
	case 10:
		return( H5T_NATIVE_INT );
	case 11:
		return( H5T_NATIVE_UINT );
	case 12:
		return( H5T_NATIVE_LONG );
	case 13:
		return( H5T_NATIVE_ULONG );
	case 14:
		return( H5T_NATIVE_LLONG );
	case 15:
		return( H5T_NATIVE_ULLONG );
	case 16:
		return( H5T_NATIVE_FLOAT );
	case 17:
		return( H5T_NATIVE_DOUBLE );
	case 18:
		return( H5T_NATIVE_LDOUBLE );
	case 19:
		return( H5T_NATIVE_B8 );
	case 20:
		return( H5T_NATIVE_B16 );
	case 21:
		return( H5T_NATIVE_B32 );
	case 22:
		return( H5T_NATIVE_B64 );
	case 23:
		return( H5T_NATIVE_OPAQUE );
	case 24:
		return( H5T_NATIVE_HADDR );
	case 25:
		return( H5T_NATIVE_HSIZE );
	case 26:
		return( H5T_NATIVE_HSSIZE );
	case 27:
		return( H5T_NATIVE_HERR );
	case 28:
		return( H5T_NATIVE_HBOOL );
	case 29:
		return( H5T_STD_U8LE );
	case 30:
		return( H5T_STD_U8BE );
	case 31:
		return( H5T_STD_U16LE );
	case 32:
		return( H5T_STD_U16BE );
	case 33:
		return( H5T_STD_U32LE );
	case 34:
		return( H5T_STD_U32BE );
	case 35:
		return( H5T_STD_U64LE );
	case 36:
		return( H5T_STD_U64BE );
	case 37:
		return( H5T_STD_B8LE );
	case 38:
		return( H5T_STD_B8BE );
	case 39:
		return( H5T_STD_B16LE );
	case 40:
		return( H5T_STD_B16BE );
	case 41:
		return( H5T_STD_B32LE );
	case 42:
		return( H5T_STD_B32BE );
	case 43:
		return( H5T_STD_B64LE );
	case 44:
		return( H5T_STD_B64BE );
	case 45:
		return( H5T_STD_REF_OBJ );
	case 46:
		return( H5T_STD_REF_DSETREG );
	case 47:
		return( H5T_C_S1 );
	case 48:
		return( H5T_STD_I8LE );
	case 49:
		return( H5T_STD_I8BE );
	case 50:
		return( H5T_STD_I16LE );
	case 51:
		return( H5T_STD_I16BE );
	case 52:
		return( H5T_STD_I32LE );
	case 53:
		return( H5T_STD_I32BE );
	case 54:
		return( H5T_STD_I64LE );
	case 55:
		return( H5T_STD_I64BE );
	default:
		return( -1 );
	}

	return( -1 );
}

/*
0	None
1	IEEE_F32BE
2	IEEE_F32LE
3	IEEE_F64BE
4	IEEE_F64LE
5	NATIVE_CHAR
6	NATIVE_SCHAR
7	NATIVE_UCHAR
8	NATIVE_SHORT
9	NATIVE_USHORT
10	NATIVE_INT
11	NATIVE_UINT
12	NATIVE_LONG
13	NATIVE_ULONG
14	NATIVE_LLONG
15	NATIVE_ULLONG
16	NATIVE_FLOAT
17	NATIVE_DOUBLE
18	NATIVE_LDOUBLE
19	NATIVE_B8
20	NATIVE_B16
21	NATIVE_B32
22	NATIVE_B64
23	NATIVE_OPAQUE
24	NATIVE_HADDR
25	NATIVE_HSIZE
26	NATIVE_HSSIZE
27	NATIVE_HERR
28	NATIVE_HBOOL
29	STD_U8LE
30	STD_U8BE
31	STD_U16LE
32	STD_U16BE
33	STD_U32LE
34	STD_U32BE
35	STD_U64LE
36	STD_U64BE
37	STD_B8LE
38	STD_B8BE
39	STD_B16LE
40	STD_B16BE
41	STD_B32LE
42	STD_B32BE
43	STD_B64LE
44	STD_B64BE
45	STD_REF_OBJ
46	STD_REF_DSETREG
47	C_S1
48	STD_I8LE
49	STD_I8BE
50	STD_I16LE
51	STD_I16BE
52	STD_I32LE
53	STD_I32BE
54	STD_I64LE
55	STD_I64BE
*/