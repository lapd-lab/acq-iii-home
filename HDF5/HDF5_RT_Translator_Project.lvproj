﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="10008000">
	<Property Name="CCSymbols" Type="Str"></Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="RT_Translator_system_related.lvlib" Type="Library" URL="../RT_Translator_system_related.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Array of VData to VArray.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Array of VData to VArray.vi"/>
				<Item Name="Array of VData to VCluster.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Array of VData to VCluster.vi"/>
				<Item Name="Array Size(s).vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Array Size(s).vi"/>
				<Item Name="Build Error Cluster.vi" Type="VI" URL="/&lt;userlib&gt;/error/error.llb/Build Error Cluster.vi"/>
				<Item Name="Cluster to Array of VData.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Cluster to Array of VData.vi"/>
				<Item Name="Compute 1D Index.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Compute 1D Index.vi"/>
				<Item Name="Get Array Element TD.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Get Array Element TD.vi"/>
				<Item Name="Get Array Element TDEnum.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Get Array Element TDEnum.vi"/>
				<Item Name="Get Cluster Element by Name.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Get Cluster Element by Name.vi"/>
				<Item Name="Get Cluster Element Names.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Get Cluster Element Names.vi"/>
				<Item Name="Get Cluster Elements TDs.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Get Cluster Elements TDs.vi"/>
				<Item Name="Get Data Name from TD.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Get Data Name from TD.vi"/>
				<Item Name="Get Data Name.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Get Data Name.vi"/>
				<Item Name="Get Default Data from TD.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Get Default Data from TD.vi"/>
				<Item Name="Get Header from TD.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Get Header from TD.vi"/>
				<Item Name="Get Last PString.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Get Last PString.vi"/>
				<Item Name="Get PString.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Get PString.vi"/>
				<Item Name="Get TDEnum from Data.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Get TDEnum from Data.vi"/>
				<Item Name="Index Array.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Index Array.vi"/>
				<Item Name="LF Read File (DBL).vi" Type="VI" URL="/&lt;userlib&gt;/largefile/largefile.llb/LF Read File (DBL).vi"/>
				<Item Name="LF Read File (I16).vi" Type="VI" URL="/&lt;userlib&gt;/largefile/largefile.llb/LF Read File (I16).vi"/>
				<Item Name="LF Read File (I32).vi" Type="VI" URL="/&lt;userlib&gt;/largefile/largefile.llb/LF Read File (I32).vi"/>
				<Item Name="LF Read File (SGL).vi" Type="VI" URL="/&lt;userlib&gt;/largefile/largefile.llb/LF Read File (SGL).vi"/>
				<Item Name="LF Read File (STR).vi" Type="VI" URL="/&lt;userlib&gt;/largefile/largefile.llb/LF Read File (STR).vi"/>
				<Item Name="LF Read File.vi" Type="VI" URL="/&lt;userlib&gt;/largefile/largefile.llb/LF Read File.vi"/>
				<Item Name="LFU Error Handler.vi" Type="VI" URL="/&lt;userlib&gt;/largefile/largefile.llb/LFU Error Handler.vi"/>
				<Item Name="LFU Get File Size.vi" Type="VI" URL="/&lt;userlib&gt;/largefile/largefile.llb/LFU Get File Size.vi"/>
				<Item Name="LFU Retrieve File Handle.vi" Type="VI" URL="/&lt;userlib&gt;/largefile/largefile.llb/LFU Retrieve File Handle.vi"/>
				<Item Name="LFU Set File Pointer.vi" Type="VI" URL="/&lt;userlib&gt;/largefile/largefile.llb/LFU Set File Pointer.vi"/>
				<Item Name="Parse String with TDs.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Parse String with TDs.vi"/>
				<Item Name="Reshape 1D Array.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Reshape 1D Array.vi"/>
				<Item Name="Reshape Array to 1D VArray.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Reshape Array to 1D VArray.vi"/>
				<Item Name="Set Cluster Element by Name.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Set Cluster Element by Name.vi"/>
				<Item Name="Set Data Name.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Set Data Name.vi"/>
				<Item Name="Split Cluster TD.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Split Cluster TD.vi"/>
				<Item Name="Type Descriptor Enumeration.ctl" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Type Descriptor Enumeration.ctl"/>
				<Item Name="Type Descriptor Header.ctl" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Type Descriptor Header.ctl"/>
				<Item Name="Variant to Header Info.vi" Type="VI" URL="/&lt;userlib&gt;/lvdata/lvdata.llb/Variant to Header Info.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="GOOP Object Repository Method.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository Method.ctl"/>
				<Item Name="GOOP Object Repository Statistics.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository Statistics.ctl"/>
				<Item Name="GOOP Object Repository.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="NI_Database_API.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/database/NI_Database_API.lvlib"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="Read BMP File Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Read BMP File Data.vi"/>
				<Item Name="Read BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Read BMP File.vi"/>
				<Item Name="Read BMP Header Info.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Read BMP Header Info.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="HDF5 real-time translator" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{256156A7-CAA9-4917-BEF6-BCF78D849099}</Property>
				<Property Name="App_INI_GUID" Type="Str">{091576D6-5D38-4C73-8658-110EC1C8BAE0}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">HDF5 real-time translator</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/HDF5_RT_Translator_Project/HDF5 real-time translator</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Application.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/HDF5 real-time translator/Application.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/HDF5 real-time translator/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{C044E669-D34E-443F-9DB3-BC1803864635}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">UCLA Basic Plasma Science Facility</Property>
				<Property Name="TgtF_fileDescription" Type="Str">HDF5 real-time translator</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">HDF5 real-time translator</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2013 UCLA Basic Plasma Science Facility</Property>
				<Property Name="TgtF_productName" Type="Str">HDF5 real-time translator</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{9F02CD8C-0D24-42E9-99E9-5D2B9C07FBE5}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Application.exe</Property>
			</Item>
			<Item Name="HDF5 real-time translator2" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{E0E217F8-301D-496A-A466-2CCAEB073F14}</Property>
				<Property Name="App_INI_GUID" Type="Str">{865A5E32-6232-4D52-9F66-BE3A870DD72E}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">HDF5 real-time translator2</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/HDF5_RT_Translator_Project/HDF5 real-time translator2</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Destination[0].destName" Type="Str">Application.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/HDF5 real-time translator2/Application.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/HDF5 real-time translator2/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{C044E669-D34E-443F-9DB3-BC1803864635}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">UCLA Basic Plasma Science Facility</Property>
				<Property Name="TgtF_fileDescription" Type="Str">HDF5 real-time translator2</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">HDF5 real-time translator2</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2013 UCLA Basic Plasma Science Facility</Property>
				<Property Name="TgtF_productName" Type="Str">HDF5 real-time translator2</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{A201FE18-C799-42D0-8616-19B982156FE8}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Application.exe</Property>
			</Item>
			<Item Name="HDF5 real-time translator3" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{33326BEB-DD3C-4EBB-B28C-63F9741C56EA}</Property>
				<Property Name="App_INI_GUID" Type="Str">{A6CF1102-10D9-4C89-9CBF-D638C7967122}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">HDF5 real-time translator3</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/HDF5_RT_Translator_Project/HDF5 real-time translator3</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Destination[0].destName" Type="Str">Application.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/HDF5 real-time translator3/Application.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/HDF5 real-time translator3/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{C044E669-D34E-443F-9DB3-BC1803864635}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">UCLA Basic Plasma Science Facility</Property>
				<Property Name="TgtF_fileDescription" Type="Str">HDF5 real-time translator3</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">HDF5 real-time translator3</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2013 UCLA Basic Plasma Science Facility</Property>
				<Property Name="TgtF_productName" Type="Str">HDF5 real-time translator3</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{9B6A8F05-7D05-44BD-85F7-DBE737D5EA72}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Application.exe</Property>
			</Item>
			<Item Name="HDF5 real-time translator4" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{01555EF9-8E6E-4F0F-98A1-98854492F2F9}</Property>
				<Property Name="App_INI_GUID" Type="Str">{54D78193-5E03-4C9A-97AD-D43D7D04B516}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">HDF5 real-time translator4</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/HDF5_RT_Translator_Project/HDF5 real-time translator4</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Destination[0].destName" Type="Str">Application.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/HDF5 real-time translator4/Application.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/HDF5 real-time translator4/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{C7CB1D3A-6D3C-4402-9240-447A2495C65C}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Dependencies/Items in Memory/HDF5 real-time translator.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">UCLA Basic Plasma Science Facility</Property>
				<Property Name="TgtF_fileDescription" Type="Str">HDF5 real-time translator4</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">HDF5 real-time translator4</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2013 UCLA Basic Plasma Science Facility</Property>
				<Property Name="TgtF_productName" Type="Str">HDF5 real-time translator4</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{B1CA8784-4BA1-4483-B640-411C8AF93B9D}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Application.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
